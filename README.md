# Survey API Back-end

Back-end API and service components for Survey project. Provides Computer Aided Web Interviewing (CAWI) functionality. Project Spring components are used in other projects

Built with Spring Framework. Provides REST API in (HATEOAS) HAL format.


## Requirements

Minimal (for develop, demo profiles):

* [Java 8](https://java.com/en/download/)

Recommended for production environment:

* Elastisearch server
* MariaDB

## Getting started

Run using `./gradlew bootRun`.

This will build and start a test server in development mode using Gradle and Spring Boot.  By default, the server starts at port 8080 and exposes RESTful API root (HATEOAS) at http://localhost:8080/api/v1.

### Configuration

Look into `application.properties` for configuration examples.

### Building

Use Gradle scripts:

* `./gradlew check` to run all tests and code checks
* `./gradlew build` to build a project package
* `./gradlew help` for more options
