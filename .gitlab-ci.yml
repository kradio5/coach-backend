#
# Gitlab runner script for Docker builds.
#
# Secure variables expected:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_ENV_PRODUCTION - Production environment name for deployment
#   AWS_ENV_STAGING - Staging environment name for deployment
#

image: ausov/ci-awscli:latest

variables:
  AWS_DEFAULT_REGION: eu-central-1
  REGISTRY: 875290181947.dkr.ecr.eu-central-1.amazonaws.com
  GRADLE_USER_HOME: build/gradle-home

stages:
  - build
  - publish
  - clean
  - deploy

build:
  stage: build
  image: ausov/docker-ci-oraclejava
  variables:
  before_script:
    - mkdir -p ${GRADLE_USER_HOME}
    - cp config/ci/gradle.properties ${GRADLE_USER_HOME}/gradle.properties
  script:
    - ./gradlew build dockerfile --refresh-dependencies -PydwRepoUsername=${AWS_ACCESS_KEY_ID} -PydwRepoPassword=${AWS_SECRET_ACCESS_KEY}
  cache:
    paths:
      - ${GRADLE_USER_HOME}/caches
      - ${GRADLE_USER_HOME}/wrapper/dists
  artifacts:
    paths:
      - build/reports
      - build/docker
    when: always
    expire_in: 1 day

snapshot: &publish
  stage: publish
  only:
    - master
  tags: [docker]
  image: ausov/docker-ci-docker-aws
  services:
    - docker:dind
  before_script:
    - docker info
    - eval $(aws ecr get-login --region ${AWS_DEFAULT_REGION})
    - export IMAGE="${REGISTRY}/${CI_PROJECT_PATH}"
  script:
    - docker build --pull --tag "$IMAGE:snapshot" build/docker
    - docker push "$IMAGE:snapshot"
  dependencies:
    - build

release:
  <<: *publish
  stage: publish
  only:
    - /^v\d+\.\d+\.\d+$/
  script:
    - docker build --pull --tag "$IMAGE:latest" build/docker
    - export BUILD_VER=( ${CI_BUILD_REF_NAME//./ } )
    - docker tag $IMAGE "${IMAGE}:${CI_BUILD_REF_NAME}"
    - docker tag $IMAGE "${IMAGE}:${BUILD_VER[0]}.${BUILD_VER[1]}"
    - docker tag $IMAGE "${IMAGE}:${BUILD_VER[0]}"
    - docker push $IMAGE
  
clean:
  only:
    - master
  stage: clean
  script:
    - src/main/scripts/aws-ecr-cleanup.sh ${CI_PROJECT_PATH}

deploy-staging: &deploy
  stage: deploy
  environment: staging
  allow_failure: true
  only:
    - master
  before_script:
    - AWS_ENV=$AWS_ENV_STAGING
  script:
    - aws elasticbeanstalk restart-app-server --environment-name $AWS_ENV

# Deploy on production
deploy-production:
  <<: *deploy
  environment: production
  when: manual
  allow_failure: false
  only:
    - /^v\d+\.\d+\.\d+$/
  before_script:
    - AWS_ENV=$AWS_ENV_PRODUCTION
