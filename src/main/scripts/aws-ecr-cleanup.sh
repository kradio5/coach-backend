#!/bin/bash
#
# Cleanup AWS ECR images without a tag.
# See also: https://github.com/HubLogix/aws-ecs-deploy/blob/master/aws-ecr-clean 
#

repo=$1

aws ecr list-images --repository-name $repo --query 'imageIds[?type(imageTag)!=`string`].[imageDigest]' --output text | while read line; do aws ecr batch-delete-image --repository-name $repo --image-ids imageDigest=$line; done
