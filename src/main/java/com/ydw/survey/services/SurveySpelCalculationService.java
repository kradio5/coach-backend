package com.ydw.survey.services;

import com.ydw.survey.annotations.SpelFunction;
import com.ydw.survey.dao.QuestionAnswerDao;
import com.ydw.survey.dao.QuestionDao;
import com.ydw.survey.dao.SurveyCalculationDao;
import com.ydw.survey.dao.SurveyResultCalculationDao;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

// @Service
// @Transactional(readOnly = true)
public class SurveySpelCalculationService {

  private static final Logger LOG = LoggerFactory.getLogger(SurveySpelCalculationService.class);

  @Autowired
  private QuestionAnswerDao questionAnswerDao;

  @Autowired
  private QuestionDao questionDao;

  private final Map<String, Method> spelFunctions = new HashMap<String, Method>();

  @Autowired
  private SurveyCalculationDao surveyCalculationDao;

  @Autowired
  private SurveyResultCalculationDao surveyResultCalculationDao;

  /**
   * Calculates values basing on survey calculation settings.<br />
   * <br />
   * Note: calculation objects are saved
   */
  public void calculateSurveyResult(final SurveyResult surveyResult) {
    List<SurveyResultCalculation> surveyResultCalculations =
        surveyResultCalculationDao.findBySurveyResultOrderBySortOrder(surveyResult);
    surveyResultCalculationDao.delete(surveyResultCalculations);
    surveyResultCalculations = calculateSurveyResults(surveyResult);
    surveyResultCalculationDao.save(surveyResultCalculations);
  }

  /**
   * Calculate template.
   */
  public String calculateTemplate(final String calculationTemplate, final Object... variables) {
    final SurveySpelCalculator surveyCalculator =
        new SurveySpelCalculator(spelFunctions, false, variables);
    return surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Initialize functions.
   */
  @PostConstruct
  public void init() {
    prepareSpelFunctions();
  }

  /**
   * Checks if survey calculation has valid expression.
   */
  public void validateSurveyCalculation(final SurveyCalculation surveyCalculation) {
    validateSurveyCalculations(Collections.singletonList(surveyCalculation));
  }

  /**
   * Validate calculation expression for list of survey calculations.
   */
  public void validateSurveyCalculations(final List<SurveyCalculation> surveyCalculations) {
    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return;
    }
    final Survey survey = surveyCalculations.get(0).getSurvey();
    final List<Question> questions = questionDao.findBySectionSurveyOrderBySortOrder(survey);
    final List<SurveyCalculation> existingSurveyCalculations =
        surveyCalculationDao.findBySurveyOrderBySortOrder(survey);
    final SurveySpelCalculator surveyCalculator =
        new SurveySpelCalculator(spelFunctions, questions, existingSurveyCalculations);
    surveyCalculator.performSurveyValidations(surveyCalculations);
  }

  /**
   * Checks if survey calculations have valid expressions.
   */
  public void validateSurveyCalculations(final Survey survey) {
    final List<SurveyCalculation> surveyCalculations =
        surveyCalculationDao.findBySurveyOrderBySortOrder(survey);
    validateSurveyCalculations(surveyCalculations);

  }

  /**
   * Validate template.
   */
  public void validateTemplate(final String calculationTemplate, final Object... variables) {
    final SurveySpelCalculator surveyCalculator =
        new SurveySpelCalculator(spelFunctions, true, variables);
    surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Calculate expressions for list of survey result calculations.
   */
  private List<SurveyResultCalculation> calculateSurveyResults(
      final SurveyResult surveyResult) {
    final Survey survey = surveyResult.getSurvey();
    final List<SurveyCalculation> surveyCalculations = survey.getCalculations();
    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return null;
    }
    final List<Question> questions = questionDao.findBySectionSurveyOrderBySortOrder(survey);
    final List<QuestionAnswer> questionAnswers =
        questionAnswerDao.findBySectionSurveyResultOrderBySortOrder(surveyResult);
    final SurveySpelCalculator surveyCalculator =
        new SurveySpelCalculator(spelFunctions, questions, questionAnswers, surveyCalculations);
    final List<SurveyResultCalculation> surveyResultCalculations =
        surveyCalculator.performSurveyResultCalculations(surveyResult, surveyCalculations);
    return surveyResultCalculations;
  }

  /**
   * Collect methods of CalculatorFunctions annotated accordingly.<br />
   * <br />
   * Examples of function usage in SpEL:<br />
   * #MIN(#q2, #q3)<br />
   * #MAX(#q2, #q3)<br />
   * #IF(#q2 < #q3, 'error', #IF(#q3 < #q2, 'ok', 'error'))<br />
   * #SWITCH(#q1, #q2, 'q2', #q3, 'q3', #q4, 'q4', 'not found')
   */
  private void prepareSpelFunctions() {
    try {
      final Method[] calculationMethods = SpelFunctions.class.getDeclaredMethods();
      final int calculationMethodsLength = calculationMethods.length;
      for (int index = 0; index < calculationMethodsLength; index++) {
        final Method calculationMethod = calculationMethods[index];
        final SpelFunction calculatorMethodAnnotation =
            calculationMethod.getAnnotation(SpelFunction.class);
        if (calculatorMethodAnnotation == null) {
          continue;
        }
        final String calculationMethodName = calculatorMethodAnnotation.name();
        spelFunctions.put(calculationMethodName, calculationMethod);
      }
    } catch (final Exception exc) {
      LOG.error("prepareCalculatorFunctions", exc);
      throw new IllegalStateException(exc);
    }
  }

}
