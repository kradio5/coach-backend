package com.ydw.survey.services;

public final class SurveyErrorCodes {

  public static final String SURVEY_CALCULATOR_INVALID_TEMPLATE =
      "wrong calculation template format";

  public static final String SURVEY_CALCULATOR_INVALID_CONDITION = "wrong_condition_format";

  public static final String SURVEY_CALCULATOR_INVALID_VARIABLE = "invalid_variable";

  public static final String SURVEY_CALCULATOR_INVALID_EXPRESSION = "invalid_expression";

  public static final String SURVEY_CAMPAIGN_NOT_ACTIVE = "survey_campaign_not_active";

  public static final String SURVEY_CAMPAIGN_NOT_STARTED = "survey_campaign_not_started";

  public static final String SURVEY_CAMPAIGN_EXPIRED = "survey_campaign_expired";

  public static final String SURVEY_CAMPAIGN_LOCKED = "survey_campaign_locked";

  public static final String SURVEY_CAMPAIGN_NOT_FOUND = "survey_campaign_not_found";

  public static final String SURVEY_RESULT_NOT_FOUND = "survey_result_not_found";

  public static final String SURVEY_RESULT_NOT_COMPLETED = "survey_result_not_completed";

  public static final String INVALID_DATA = "invalid_data";

}
