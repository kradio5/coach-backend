package com.ydw.survey.services;

/**
 * Base class for rest event handlers.
 */
public abstract class AbstractRestEventHandler {

  protected int getNextSortOrder(final Object lastSortOrderObj) {
    int lastSortOrder;
    if (lastSortOrderObj == null) {
      lastSortOrder = 0;
    } else {
      lastSortOrder = (int) lastSortOrderObj + 1;
    }
    return lastSortOrder;
  }

}
