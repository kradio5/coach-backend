package com.ydw.survey.services;

import com.ydw.survey.dao.QuestionAnswerDao;
import com.ydw.survey.dao.QuestionDao;
import com.ydw.survey.dao.SurveyCalculationDao;
import com.ydw.survey.dao.SurveyResultCalculationDao;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class SurveyEvalexCalculationService {

  @Autowired
  private QuestionAnswerDao questionAnswerDao;

  @Autowired
  private QuestionDao questionDao;

  @Autowired
  private SurveyCalculationDao surveyCalculationDao;

  @Autowired
  private SurveyResultCalculationDao surveyResultCalculationDao;

  /**
   * Calculates values basing on survey calculation settings.<br />
   * <br />
   * Note: calculation objects are saved
   */
  public void calculateSurveyResult(final SurveyResult surveyResult) {
    List<SurveyResultCalculation> surveyResultCalculations =
        surveyResultCalculationDao.findBySurveyResultOrderBySortOrder(surveyResult);
    surveyResultCalculationDao.delete(surveyResultCalculations);
    surveyResultCalculations = calculateSurveyResults(surveyResult);
    surveyResultCalculationDao.save(surveyResultCalculations);
  }

  /**
   * Calculate template.
   */
  public String calculateTemplate(final String calculationTemplate, final Object... variables) {
    final SurveyEvalexCalculator surveyCalculator =
        new SurveyEvalexCalculator(false, variables);
    return surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Calculate template without running evalex expressions.
   */
  public String calculateTemplateWithoutExpressions(final String calculationTemplate,
      final Object... variables) {
    final SurveyEvalexCalculator surveyCalculator =
        new SurveyEvalexCalculator(false, true, variables);
    return surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Checks if survey calculation has valid expression.
   */
  public void validateSurveyCalculation(final SurveyCalculation surveyCalculation) {
    validateSurveyCalculations(Collections.singletonList(surveyCalculation));
  }

  /**
   * Validate calculation expression for list of survey calculations.
   */
  public void validateSurveyCalculations(final List<SurveyCalculation> surveyCalculations) {
    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return;
    }
    final Survey survey = surveyCalculations.get(0).getSurvey();
    final List<Question> questions = questionDao.findBySectionSurveyOrderBySortOrder(survey);
    final List<SurveyCalculation> existingSurveyCalculations =
        surveyCalculationDao.findBySurveyOrderBySortOrder(survey);
    final SurveyEvalexCalculator surveyCalculator =
        new SurveyEvalexCalculator(questions, existingSurveyCalculations);
    surveyCalculator.performSurveyValidations(surveyCalculations);
  }

  /**
   * Checks if survey calculations have valid expressions.
   */
  public void validateSurveyCalculations(final Survey survey) {
    final List<SurveyCalculation> surveyCalculations =
        surveyCalculationDao.findBySurveyOrderBySortOrder(survey);
    validateSurveyCalculations(surveyCalculations);

  }

  /**
   * Validate template.
   */
  public void validateTemplate(final String calculationTemplate, final Object... variables) {
    final SurveyEvalexCalculator surveyCalculator = new SurveyEvalexCalculator(true, variables);
    surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Validate template without running evalex expressions.
   */
  public void validateTemplateWithoutExpressions(final String calculationTemplate,
      final Object... variables) {
    final SurveyEvalexCalculator surveyCalculator =
        new SurveyEvalexCalculator(true, true, variables);
    surveyCalculator.performTemplateCalculation(calculationTemplate);
  }

  /**
   * Calculate expressions for list of survey result calculations.
   */
  private List<SurveyResultCalculation> calculateSurveyResults(
      final SurveyResult surveyResult) {
    final Survey survey = surveyResult.getSurvey();
    final List<SurveyCalculation> surveyCalculations = survey.getCalculations();
    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return null;
    }
    final List<Question> questions = questionDao.findBySectionSurveyOrderBySortOrder(survey);
    final List<QuestionAnswer> questionAnswers =
        questionAnswerDao.findBySectionSurveyResultOrderBySortOrder(surveyResult);
    final SurveyEvalexCalculator surveyCalculator =
        new SurveyEvalexCalculator(questions, questionAnswers, surveyCalculations);
    final List<SurveyResultCalculation> surveyResultCalculations =
        surveyCalculator.performSurveyResultCalculations(surveyResult, surveyCalculations);
    return surveyResultCalculations;
  }

}
