package com.ydw.survey.services;

import com.ydw.common.security.utils.SecurityUtils;
import com.ydw.survey.initializer.SurveyResultDataPopulator;
import com.ydw.survey.model.SurveyResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(SurveyResult.class)
@Transactional
public class SurveyResultRestEventHandler {

  @Autowired
  private SurveyResultDataPopulator surveyResultDataPopulator;

  @Autowired
  private SurveyResultService surveyResultService;

  @Autowired
  private SurveyEvalexCalculationService surveyEvalexCalculationService;

  /**
   * After Survey Result is created the full structure of sections,<br />
   * answers and calculations must be populated.
   */
  @HandleAfterCreate
  public void afterCreate(final SurveyResult surveyResult) {
    surveyResultDataPopulator.fillSurveyResult(surveyResult);
  }

  /**
   * After survey result is saved check if calculation should be done.
   */
  @HandleAfterSave
  public void afterSave(final SurveyResult surveyResult) {
    if (surveyResult.isCalculate()) {
      surveyEvalexCalculationService.calculateSurveyResult(surveyResult);
    }
  }

  /**
   * Check if campaign is not empty then default survey basing on current amount of passed surveys.
   */
  @HandleBeforeCreate
  public void beforeCreate(final SurveyResult surveyResult) {
    surveyResult.setCreatedBy(SecurityUtils.getMyPrincipal());
    surveyResultService.assignSurveyResultByCampaign(surveyResult);
  }

  /**
   * Before survey result is saved check if it's completed.
   */
  @HandleBeforeSave
  public void beforeSave(final SurveyResult surveyResult) {
    if (surveyResult.isCalculate()) {
      surveyResult.setCompleted(true);
      if (surveyResult.getSurveyCampaign() != null) {
        surveyResult.setCampaignAnswerCounter(surveyResult.getCampaignAnswerCounter() + 1);
      }
    }
  }

}
