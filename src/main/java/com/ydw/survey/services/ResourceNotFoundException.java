package com.ydw.survey.services;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT) // 204
public class ResourceNotFoundException extends ResourceException {

  private static final long serialVersionUID = -7021616839403341848L;

  public ResourceNotFoundException(final String code) {
    super(code);
  }

}
