package com.ydw.survey.services;

import com.ydw.survey.dao.SurveyCategoryDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveySection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(Survey.class)
@Transactional
public class SurveyRestEventHandler {

  @Autowired
  private SurveyCategoryDao surveyCategoryDao;

  @Autowired
  private SurveyService surveyService;

  @HandleAfterCreate
  public void afterCreate(final Survey survey) {
    surveyService.afterCreate(survey);
  }

  /**
   * After survey is saved check if validation should be done.
   */
  @HandleAfterSave
  public void afterSave(final Survey survey) {
    surveyService.afterSave(survey);
  }

  /**
   * Before survey is created create first section and set category.
   */
  @HandleBeforeCreate
  public void beforeCreate(final Survey survey) {
    survey.addSection(new SurveySection());
    if (survey.getSingleCategorySlug() != null) {
      survey.getCategories().clear();
      survey.addCategory(surveyCategoryDao.findOneBySlug(survey.getSingleCategorySlug()));
    }
  }

  @HandleBeforeDelete
  public void beforeDelete(final Survey survey) {
    surveyService.beforeDelete(survey);
  }

  /**
   * Before survey is saved set category.
   */
  @HandleBeforeSave
  public void beforeSave(final Survey survey) {
    if (survey.getSingleCategorySlug() != null) {
      survey.getCategories().clear();
      survey.addCategory(surveyCategoryDao.findOneBySlug(survey.getSingleCategorySlug()));
    }
  }

}
