package com.ydw.survey.services;

import com.ydw.common.rest.errorprocessing.ErrorAttribute;

public abstract class ResourceException extends RuntimeException {

  private static final long serialVersionUID = 256682528675378711L;

  @ErrorAttribute
  private final String code;

  public ResourceException(final String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }
}
