package com.ydw.survey.services;

import com.ydw.survey.model.TextFloatValue;

import org.springframework.expression.EvaluationException;
import org.springframework.expression.Operation;
import org.springframework.expression.OperatorOverloader;

public class QuestionAnswerValueOperationOverloader implements OperatorOverloader {

  /**
   * Perform overridden operation between two objects.
   */
  @Override
  public Object operate(final Operation operation, final Object leftOperand,
      final Object rightOperand) throws EvaluationException {
    Object result = null;
    if (checkOperandClasses(leftOperand, rightOperand, TextFloatValue.class, Number.class)) {
      final double leftOperandValue =
          TextFloatValue.class.isAssignableFrom(leftOperand.getClass())
              ? ((TextFloatValue) leftOperand).value : ((Number) leftOperand).intValue();
      final double rightOperandValue =
          TextFloatValue.class.isAssignableFrom(rightOperand.getClass())
              ? ((TextFloatValue) rightOperand).value : ((Number) rightOperand).doubleValue();
      final double resultDouble =
          performOverloadedOperation(operation, leftOperandValue, rightOperandValue);
      if ((int) resultDouble == resultDouble) {
        result = (int) resultDouble;
      } else {
        result = resultDouble;
      }
    }
    return result;
  }

  /**
   * Check if operation is overridden.
   */
  @Override
  public boolean overridesOperation(final Operation operation, final Object leftOperand,
      final Object rightOperand) throws EvaluationException {
    boolean result = false;
    result = checkOperandClasses(leftOperand, rightOperand, TextFloatValue.class, Number.class);
    return result;
  }

  /**
   * Check if both objects are assignable from any of given couple of classes.
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private boolean checkOperandClasses(final Object object1, final Object object2,
      final Class class1, final Class class2) {
    boolean result = false;
    if (object1 != null && object2 != null
        && (class1.isAssignableFrom(object1.getClass())
            && (class1.isAssignableFrom(object2.getClass())
                || class2.isAssignableFrom(object2.getClass()))
        || class2.isAssignableFrom(object1.getClass())
            && (class1.isAssignableFrom(object2.getClass())
                || class2.isAssignableFrom(object2.getClass())))) {
      result = true;
    }
    return result;
  }

  private double performOverloadedOperation(final Operation operation, final double leftOperand,
      final double rightOperand) {
    double result;
    switch (operation) {
      case ADD:
        result = leftOperand + rightOperand;
        break;
      case SUBTRACT:
        result = leftOperand - rightOperand;
        break;
      case DIVIDE:
        result = leftOperand / rightOperand;
        break;
      case MULTIPLY:
        result = leftOperand * rightOperand;
        break;
      case MODULUS:
        result = leftOperand % rightOperand;
        break;
      case POWER:
        result = (int) leftOperand ^ (int) rightOperand;
        break;
      default:
        result = 0;
    }
    return result;
  }
}
