package com.ydw.survey.services;

import static org.springframework.util.StringUtils.isEmpty;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.survey.dao.QuestionDao;
import com.ydw.survey.model.Question;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(Question.class)
@Transactional
public class QuestionRestEventHandler extends AbstractRestEventHandler {

  @Autowired
  private QuestionDao questionDao;

  /**
   * Copy question from source.
   */
  @HandleBeforeCreate
  public void beforeCreate(final Question question) {
    // check if not yet invoked from other listeners (section is null while copying)
    if (question.getCopyFromSlug() != 0 && null == question.getSection()) {
      final String title = question.getTitle().getValue();
      final long copyFromSlug = question.getCopyFromSlug();
      final Question sourceQuestion = questionDao.findById(copyFromSlug);
      BeanUtils.copyProperties(sourceQuestion, question);
      question.setVariableName(null);
      question.setCopyFromSlug(copyFromSlug);
      if (!isEmpty(title)) {
        question.setTitle(new LocalizedString(title));
      }
    }
    assignNextSortOrder(question);
  }

  private void assignNextSortOrder(final Question question) {
    if (null != question.getSection()) {
      question.setSortOrder(
          getNextSortOrder(questionDao.findMaxSortOrderBySection(question.getSection())));
    }
  }

}
