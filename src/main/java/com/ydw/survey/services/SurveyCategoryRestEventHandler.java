package com.ydw.survey.services;

import com.ydw.survey.dao.SurveyCategoryDao;
import com.ydw.survey.model.SurveyCategory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(SurveyCategory.class)
public class SurveyCategoryRestEventHandler {

  @Autowired
  private SurveyCategoryDao surveyCategoryDao;

  /**
   * Assign parent from parent slug.
   */
  @HandleBeforeCreate
  public void beforeCreate(final SurveyCategory surveyCategory) {
    if (!StringUtils.isEmpty(surveyCategory.getDefaultParentSlug())) {
      surveyCategory
          .setParent(surveyCategoryDao.findOneBySlug(surveyCategory.getDefaultParentSlug()));
    }
  }

  /**
   * Assign parent from parent slug.
   */
  @HandleBeforeSave
  public void beforeSave(final SurveyCategory surveyCategory) {
    if (!StringUtils.isEmpty(surveyCategory.getDefaultParentSlug())) {
      surveyCategory
          .setParent(surveyCategoryDao.findOneBySlug(surveyCategory.getDefaultParentSlug()));
    }
  }

}
