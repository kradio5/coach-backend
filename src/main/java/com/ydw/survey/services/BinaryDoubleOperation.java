package com.ydw.survey.services;

public interface BinaryDoubleOperation {
  double perform(double value1, double value2);
}
