package com.ydw.survey.services;

import com.ydw.survey.dao.SurveySectionDao;
import com.ydw.survey.model.SurveySection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(SurveySection.class)
@Transactional
public class SurveySectionRestEventHandler extends AbstractRestEventHandler {

  @Autowired
  private SurveySectionDao surveySectionDao;

  /**
   * Copy question from source.
   */
  @HandleBeforeCreate
  public void beforeCreate(final SurveySection section) {
    assignNextSortOrder(section);
  }

  private void assignNextSortOrder(final SurveySection section) {
    if (null != section.getSurvey()) {
      section.setSortOrder(
          getNextSortOrder(surveySectionDao.findMaxSortOrderBySurvey(section.getSurvey())));
    }
  }

}
