package com.ydw.survey.services;

import com.ydw.survey.dao.SurveyCalculationDao;
import com.ydw.survey.model.SurveyCalculation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(SurveyCalculation.class)
@Transactional
public class SurveyCalculationRestEventHandler extends AbstractRestEventHandler {

  @Autowired
  private SurveyCalculationDao surveyCalculationDao;

  @Autowired
  private SurveyEvalexCalculationService surveyEvalexCalculationService;

  /**
   * Before Survey Calculation is saved check if expression is valid.
   */
  @HandleBeforeCreate
  public void beforeCreate(final SurveyCalculation surveyCalculation) {
    surveyEvalexCalculationService.validateSurveyCalculation(surveyCalculation);
    assignNextSortOrder(surveyCalculation);
  }

  /**
   * Before Survey Calculation is saved check if expression is valid.
   */
  @HandleBeforeSave
  public void beforeSave(final SurveyCalculation surveyCalculation) {
    surveyEvalexCalculationService.validateSurveyCalculation(surveyCalculation);
  }

  private void assignNextSortOrder(final SurveyCalculation surveyCalculation) {
    if (null != surveyCalculation.getSurvey()) {
      surveyCalculation.setSortOrder(getNextSortOrder(
          surveyCalculationDao.findMaxSortOrderBySurvey(surveyCalculation.getSurvey())));
    }
  }

}
