package com.ydw.survey.services;

import static org.springframework.util.StringUtils.isEmpty;

import com.ydw.survey.model.HasVariable;
import com.ydw.survey.model.HasVariableName;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;

import com.udojava.evalex.Expression;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SurveyEvalexCalculator {

  private static final double VALUE_ACCURACY = 0.00000001;

  public static final String CALC_RESULT_NOT_APPLICABLE = "N/A";

  private static final int DUMMY_VALUE = 1;

  private static final String CONDITION_TRUE = "1";

  private static final String CONDITION_FALSE = "0";

  private final Pattern variablePattern = Pattern.compile("#(\\w*)");

  private final Pattern expressionPattern = Pattern.compile("\\{\\{(.*?(?=\\}\\}))\\}\\}");

  private boolean validate;

  private boolean skipExpressions;

  private final Map<String, Object> calculationVariables = new HashMap<>();

  /**
   * Initializes calculator with data required for calculation of survey result.
   *
   * @param validate - validate template only, in case of errors throw exception <br />
   *        if validate is false then in case of errors "N/A" is returned as result
   * @param skipExpressions - if true then Evalex expressions are not executed, just variables are
   *        replaced with values
   * @param variables - variables used in template with prefix #
   */
  public SurveyEvalexCalculator(final boolean validate, final boolean skipExpressions,
      final Object... variables) {
    this.validate = validate;
    this.skipExpressions = skipExpressions;
    if (variables != null) {
      final int variablePairLength = variables.length / 2;
      for (int index = 0; index < variablePairLength; index++) {
        final int variableIndex = index * 2;
        final Object variableName = variables[variableIndex];
        final Object variableValue = variables[variableIndex + 1];
        if (null != variableName) {
          calculationVariables.put(variableName.toString(), variableValue);
        }
      }
    }
  }

  public SurveyEvalexCalculator(final boolean validate, final Object... variables) {
    this(validate, false, variables);
  }

  /**
   * Initializes calculator with data required for calculation of survey result.
   */
  public SurveyEvalexCalculator(final List<Question> questions,
      final List<QuestionAnswer> questionAnswers,
      final List<SurveyCalculation> surveyCalculations) {
    prepareVariables(questionAnswers);
  }

  /**
   * Initializes calculator with data required for calculation of survey result.
   */
  public SurveyEvalexCalculator(final List<Question> questions,
      final List<SurveyCalculation> surveyCalculations) {
    prepareDummyVariables(questions);
    prepareDummyVariables(surveyCalculations);
    validate = true;
  }

  public boolean isValidate() {
    return validate;
  }

  /**
   * Calculate survey result calculations.
   */
  public List<SurveyResultCalculation> performSurveyResultCalculations(
      final SurveyResult surveyResult, final List<SurveyCalculation> surveyCalculations) {

    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return null;
    }

    final List<SurveyResultCalculation> surveyResultCalculations = new ArrayList<>();
    for (final SurveyCalculation surveyCalculation : surveyCalculations) {
      if (surveyCalculation.isDeleted()) {
        continue;
      }
      final String resultCalculation = performSurveyResultCalculation(surveyCalculation);
      Object value = null;
      if (null != resultCalculation) {
        final SurveyResultCalculation surveyResultCalculation =
            new SurveyResultCalculation(surveyResult, surveyCalculation);
        surveyResultCalculation.setResultCalculation(resultCalculation);
        surveyResultCalculations.add(surveyResultCalculation);
        value = surveyResultCalculation.getValue();
      }
      if (!isEmpty(surveyCalculation.getVariableName())) {
        // register variable to use in the next calculations
        if (value == null) {
          value = CALC_RESULT_NOT_APPLICABLE;
        }
        calculationVariables.put(surveyCalculation.getVariableName(), value);
      }
    }
    return surveyResultCalculations;
  }

  /**
   * Calculate survey calculations.
   */
  public void performSurveyValidations(final List<SurveyCalculation> surveyCalculations) {

    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return;
    }

    for (final SurveyCalculation surveyCalculation : surveyCalculations) {
      if (surveyCalculation.isDeleted()) {
        continue;
      }
      performSurveyValidation(surveyCalculation);
    }
  }

  /**
   * Calculate expression (template).<br />
   * For testing purpose.
   */
  public String performTemplateCalculation(final String calculationTemplate) {
    return calculateTemplate(calculationTemplate);
  }

  /**
   * Calculate Evalex expression.
   */
  private String calculateEvalexExpression(String evalexExpression) {

    // check that values exist for all variables
    final Matcher matcher = variablePattern.matcher(evalexExpression);
    while (matcher.find()) {
      final String variableName = matcher.group(1);
      if (calculationVariables.get(variableName) == null) {
        throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE,
            variableName);
      } else {
        // remove "#" in variable
        evalexExpression = evalexExpression.replaceFirst("#" + variableName, variableName);
      }
    }

    try {
      // calculate expression
      final Expression expression = new Expression(evalexExpression);
      if (!calculationVariables.isEmpty()) {
        for (final Entry<String, Object> variable : calculationVariables.entrySet()) {
          expression.and(variable.getKey(), variable.getValue().toString());
        }
      }
      final BigDecimal result = expression.eval();
      String resultStr;
      if (Math.abs(result.longValue() - result.doubleValue()) < VALUE_ACCURACY) {
        resultStr = String.valueOf(result.longValue());
      } else {
        resultStr = String.valueOf(result.doubleValue());
      }
      return resultStr;
    } catch (final Exception exc) {
      throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_EXPRESSION,
          evalexExpression);
    }
  }

  /**
   * Calculate template.<br />
   * <br />
   * Template is considered as tree where expressions can include other expressions and / or
   * variables and also variables can be used beyond any expression.
   */
  private String calculateTemplate(final String calculationTemplate) {

    if (isEmpty(calculationTemplate)) {
      return calculationTemplate;
    }

    String calculationResult;
    try {
      final StringBuilder calculationResultBuilder = new StringBuilder(calculationTemplate);
      if (skipExpressions) { // just replace variables with values
        calculateWithoutExpressions(calculationTemplate, calculationResultBuilder);
      } else {
        // calculate expressions
        calculateWithExpressions(calculationTemplate, calculationResultBuilder);
        // replace left variables with values
        calculateWithoutExpressions(calculationResultBuilder.toString(),
            calculationResultBuilder);
      }
      calculationResult = calculationResultBuilder.toString();
    } catch (final CalculationException calcexc) {
      if (!validate) {
        // expression calculation is wrong
        calculationResult = CALC_RESULT_NOT_APPLICABLE; // all calculation result is N/A
      } else {
        throw calcexc;
      }
    }

    String result;
    try {
      // try calculation final result as top level expression
      if (!skipExpressions) {
        if (CALC_RESULT_NOT_APPLICABLE
            .equals(result = calculateEvalexExpression(calculationResult))) {
          // top level is not a expression, return as it is
          result = calculationResult;
        }
      } else {
        result = calculationResult;
      }
    } catch (final Exception exc) {
      // top level is not a expression, return as it is
      result = calculationResult;
    }
    return result;
  }

  private void calculateWithExpressions(final String calculationTemplate,
      final StringBuilder calculationResultBuilder) {
    int indexCorrection = 0;
    final Matcher matcher = expressionPattern.matcher(calculationTemplate);
    while (matcher.find()) {
      final String expression = matcher.group(1);
      String expressionCalculation;
      try {
        expressionCalculation = calculateEvalexExpression(expression);
      } catch (final CalculationException exc) {
        if (SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_EXPRESSION.equals(exc.getCode())) {
          // replace variables in expression with values
          final StringBuilder calculationExpressionBuilder = new StringBuilder(expression);
          calculateWithoutExpressions(expression, calculationExpressionBuilder);
          expressionCalculation = calculationExpressionBuilder.toString();
        } else {
          throw exc;
        }
      }
      calculationResultBuilder.replace(matcher.start() + indexCorrection,
          matcher.end() + indexCorrection, expressionCalculation);
      indexCorrection += expressionCalculation.length() - (matcher.end() - matcher.start());
    }
  }

  private void calculateWithoutExpressions(final String calculationTemplate,
      final StringBuilder calculationResultBuilder) {
    final Matcher matcher = variablePattern.matcher(calculationTemplate);
    while (matcher.find()) {
      final String variableName = matcher.group(1);
      final Object variableValue = calculationVariables.get(variableName);
      if (variableValue == null) {
        throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE,
            variableName);
      } else {
        // replace variable with its value
        final int variableStartIndex = calculationResultBuilder.indexOf("#" + variableName);
        calculationResultBuilder.replace(variableStartIndex,
            variableStartIndex + variableName.length() + 1, variableValue.toString());
      }
    }
  }

  private boolean checkCondition(final String condition, final boolean validate) {
    // check that condition is valid
    String conditionResult = calculateTemplate(condition);
    if (!isEmpty(conditionResult)) {
      conditionResult = conditionResult.toString().toLowerCase();
    }
    if (validate && !CONDITION_TRUE.equals(conditionResult)
        && !CONDITION_FALSE.equals(conditionResult)) {
      throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_CONDITION);
    }
    return CONDITION_TRUE.equals(conditionResult);
  }

  private String performSurveyResultCalculation(final SurveyCalculation surveyCalculation) {
    String resultCalculation = null;
    // check condition
    boolean condition;
    if (!isEmpty(surveyCalculation.getCondition())) {
      condition = checkCondition(surveyCalculation.getCondition(), false);
    } else {
      condition = true;
    }
    if (condition && null != surveyCalculation.getTemplate()) {
      final String calculationTemplate = surveyCalculation.getTemplate().getValue();
      resultCalculation = calculateTemplate(calculationTemplate);
    }
    return resultCalculation;
  }

  private void performSurveyValidation(final SurveyCalculation surveyCalculation) {
    final String condition = surveyCalculation.getCondition();
    if (!isEmpty(condition)) {
      checkCondition(condition, true);
    }
    // check that template is valid
    if (null != surveyCalculation.getTemplate()) {
      final String calculationTemplate = surveyCalculation.getTemplate().getValue();
      calculateTemplate(calculationTemplate);
    }
    if (!isEmpty(surveyCalculation.getVariableName())) {
      // register variable to use in the next calculations
      calculationVariables.put(surveyCalculation.getVariableName(), DUMMY_VALUE);
    }
  }

  private <T extends HasVariableName> void prepareDummyVariables(
      final List<T> variableHolders) {
    if (null != variableHolders && !variableHolders.isEmpty()) {
      for (final HasVariableName variableHolder : variableHolders) {
        final String variableName = variableHolder.getVariableName();
        if (null == variableName) {
          continue;
        }
        calculationVariables.put(variableHolder.getVariableName(), DUMMY_VALUE);
      }
    }
  }

  private <T extends HasVariable> void prepareVariables(final List<T> variableValueHolders) {
    if (null != variableValueHolders && !variableValueHolders.isEmpty()) {
      for (final HasVariable variableValueHolder : variableValueHolders) {
        final String variableName = variableValueHolder.getVariableHolder().getVariableName();
        if (null == variableName) {
          continue;
        }
        calculationVariables.put(variableName, variableValueHolder.getValue());
      }
    }
  }

}
