package com.ydw.survey.services;

import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.search.dao.SurveySearchDao;
import com.ydw.survey.search.model.SurveySearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Internal operations on {@link Survey} entities.
 *
 * @author Andrew Usov
 */
@Service
@Transactional
public class SurveyService {

  @Autowired
  private SurveyCampaignDao campaignDao;

  @Autowired
  private SurveyResultDao resultDao;

  @Autowired
  private SurveyDao surveyDao;

  @Autowired
  private SurveyEvalexCalculationService surveyEvalexCalculationService;

  @Autowired
  private SurveySearchDao surveySearchDao;

  /**
   * Performs actions after entity is created or saved.
   */
  public void afterCreate(final Survey entity) {
    surveySearchDao.save(new SurveySearch(entity));
  }

  /**
   * Performs after-save operations.
   */
  public void afterSave(final Survey entity) {
    if (entity.isValidate()) {
      surveyEvalexCalculationService.validateSurveyCalculations(entity);
    }
    surveySearchDao.save(new SurveySearch(entity));
  }

  /**
   * Performs required actions before delete.
   */
  public void beforeDelete(final Survey survey) {
    surveySearchDao.delete(survey.getId());
  }

  /**
   * Deletes an entity from DB and search index.
   */
  public void delete(final Survey entity) {
    beforeDelete(entity);
    surveyDao.delete(entity);
  }

  /**
   * Deletes all surveys and campaigns.
   */
  public void deleteAll() {
    resultDao.deleteAll();
    campaignDao.deleteAll();
    surveyDao.deleteAll();
    surveySearchDao.deleteAll();
  }

  /**
   * Saves entity to DB and search index.
   */
  public <S extends Survey> S save(final S entity) {
    final boolean isNew = entity.isNew();
    final S saved = surveyDao.save(entity);
    if (isNew) {
      afterCreate(saved);
    } else {
      afterSave(saved);
    }
    return saved;
  }

}
