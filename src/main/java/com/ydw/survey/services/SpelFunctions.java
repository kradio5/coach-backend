package com.ydw.survey.services;

import com.ydw.survey.annotations.SpelFunction;
import com.ydw.survey.model.IDoubleValue;

import org.apache.commons.lang3.ArrayUtils;

public class SpelFunctions {

  /**
   * Returns average of n values.<br />
   * <br />
   * Considered in SPEL as AVERAGE function
   */
  @SpelFunction(name = "AVERAGE")
  public static Object average(final Object... values) {
    final BinaryDoubleOperation sumOperation = (value1, value2) -> (value1 + value2);
    final double operationResult = binaryDoubleOperation(sumOperation, values)
        / (values != null && values.length > 0 ? values.length : 1);
    return convertToInt(operationResult);
  }

  /**
   * Checks expression and returns result or otherwise objects.<br />
   * <br />
   * Considered in SPEL as IF function
   */
  @SpelFunction(name = "IF")
  public static Object ifCondition(final boolean expression, final Object result,
      final Object... otherwise) {
    return expression ? result : ArrayUtils.isEmpty(otherwise) ? null : otherwise[0];
  }

  /**
   * Returns max from n values.<br />
   * <br />
   * Considered in SPEL as MAX function
   */
  @SpelFunction(name = "MAX")
  public static double max(final double... values) {
    final BinaryDoubleOperation maxOperation = (value1, value2) -> Math.max(value1, value2);
    return binaryDoubleOperation(maxOperation, values);
  }

  /**
   * Returns min from n values.<br />
   * <br />
   * Considered in SPEL as MIN function
   */
  @SpelFunction(name = "MIN")
  public static double min(final double... values) {
    final BinaryDoubleOperation minOperation = (value1, value2) -> Math.min(value1, value2);
    return binaryDoubleOperation(minOperation, values);
  }

  /**
   * Returns INTEGER of a value.<br />
   * <br />
   * Considered in SPEL as NUMBER function
   */
  @SpelFunction(name = "NUMBER")
  public static int number(final Object value) {
    return (int) convertToDouble(value);
  }

  /**
   * Compares object to odd elements of array and returns following even value.<br />
   * If no matches is found then the last odd value is returned (or null if array length is even).
   * <br />
   * Considered in SPEL as SWITCH function
   *
   * @param valuePairs [compareTo1, result if sourceObject equals to compareTo1, compareTo2, result
   *        if sourceObject equals to compareTo2, ... , otherwise (optional)]
   */
  @SpelFunction(name = "SWITCH")
  public static Object switchCondition(final Object sourceObject, final Object... valuePairs) {
    if (ArrayUtils.isEmpty(valuePairs) || valuePairs.length < 2) {
      throw new IllegalStateException("Invalid number of arguments in SWITCH condition");
    }
    final int valuePairsLength = valuePairs.length - valuePairs.length % 2;
    int index = 0;
    Object result = null;
    for (; index < valuePairsLength; index++) {
      if (compareObjects(sourceObject, valuePairs[index++])) {
        result = valuePairs[index];
        break;
      }
    }
    if (null == result) {
      result = valuePairsLength < valuePairs.length ? valuePairs[valuePairs.length - 1] : null;
    }
    return result;
  }

  private static double binaryDoubleOperation(final BinaryDoubleOperation doubleOperation,
      final Object... values) {
    int valuesLength;
    if (null == values || (valuesLength = values.length) == 0) {
      return 0d;
    }
    double result = convertToDouble(values[0]);
    for (int index = 1; index < valuesLength; index++) {
      result = doubleOperation.perform(result, convertToDouble(values[index]));
    }
    return result;
  }

  private static boolean compareObjects(final Object sourceObject,
      final Object conditionObject) {
    return null == sourceObject && null == conditionObject || null != sourceObject
        && null != conditionObject && (sourceObject.equals(conditionObject)
            || sourceObject.toString().equals(conditionObject.toString()));
  }

  private static double convertToDouble(final Object object) {
    return object == null ? 0D
        : Number.class.isAssignableFrom(object.getClass()) ? ((Number) object).doubleValue()
            : IDoubleValue.class.isAssignableFrom(object.getClass())
                ? ((IDoubleValue) object).getValue() : Double.parseDouble(object.toString());
  }

  private static Object convertToInt(final double operationResult) {
    Object result;
    if ((int) operationResult == operationResult) {
      result = (int) operationResult;
    } else {
      result = operationResult;
    }
    return result;
  }

}
