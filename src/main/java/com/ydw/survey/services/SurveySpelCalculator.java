package com.ydw.survey.services;

import static org.springframework.util.StringUtils.isEmpty;

import com.ydw.survey.model.HasVariable;
import com.ydw.survey.model.HasVariableName;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;

import org.springframework.expression.AccessException;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SurveySpelCalculator {

  public static final String CALC_RESULT_NOT_APPLICABLE = "N/A";

  private static final int DUMMY_VALUE = 1;

  private static final String EXPRESSION_START = "{{";

  private static final String EXPRESSION_END = "}}";

  private static final String CONDITION_TRUE = "true";

  private static final String CONDITION_FALSE = "false";

  private static final String SPEL_FUNCTION_PATTERN_PLACEHOLDER = "_FUNCTIONS_";

  private static final String SPEL_FUNCTION_PATTERN =
      "(\\W|^)(" + SPEL_FUNCTION_PATTERN_PLACEHOLDER + ")\\s?\\({1}";

  private final Pattern variablePattern = Pattern.compile("#(\\w*)");

  private Map<String, Method> spelFunctions;

  private Pattern spelFunctionsPattern;

  private boolean validate;

  private final Map<String, Object> calculationVariables = new HashMap<>();

  /**
   * Initializes calculator with data required for calculation of survey result.
   */
  public SurveySpelCalculator(final Map<String, Method> spelFunctions, final boolean validate,
      final Object... variables) {
    setSpelFunctions(spelFunctions);
    this.validate = validate;
    if (variables != null) {
      final int variablePairLength = variables.length / 2;
      for (int index = 0; index < variablePairLength; index++) {
        final int variableIndex = index * 2;
        final Object variableName = variables[variableIndex];
        final Object variableValue = variables[variableIndex + 1];
        if (null != variableName) {
          calculationVariables.put(variableName.toString(), variableValue);
        }
      }
    }
  }

  /**
   * Initializes calculator with data required for calculation of survey result.
   */
  public SurveySpelCalculator(final Map<String, Method> spelFunctions,
      final List<Question> questions, final List<QuestionAnswer> questionAnswers,
      final List<SurveyCalculation> surveyCalculations) {
    setSpelFunctions(spelFunctions);
    prepareVariables(questionAnswers);
  }

  /**
   * Initializes calculator with data required for calculation of survey result.
   */
  public SurveySpelCalculator(final Map<String, Method> spelFunctions,
      final List<Question> questions, final List<SurveyCalculation> surveyCalculations) {
    setSpelFunctions(spelFunctions);
    prepareDummyVariables(questions);
    prepareDummyVariables(surveyCalculations);
    validate = true;
  }

  public boolean isValidate() {
    return validate;
  }

  /**
   * Calculate survey result calculations.
   */
  public List<SurveyResultCalculation> performSurveyResultCalculations(
      final SurveyResult surveyResult, final List<SurveyCalculation> surveyCalculations) {

    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return null;
    }

    final StandardEvaluationContext evaluationContext = createEvaluationContext();

    final List<SurveyResultCalculation> surveyResultCalculations = new ArrayList<>();
    for (final SurveyCalculation surveyCalculation : surveyCalculations) {
      if (surveyCalculation.isDeleted()) {
        continue;
      }
      final String resultCalculation =
          performSurveyResultCalculation(evaluationContext, surveyCalculation);
      Object value = null;
      if (null != resultCalculation) {
        final SurveyResultCalculation surveyResultCalculation =
            new SurveyResultCalculation(surveyResult, surveyCalculation);
        surveyResultCalculation.setResultCalculation(resultCalculation);
        surveyResultCalculations.add(surveyResultCalculation);
        value = surveyResultCalculation.getValue();
      }
      if (!isEmpty(surveyCalculation.getVariableName())) {
        // register variable to use in the next calculations
        if (value == null) {
          value = CALC_RESULT_NOT_APPLICABLE;
        }
        calculationVariables.put(surveyCalculation.getVariableName(), value);
        evaluationContext.setVariable(surveyCalculation.getVariableName(), value);
      }
    }
    return surveyResultCalculations;
  }

  /**
   * Calculate survey calculations.
   */
  public void performSurveyValidations(final List<SurveyCalculation> surveyCalculations) {

    if (null == surveyCalculations || surveyCalculations.isEmpty()) {
      return;
    }

    final StandardEvaluationContext evaluationContext = createEvaluationContext();

    for (final SurveyCalculation surveyCalculation : surveyCalculations) {
      if (surveyCalculation.isDeleted()) {
        continue;
      }
      performSurveyValidation(evaluationContext, surveyCalculation);
    }
  }

  /**
   * Calculate expression (template).<br />
   * For testing purpose.
   */
  public String performTemplateCalculation(final String calculationTemplate) {
    return calculateTemplate(createEvaluationContext(), calculationTemplate);
  }

  public void setValidate(final boolean validate) {
    this.validate = validate;
  }

  /**
   * Calculate block surrounded by EXPRESSION_START and EXPRESSION_END and push result to the upper
   * block.
   */
  private void calculateCurrentLevel(final StandardEvaluationContext evaluationContext,
      final Map<Integer, StringBuilder> expressionByLevel, final int currentBlockIndex) {

    final int upperBlockIndex = currentBlockIndex - 1;
    final String expression = expressionByLevel.get(currentBlockIndex).toString();

    // validate variables
    final Matcher matcher = variablePattern.matcher(expression);
    while (matcher.find()) {
      final String variableName = matcher.group(1);
      if (evaluationContext.lookupVariable(variableName) == null) {
        throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE,
            variableName);
      }
    }

    // if (calculateAsExpression) {
    // calculate expression
    final String expressionValue;
    try {
      final String preProcessedFormula = preProcessSpelExpression(expression);
      expressionValue = calculateSpelExpression(evaluationContext, preProcessedFormula);
    } catch (final Exception exc) {
      throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_EXPRESSION,
          expression);
    }
    expressionByLevel.get(upperBlockIndex).append(expressionValue);
    // }
  }

  /**
   * Calculate SpEL expression.
   */
  private String calculateSpelExpression(final StandardEvaluationContext evaluationContext,
      final String spelExpression) {
    // calculate expression
    final ExpressionParser parser = new SpelExpressionParser();
    final Expression expression = parser.parseExpression(spelExpression);
    final Object calcResultObj = expression.getValue(evaluationContext);
    final String calcResult =
        null != calcResultObj ? calcResultObj.toString() : CALC_RESULT_NOT_APPLICABLE;
    return calcResult;
  }

  /**
   * Calculate template.<br />
   * <br />
   * Template is considered as tree where expressions can include other expressions and / or
   * variables and also variables can be used beyond any expression.
   */
  private String calculateTemplate(final StandardEvaluationContext evaluationContext,
      final String calculationTemplate) {

    if (isEmpty(calculationTemplate)) {
      return calculationTemplate;
    } else {
      String calculationResult = null;
      final String[] templateSplit =
          split(calculationTemplate, true, EXPRESSION_START, EXPRESSION_END);
      final int templateSplitLength = templateSplit.length;
      if (templateSplitLength > 1) {
        int level = 0;
        final Map<Integer, StringBuilder> expressionByLevel = new HashMap<>();
        expressionByLevel.put(0, new StringBuilder());
        for (int templateItemIndex =
            0; templateItemIndex < templateSplitLength; templateItemIndex++) {
          final String templateItem = templateSplit[templateItemIndex];

          if (EXPRESSION_START.equals(templateItem)) {

            if (++level > 1) {
              break; // wrong template format (spel in spel)
            }

            // start of expression
            final StringBuilder expression = expressionByLevel.get(level);
            if (expression == null) {
              expressionByLevel.put(level, new StringBuilder());
            } else {
              expression.delete(0, expression.length());
            }

          } else if (EXPRESSION_END.equals(templateItem)) {

            if (--level < 0) {
              break; // wrong template format
            }
            // end of expression
            try {
              calculateCurrentLevel(evaluationContext, expressionByLevel, level + 1);
            } catch (final CalculationException calcexc) {
              if (!validate && (SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE
                  .equals(calcexc.getCode())
                  || SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_EXPRESSION
                      .equals(calcexc.getCode()))) {
                // expression calculation is wrong
                calculationResult = CALC_RESULT_NOT_APPLICABLE; // all calculation result is N/A
                break; // stop calculation
              } else {
                throw calcexc;
              }
            }

          } else {
            expressionByLevel.get(level).append(templateItem);
          }
        }

        if (isEmpty(calculationResult)) {
          if (level != 0) {
            // wrong template format
            if (validate) {
              throw new CalculationException(
                  SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_TEMPLATE);
            } else {
              calculationResult = CALC_RESULT_NOT_APPLICABLE;
            }
          } else {
            calculationResult = expressionByLevel.get(0).toString();
          }
        }
      } else {
        calculationResult = calculationTemplate;
      }
      String result;
      try {
        // try calculation final result as top level expression
        if (CALC_RESULT_NOT_APPLICABLE
            .equals(result = calculateSpelExpression(evaluationContext, calculationResult))) {
          // top level is not a expression, return as it is
          result = calculationResult;
        }
      } catch (final Exception exc) {
        // top level is not a expression, return as it is
        result = calculationResult;
      }
      return result;
    }

  }

  private boolean checkCondition(final StandardEvaluationContext evaluationContext,
      final String condition, final boolean validate) {
    // check that condition is valid
    String conditionResult = calculateTemplate(evaluationContext, condition);
    if (!isEmpty(conditionResult)) {
      conditionResult = conditionResult.toString().toLowerCase();
    }
    if (validate && !CONDITION_TRUE.equals(conditionResult)
        && !CONDITION_FALSE.equals(conditionResult)) {
      throw new CalculationException(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_CONDITION);
    }
    return Boolean.parseBoolean(conditionResult);
  }

  /**
   * Create context for calculation and register there pre-defined functions (from
   * CalculatorFunctions) and variables. Deny access for using class instantiations and methods
   */
  private StandardEvaluationContext createEvaluationContext() {
    final StandardEvaluationContext evaluationContext = new StandardEvaluationContext();
    // do not allow create new objects
    evaluationContext.addConstructorResolver((context, typeName, argumentTypes) -> {
      throw new AccessException("constructors are not allowed");
    });
    // do not allow execution methods on objects
    evaluationContext.addMethodResolver((context, targetObject, name, argumentTypes) -> {
      throw new AccessException("methods are not allowed");
    });
    // overload operators for some objects that could be considered as primitive types
    evaluationContext.setOperatorOverloader(new QuestionAnswerValueOperationOverloader());
    if (null != spelFunctions && !spelFunctions.isEmpty()) {
      for (final Entry<String, Method> function : spelFunctions.entrySet()) {
        evaluationContext.registerFunction(function.getKey(), function.getValue());
      }
    }
    for (final Entry<String, Object> calculationVariable : calculationVariables.entrySet()) {
      evaluationContext.setVariable(calculationVariable.getKey(),
          calculationVariable.getValue());
    }
    return evaluationContext;
  }

  private String performSurveyResultCalculation(
      final StandardEvaluationContext evaluationContext,
      final SurveyCalculation surveyCalculation) {
    String resultCalculation = null;
    // check condition
    boolean condition;
    if (!isEmpty(surveyCalculation.getCondition())) {
      condition = checkCondition(evaluationContext, surveyCalculation.getCondition(), false);
    } else {
      condition = true;
    }
    if (condition && null != surveyCalculation.getTemplate()) {
      final String calculationTemplate = surveyCalculation.getTemplate().getValue();
      resultCalculation = calculateTemplate(evaluationContext, calculationTemplate);
    }
    return resultCalculation;
  }

  private void performSurveyValidation(final StandardEvaluationContext evaluationContext,
      final SurveyCalculation surveyCalculation) {
    final String condition = surveyCalculation.getCondition();
    if (!isEmpty(condition)) {
      checkCondition(evaluationContext, condition, true);
    }
    // check that template is valid
    if (null != surveyCalculation.getTemplate()) {
      final String calculationTemplate = surveyCalculation.getTemplate().getValue();
      calculateTemplate(evaluationContext, calculationTemplate);
    }
    if (!isEmpty(surveyCalculation.getVariableName())) {
      // register variable to use in the next calculations
      calculationVariables.put(surveyCalculation.getVariableName(), DUMMY_VALUE);
      evaluationContext.setVariable(surveyCalculation.getVariableName(), DUMMY_VALUE);
    }
  }

  private <T extends HasVariableName> void prepareDummyVariables(
      final List<T> variableHolders) {
    if (null != variableHolders && !variableHolders.isEmpty()) {
      for (final HasVariableName variableHolder : variableHolders) {
        final String variableName = variableHolder.getVariableName();
        if (null == variableName) {
          continue;
        }
        calculationVariables.put(variableHolder.getVariableName(), DUMMY_VALUE);
      }
    }
  }

  private void prepareSpeFunctionPattern() {
    if (spelFunctions.isEmpty()) {
      return;
    }
    final StringBuilder spelFunctionsStr = new StringBuilder();
    for (final String spelFunction : spelFunctions.keySet()) {
      spelFunctionsStr.append(spelFunction);
      spelFunctionsStr.append('|');
    }
    spelFunctionsStr.deleteCharAt(spelFunctionsStr.length() - 1);
    spelFunctionsPattern = Pattern.compile(SPEL_FUNCTION_PATTERN
        .replaceAll(SPEL_FUNCTION_PATTERN_PLACEHOLDER, spelFunctionsStr.toString()));
  }

  private <T extends HasVariable> void prepareVariables(final List<T> variableValueHolders) {
    if (null != variableValueHolders && !variableValueHolders.isEmpty()) {
      for (final HasVariable variableValueHolder : variableValueHolders) {
        final String variableName = variableValueHolder.getVariableHolder().getVariableName();
        if (null == variableName) {
          continue;
        }
        calculationVariables.put(variableName, variableValueHolder.getValue());
      }
    }
  }

  /**
   * Add '#' before each spel function.
   */
  private String preProcessSpelExpression(String expression) {
    if (!spelFunctions.isEmpty()) {
      int insertAmount = 0;
      final Matcher matcher = spelFunctionsPattern.matcher(expression);
      final StringBuilder expressionBuilder = new StringBuilder(expression);
      while (matcher.find()) {
        final int spelFunctionIndex = matcher.start(1);
        expressionBuilder.insert(spelFunctionIndex + insertAmount++, '#');
      }
      expression = expressionBuilder.toString();
    }
    return expression;
  }

  private void setSpelFunctions(final Map<String, Method> spelFunctions) {
    this.spelFunctions = null != spelFunctions ? spelFunctions : Collections.emptyMap();
    prepareSpeFunctionPattern();
  }

  /**
   * Split string to string array basing on delimeters array.<br />
   * Consider nested cases (e.g. _start_ ... _start_ ... _end_ ... _end_).
   */
  private String[] split(final String str, final boolean withDelimeter,
      final String... delimiter) {
    if (str == null) {
      return new String[0];
    }
    if (delimiter == null || delimiter.length == 0) {
      return new String[] { str };
    }
    final List<String> result = new ArrayList<String>();
    final int stringLength = str.length();
    final int delimeterLength = delimiter.length;
    int position = 0;
    while (true) {
      String closestDelimeter = null;
      int closestDelimeterPosition = stringLength;
      for (int delimeterIndex = 0; delimeterIndex < delimeterLength; delimeterIndex++) {
        int delimeterPosition;
        if ((delimeterPosition = str.indexOf(delimiter[delimeterIndex], position)) != -1
            && delimeterPosition < closestDelimeterPosition) {
          closestDelimeterPosition = delimeterPosition;
          closestDelimeter = delimiter[delimeterIndex];
        }
      }
      if (closestDelimeter != null) {
        if (position < closestDelimeterPosition) {
          result.add(str.substring(position, closestDelimeterPosition));
        }
        if (withDelimeter) {
          result.add(closestDelimeter);
        }
        position = closestDelimeterPosition + closestDelimeter.length();
      } else {
        if (stringLength > 0 && position < stringLength) {
          // Add rest of String, but not in case of empty input.
          result.add(str.substring(position));
        }
        break;
      }
    }
    return StringUtils.toStringArray(result);
  }

}
