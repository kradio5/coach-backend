package com.ydw.survey.services;

import com.ydw.common.rest.errorprocessing.ErrorAttribute;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT) // 409
public class CalculationException extends ResourceException {

  private static final long serialVersionUID = -7021616839403341848L;

  @ErrorAttribute
  private final String details;

  public CalculationException(final String code) {
    super(code);
    this.details = null;
  }

  public CalculationException(final String code, final String details) {
    super(code);
    this.details = details;
  }

  public String getDetails() {
    return details;
  }
}
