package com.ydw.survey.services;

import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.initializer.SurveyResultDataPopulator;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class SurveyResultService {

  private static final Logger LOG = LoggerFactory.getLogger(SurveyResultService.class);

  private final SurveyCampaignDao surveyCampaignDao;

  private final SurveyDao surveyDao;

  private final SurveyResultDao surveyResultDao;

  /**
   * Instantiates service.
   */
  @Autowired
  public SurveyResultService(final SurveyCampaignDao surveyCampaignDao,
      final SurveyDao surveyDao,
      final SurveyResultDao surveyResultDao,
      final SurveyResultDataPopulator surveyResultDataPopulator) {
    this.surveyCampaignDao = surveyCampaignDao;
    this.surveyDao = surveyDao;
    this.surveyResultDao = surveyResultDao;
  }

  /**
   * Assign survey to survey result basing on campaign.
   */
  public void assignSurveyResultByCampaign(final SurveyResult surveyResult) {
    if (surveyResult.getSurvey() == null && surveyResult.getSurveyCampaign() != null) {
      final List<SurveyResult> surveyResults =
          surveyResultDao.findBySurveyCampaignAndCreatedByOrderByCreated(
              surveyResult.getSurveyCampaign(), surveyResult.getCreatedBy());
      final List<Survey> surveys = surveyResult.getSurveyCampaign().getSurveys();
      if (surveyResults.size() < surveys.size()) {
        surveyResult.setSurvey(surveys.get(surveyResults.size()));
        if (surveyResults.size() == surveys.size() - 1) {
          surveyResult.setLastInCampaign(true);
        }
      } else {
        throw new InvalidResourceException(SurveyErrorCodes.SURVEY_CAMPAIGN_LOCKED);
      }
    }
  }

  /**
   * Find current survey result to be answered for the campaign.
   *
   * @param surveyCampaignSlug - reference to campaign
   * @param accountId - identifier of the user
   */
  public SurveyResult findBySurveyCampaignSlugAndCreatedBy(final String surveyCampaignSlug,
      final String accountId) {

    final SurveyCampaign surveyCampaign = getActiveSurveyCampaign(surveyCampaignSlug);
    if (surveyCampaign == null) {
      throw new ResourceNotFoundException(SurveyErrorCodes.SURVEY_CAMPAIGN_NOT_FOUND);
    }
    // find current survey result
    final List<SurveyResult> surveyResults = surveyResultDao
        .findBySurveyCampaignAndCreatedByOrderByCreated(surveyCampaign, accountId);
    int completedSurveyAmount = 0;
    int lastCampaignAnswerCounter = 0;
    SurveyResult currentSurveyResult = null;
    for (final SurveyResult surveyResult : surveyResults) {
      if (!surveyResult.isCompleted()) {
        // if surveyResult started but not completed or values to be changed
        currentSurveyResult = surveyResult;
      } else if (surveyResult.getCampaignAnswerCounter() < lastCampaignAnswerCounter) {
        currentSurveyResult = surveyResult;
      } else {
        completedSurveyAmount++;
        lastCampaignAnswerCounter = surveyResult.getCampaignAnswerCounter();
      }
      if (currentSurveyResult != null) {
        LOG.info("found survey result: " + currentSurveyResult.getSlug());
        break;
      }
    }
    if (currentSurveyResult == null) {
      if (surveyCampaign.getSurveys().size() == completedSurveyAmount) {
        if (surveyCampaign.isAllowAnswerChanges()) {
          // start again from the first survey
          currentSurveyResult = surveyResults.get(0);
        } else {
          throw new InvalidResourceException(SurveyErrorCodes.SURVEY_CAMPAIGN_LOCKED);
        }
      } else {
        throw new ResourceNotFoundException(SurveyErrorCodes.SURVEY_RESULT_NOT_FOUND);
      }
    }
    // if current survey is last in this campaign
    if (currentSurveyResult != null && currentSurveyResult.getSurvey()
        .equals(surveyCampaign.getSurveys().get(surveyCampaign.getSurveys().size() - 1))) {
      currentSurveyResult.setLastInCampaign(true);
    }

    return currentSurveyResult;
  }

  /**
   * Find current survey result to be answered for the campaign.<br />
   * Convert result to resource (must be done in transactional class due to LAZY collections in
   * Survey result).
   *
   * @param surveyCampaignSlug - reference to campaign
   * @param accountId - identifier of the user
   */
  public PersistentEntityResource findBySurveyCampaignSlugAndCreatedBy(
      final String surveyCampaignSlug, final String accountId,
      final PersistentEntityResourceAssembler assembler) {
    return assembler
        .toResource(findBySurveyCampaignSlugAndCreatedBy(surveyCampaignSlug, accountId));
  }

  /**
   * Find last survey result created by this user.
   */
  public PersistentEntityResource findBySurveySlugAndCreatedByLast(
      final Long surveySlug, final String accountId,
      final PersistentEntityResourceAssembler assembler) {

    final Survey survey = surveyDao.findById(surveySlug);
    final List<SurveyResult> surveyResults = surveyResultDao
        .findBySurveyAndCreatedByAndCompletedTrueOrderByCreatedDesc(survey, accountId);

    if (surveyResults == null || surveyResults.isEmpty()) {
      throw new ResourceNotFoundException(SurveyErrorCodes.SURVEY_RESULT_NOT_FOUND);
    }

    return assembler
        .toResource(surveyResults.get(0));
  }

  private SurveyCampaign getActiveSurveyCampaign(final String surveyCampaignSlug) {
    final SurveyCampaign surveyCampaign =
        surveyCampaignDao.findByUuid(UUID.fromString(surveyCampaignSlug));
    // check if campaign active and not expired
    if (surveyCampaign != null) {
      if (!surveyCampaign.isActive() || surveyCampaign.isDeleted()) {
        throw new InvalidResourceException(SurveyErrorCodes.SURVEY_CAMPAIGN_NOT_ACTIVE);
      } else {
        final ZonedDateTime currentDate = ZonedDateTime.now();
        if (surveyCampaign.getStartDate() != null
            && currentDate.isBefore(surveyCampaign.getStartDate())) {
          throw new InvalidResourceException(SurveyErrorCodes.SURVEY_CAMPAIGN_NOT_STARTED);
        } else if (surveyCampaign.getExpireDate() != null
            && currentDate.isAfter(surveyCampaign.getExpireDate())) {
          throw new InvalidResourceException(SurveyErrorCodes.SURVEY_CAMPAIGN_EXPIRED);
        }
      }
    }
    return surveyCampaign;
  }

}
