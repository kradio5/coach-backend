package com.ydw.survey.services;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT) // 409
public class InvalidParametersException extends ResourceException {

  private static final long serialVersionUID = 256682528675378711L;

  public InvalidParametersException(final String code) {
    super(code);
  }

}
