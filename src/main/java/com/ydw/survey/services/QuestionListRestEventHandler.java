package com.ydw.survey.services;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.survey.dao.QuestionListDao;
import com.ydw.survey.model.QuestionList;
import com.ydw.survey.model.QuestionListValue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.data.rest.core.event.BeforeSaveEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Handler to automatically generate survey result entities.
 */
@Component
@RepositoryEventHandler(QuestionList.class)
@Transactional(readOnly = true)
public class QuestionListRestEventHandler {

  @Autowired
  private QuestionRestEventHandler questionRestEventHandler;

  @Autowired
  private QuestionListDao questionListDao;

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Copy question from source.
   */
  @HandleBeforeCreate
  public void beforeCreate(final QuestionList questionList) {
    if (questionList.getCopyFromSlug() != 0 && null == questionList.getSection()) {
      // check if general listener is not yet invoked (section is null while copying)
      questionRestEventHandler.beforeCreate(questionList);
    }
  }

  /**
   * Handles {@link BeforeSaveEvent}.
   */
  @HandleBeforeSave
  public void beforeSave(final QuestionList questionList) {
    entityManager.detach(questionList);
    // TODO: try usage of transient proxy listValues instead of detach
    final QuestionList oldQuestionList = questionListDao.findOne(questionList.getId());
    // match old list value to new list value by sort order
    if (!oldQuestionList.getListValues().isEmpty()) {
      final Map<String, QuestionListValue> oldListValuesMap = oldQuestionList.getListValues()
          .stream().collect(Collectors.toMap(QuestionListValue::getListValueId, item -> item));
      for (final QuestionListValue questionListValue : questionList.getListValues()) {
        if (!oldListValuesMap.containsKey(questionListValue.getListValueId())) {
          continue;
        }
        final LocalizedString oldTitle =
            oldListValuesMap.get(questionListValue.getListValueId()).getTitle();
        questionListValue.getTitle().merge(oldTitle);
      }
    }
  }

}
