package com.ydw.survey.content.services;

import com.ydw.survey.common.rest.RestEntityServiceSupport;
import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.content.model.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Service;

@Service
public class CategoryService extends RestEntityServiceSupport<Category, Long> {

  private final CategoryDao categoryDao;

  private final Repositories repositories;

  /**
   * Constructor with required fields.
   *
   * @param categoryDao Can not be {@code null}
   * @param eventPublisher Can not be {@code null}
   * @param repositories Can not be {@code null}
   */
  @Autowired
  public CategoryService(final CategoryDao categoryDao,
      final ApplicationEventPublisher eventPublisher,
      final Repositories repositories) {
    super(categoryDao, eventPublisher);
    this.categoryDao = categoryDao;
    this.repositories = repositories;
  }

  public long count() {
    return categoryDao.count();
  }

  public long count(final Class<? extends Category> categoryClass) {
    final Object dao = repositories.getRepositoryFor(categoryClass);
    return ((CrudRepository<?, ?>) dao).count();
  }

  // /**
  // * Finds categories by taxonomy and title.
  // */
  // public <T extends Category> Collection<T> findByTaxonomyAndTitle(final String taxonomy,
  // final String title) {
  // return categoryDao.<T>findByTaxonomy(taxonomy)
  // .stream()
  // .filter(item -> item.getTitle().getValue().equalsIgnoreCase(title))
  // .collect(toList());
  // }

  public Category load(final Long id) {
    return categoryDao.findOne(id);
  }

}
