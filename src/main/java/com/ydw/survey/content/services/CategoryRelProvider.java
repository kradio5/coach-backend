package com.ydw.survey.content.services;

import com.ydw.survey.content.model.Category;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CategoryRelProvider implements RelProvider {

  @Override
  public String getCollectionResourceRelFor(final Class<?> type) {
    return "categories";
  }

  @Override
  public String getItemResourceRelFor(final Class<?> type) {
    return "category";
  }

  @Override
  public boolean supports(final Class<?> delimiter) {
    return ClassUtils.isAssignable(delimiter, Category.class);
  }

}
