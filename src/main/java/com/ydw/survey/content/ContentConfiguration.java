package com.ydw.survey.content;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Content management features configuration.
 */
@EnableJpaRepositories(basePackageClasses = { com.ydw.survey.content.dao.PackageMarker.class })
@EnableElasticsearchRepositories(
    basePackageClasses = { com.ydw.survey.content.search.PackageMarker.class })
@EntityScan(basePackageClasses = { com.ydw.survey.content.model.PackageMarker.class })
@Configuration
public class ContentConfiguration {

}
