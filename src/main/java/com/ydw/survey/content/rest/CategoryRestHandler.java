package com.ydw.survey.content.rest;

import com.ydw.survey.content.model.Category;
import com.ydw.survey.content.search.CategorySearch;
import com.ydw.survey.content.search.CategorySearchDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.data.rest.core.event.AfterCreateEvent;
import org.springframework.data.rest.core.event.AfterDeleteEvent;
import org.springframework.data.rest.core.event.AfterSaveEvent;
import org.springframework.data.rest.core.event.RepositoryEvent;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

/**
 * Category base class REST event handler.
 * <p>
 * Performs storing of searchable category in the elasticsearch index.
 * </p>
 */
@Component
public class CategoryRestHandler implements ApplicationListener<RepositoryEvent> {

  @Autowired
  private CategorySearchDao searchDao;

  public void handleAfterDelete(final Category entity) {
    searchDao.delete(entity.getId());
  }

  public void handleAfterSave(final Category entity) {
    searchDao.save(new CategorySearch(entity));
  }

  @Override
  public void onApplicationEvent(final RepositoryEvent event) {
    if (!ClassUtils.isAssignable(Category.class, event.getSource().getClass())) {
      return;
    }
    if (event instanceof AfterSaveEvent || event instanceof AfterCreateEvent) {
      handleAfterSave((Category) event.getSource());
    } else if (event instanceof AfterDeleteEvent) {
      handleAfterDelete((Category) event.getSource());
    }
  }
}
