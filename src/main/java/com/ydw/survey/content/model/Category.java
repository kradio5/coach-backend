package com.ydw.survey.content.model;

import com.ydw.common.data.model.AbstractEntity;
import com.ydw.common.data.model.LocalizedString;
import com.ydw.common.data.slug.Slugified;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.rest.core.config.Projection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "category")
@DiscriminatorColumn(name = "dtype")
@JsonInclude(Include.NON_NULL)
public abstract class Category extends AbstractEntity<Long> implements Slugified {

  @Projection(name = "excerpt", types = Category.class)
  @JsonInclude(Include.NON_NULL)
  public interface CategoryExceptProjection extends Slugified {

    String getIcon();

    LocalizedString getTitle();
  }

  @Projection(name = "list", types = Category.class)
  @JsonInclude(Include.NON_NULL)
  public interface CategoryListProjection extends CategoryExceptProjection {

    int getChildrenCount();

    boolean isHasParent();
  }

  private static final long serialVersionUID = 9112740268188420408L;

  /**
   * Category children.
   */
  @Field(ignoreFields = "children")
  @JsonIgnore
  @OneToMany(mappedBy = "parent")
  private List<Category> children = new ArrayList<>();

  @Transient
  private String defaultParentSlug;

  @Column(name = "deleted")
  private Boolean deleted;

  @Column(name = "description", columnDefinition = "LONGTEXT")
  private LocalizedString description = new LocalizedString();

  @Size(max = 255)
  @Column(name = "icon")
  private String icon;

  @Field(ignoreFields = "parent")
  @JsonIgnore
  @ManyToOne
  @JoinColumn(name = "parent_id")
  private Category parent;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  @Column(name = "slug", nullable = false, unique = true)
  private String slug;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  @Column(name = "taxonomy", nullable = false)
  private String taxonomy;

  @NotNull
  @Column(name = "title", columnDefinition = "LONGTEXT")
  private LocalizedString title = new LocalizedString();

  public Category(final String taxonomy) {
    this.taxonomy = taxonomy;
  }

  /**
   * Constructor with required fields.
   */
  protected Category(final String taxonomy, final String title) {
    this(taxonomy);
    this.title = new LocalizedString(title);
  }

  /**
   * Convenience constructor for child categories.
   */
  protected Category(final String taxonomy, final String title, final Category parent) {
    this(taxonomy, title);
    this.parent = parent;
  }

  public List<Category> getChildren() {
    return children;
  }

  @Transient
  public int getChildrenCount() {
    return null != children ? children.size() : 0;
  }

  public String getDefaultParentSlug() {
    return defaultParentSlug;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public LocalizedString getDescription() {
    return description;
  }

  public String getIcon() {
    return icon;
  }

  public Category getParent() {
    return parent;
  }

  @Override
  public String getSlug() {
    return slug;
  }

  public String getTaxonomy() {
    return taxonomy;
  }

  public LocalizedString getTitle() {
    return title;
  }

  @Transient
  public boolean isHasParent() {
    return null != parent;
  }

  public void setChildren(final List<Category> children) {
    this.children = children;
  }

  public void setDefaultParentSlug(final String defaultParentSlug) {
    this.defaultParentSlug = defaultParentSlug;
  }

  public void setDeleted(final Boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setIcon(final String icon) {
    this.icon = icon;
  }

  public void setParent(final Category parent) {
    this.parent = parent;
  }

  @Override
  public void setSlug(final String slug) {
    this.slug = slug;
  }

  protected void setTaxonomy(final String taxonomy) {
    this.taxonomy = taxonomy;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return super.toString() + new ToStringBuilder(this, ToStringStyle.NO_CLASS_NAME_STYLE)
        .append("title", title)
        .append("parent", parent);
  }

}
