package com.ydw.survey.content.dao;

import com.ydw.survey.content.model.Category;

import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface CategoryDao
    extends CategoryDaoBase<Category>, QueryDslPredicateExecutor<Category> {

}
