package com.ydw.survey.content.dao;

import com.ydw.common.data.slug.SlugifiedRepository;
import com.ydw.survey.content.model.Category;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface CategoryDaoBase<T extends Category>
    extends PagingAndSortingRepository<T, Long>, SlugifiedRepository<T> {
}
