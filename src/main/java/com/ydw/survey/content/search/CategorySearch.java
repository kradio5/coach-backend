package com.ydw.survey.content.search;

import static com.ydw.survey.ElasticsearchConfiguration.DOMAIN_INDEX;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.common.spring.search.AbstractSearchable;
import com.ydw.survey.content.model.Category;

import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

/**
 * Searchable document for {@link Category}.
 */
@Document(type = "category", indexName = DOMAIN_INDEX)
public class CategorySearch extends AbstractSearchable<Long> {

  private static final long serialVersionUID = -967873992764784457L;

  private Boolean deleted;

  private LocalizedString description;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  private List<String> parentSlugs;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  private String slug;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  private String taxonomy;

  private LocalizedString title;

  public CategorySearch() {}

  /**
   * Constructs mapped search document from the entity.
   */
  public CategorySearch(final Category entity) {
    BeanUtils.copyProperties(entity, this);
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public LocalizedString getDescription() {
    return description;
  }

  public List<String> getParentSlugs() {
    return parentSlugs;
  }

  public String getSlug() {
    return slug;
  }

  public String getTaxonomy() {
    return taxonomy;
  }

  public LocalizedString getTitle() {
    return title;
  }

  public void setDeleted(final Boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setParentSlugs(final List<String> parentSlugs) {
    this.parentSlugs = parentSlugs;
  }

  public void setSlug(final String slug) {
    this.slug = slug;
  }

  public void setTaxonomy(final String taxonomy) {
    this.taxonomy = taxonomy;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }
}
