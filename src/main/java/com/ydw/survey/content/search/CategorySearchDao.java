package com.ydw.survey.content.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface CategorySearchDao extends ElasticsearchRepository<CategorySearch, Long> {

}
