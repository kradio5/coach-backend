package com.ydw.survey.content.search;

import static com.ydw.survey.common.utils.SearchUtils.iterableToList;
import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

import com.ydw.common.i18n.I18nConfiguration.I18nProperties;
import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.content.model.Category;
import com.ydw.survey.content.model.QCategory;

import com.querydsl.core.BooleanBuilder;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.annotation.PostConstruct;

@Repository
public class CategorySearchService {

  @Autowired
  private CategoryDao dao;

  @Autowired
  private I18nProperties i18n;

  @Autowired
  private CategorySearchIndexService indexService;

  @Autowired
  private CategorySearchDao searchDao;

  /**
   * Converts list of searchable documents to list of entities.
   */
  private List<Category> convert(final List<CategorySearch> content) {
    final List<Long> ids = content.stream().map(item -> item.getId()).collect(toList());
    final BooleanBuilder query = new BooleanBuilder(QCategory.category.id.in(ids));
    final Iterable<Category> iterable = dao.findAll(query.getValue());
    return iterableToList(iterable);
  }

  /**
   * Finds categories having title matching the specified text in the specified taxonomy.
   *
   * @param taxonomy The taxonomy. Required.
   * @param title The title.
   * @return The search results
   */
  public List<Category> findByTaxonomyAndTitle(final String taxonomy, final String title) {
    final BoolQueryBuilder query = QueryBuilders.boolQuery();

    // Title
    final BoolQueryBuilder titleQuery = QueryBuilders.boolQuery().minimumNumberShouldMatch(1);
    i18n.getAcceptableLocales().stream().forEach(lang -> {
      titleQuery.should(QueryBuilders.matchPhraseQuery("title." + lang, title));
    });
    query.must(titleQuery);
    // query.must(multiMatchQuery(title, titleFields).fuzziness(0).lenient(false));

    // Taxonomy
    query.must(termQuery("taxonomy", taxonomy));

    // Search
    final Iterable<CategorySearch> page = searchDao.search(query);
    return convert(iterableToList(page));
  }

  /**
   * Initializes the service.
   */
  @PostConstruct
  public void init() {
    indexService.maintainIndex();
  }

}
