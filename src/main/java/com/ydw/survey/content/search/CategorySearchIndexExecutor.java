package com.ydw.survey.content.search;

import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.content.model.Category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class CategorySearchIndexExecutor {

  @Autowired
  private CategorySearchDao searchDao;

  @Autowired
  private CategoryDao dao;

  /**
   * Re-indexes the specified entities.
   */
  @Transactional
  public Page<Category> reindexAll(final Pageable pageable) {
    final Page<Category> page = dao.findAll(pageable);
    if (page.hasContent()) {
      searchDao.save(page.map(entity -> new CategorySearch(entity)).getContent());
    }
    return page;
  }

}
