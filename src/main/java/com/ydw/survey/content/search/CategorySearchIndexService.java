package com.ydw.survey.content.search;

import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.content.model.Category;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * Category search index maintenance service.
 *
 * @author Andrew Usov
 */
@Service
public class CategorySearchIndexService {

  private static final Logger LOG = LoggerFactory.getLogger(CategorySearchIndexService.class);

  @Autowired
  private CategoryDao dao;

  @Autowired
  private CategorySearchIndexExecutor executor;

  @Autowired
  private CategorySearchDao searchDao;

  private final Object indexLock = new Object();

  /**
   * Initializes the service.
   */
  @PostConstruct
  public void init() {
    maintainIndex();
  }

  /**
   * Checks index health and recreates index, if necessary.
   */
  public void maintainIndex() {
    if (searchDao.count() != dao.count()) {
      LOG.warn("Category index is not healthy");
      reindexAll();
    }
  }

  /**
   * Recreates index.
   */
  public void reindexAll() {
    synchronized (indexLock) {
      LOG.info("Category reindex starting...");
      searchDao.deleteAll();
      Pageable pageable = new PageRequest(0, 250);
      while (pageable != null) {
        final Page<Category> page = executor.reindexAll(pageable);
        LOG.debug("Category reindex batch " + (page.getNumber() + 1) + " of "
            + page.getTotalPages() + " finished");
        pageable = page.nextPageable();
      }
    }
  }

}
