package com.ydw.survey.search.api;

import com.ydw.common.data.rest.EntityControllerSupport;
import com.ydw.common.security.utils.SecurityUtils;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.rest.SurveyLinks;
import com.ydw.survey.search.SurveySearchService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.support.DefaultedPageable;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Collections;

@RepositoryRestController
public class SurveySearchController extends EntityControllerSupport {

  private static final String BASE_MAPPING =
      "/" + SurveyLinks.SURVEYS + "/" + SurveyLinks.SEARCH;

  private final RepositoryEntityLinks entityLinks;

  @Autowired
  private SurveyLinks surveyLinks;

  @Autowired
  private SurveyDao surveyDao;

  private final PagedResourcesAssembler<Object> pagedResourcesAssembler;

  /**
   * Constructs controller with required dependencies.
   *
   * @param surveySearchService The search service.
   * @param pagedResourcesAssembler must not be {@literal null}.
   * @param entityLinks must not be {@literal null}.
   * @param mappings must not be {@literal null}.
   * @param config must not be {@literal null}.
   */
  @Autowired
  public SurveySearchController(final SurveySearchService surveySearchService,
      final PagedResourcesAssembler<Object> pagedResourcesAssembler,
      final RepositoryEntityLinks entityLinks, final ResourceMappings mappings,
      final RepositoryRestConfiguration config) {
    super(pagedResourcesAssembler, entityLinks, mappings, config);

    Assert.notNull(surveySearchService);

    this.pagedResourcesAssembler = pagedResourcesAssembler;
    this.entityLinks = entityLinks;
  }

  /**
   * <code>HEAD /{repository}/search</code> - Checks whether the search resource is present.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.HEAD)
  public HttpEntity<?> headForSearches() {
    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
  }

  /**
   * <code>GET /{repository}/search</code> - Exposes links to the individual search resources
   * exposed by the backing repository.
   */
  @ResponseBody
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.GET)
  public ResourceSupport listSearches() {
    final Links queryMethodLinks = entityLinks.linksToSearchResources(Survey.class);

    final ResourceSupport result = new ResourceSupport();

    // Find my surveys
    result.add(surveyLinks.linkToSearchMySurveys(null).withRel(SurveyLinks.FIND_MY));

    // Find all surveys
    result.add(surveyLinks.linkToSearchAll(null, null, null).withRel(SurveyLinks.FIND_ALL));

    if (!queryMethodLinks.isEmpty()) {
      result.add(queryMethodLinks);
    }
    result.add(getDefaultSelfLink());
    return result;
  }

  /**
   * <code>OPTIONS /{repository}/search</code>.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.OPTIONS)
  public HttpEntity<?> optionsForSearches() {
    final HttpHeaders headers = new HttpHeaders();
    headers.setAllow(Collections.singleton(HttpMethod.GET));
    return new ResponseEntity<Object>(headers, HttpStatus.OK);
  }

  @Override
  protected Link getDefaultSelfLink() {
    return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString());
  }

  @RequestMapping(value = BASE_MAPPING + "/" + SurveyLinks.FIND_MY,
      method = RequestMethod.GET)
  @ResponseBody
  @SuppressWarnings("unchecked")
  Resources<?> findMy(final DefaultedPageable defaultedPageable,
      final PersistentEntityResourceAssembler assembler) {

    final String accountId = SecurityUtils.getMyPrincipal();
    Resources<? extends Resource<Object>> resources = null;
    if (!StringUtils.isEmpty(accountId)) {
      final Pageable pageable = defaultedPageable.getPageable();
      final Page<?> surveysPage = surveyDao.findByCreatedByOrderByCreated(accountId, pageable);
      final Link selfLink = surveyLinks.linkToSearchMySurveys(pageable);
      resources =
          pagedResourcesAssembler.toResource((Page<Object>) surveysPage, assembler, selfLink);
    }
    return resources;
  }

}
