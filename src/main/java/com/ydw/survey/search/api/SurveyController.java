package com.ydw.survey.search.api;

import com.ydw.common.data.rest.EntityControllerSupport;
import com.ydw.survey.model.Survey;
import com.ydw.survey.rest.SurveyLinks;
import com.ydw.survey.search.SurveySearchService;
import com.ydw.survey.search.model.SurveySearchOptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.support.DefaultedPageable;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@RepositoryRestController
public class SurveyController extends EntityControllerSupport {

  private static final String BASE_MAPPING = "/" + SurveyLinks.SURVEYS;

  private final ResourceMetadata metadata;

  private final SurveySearchService surveySearchService;

  /**
   * Constructs controller with required dependencies.
   *
   * @param surveySearchService The search service.
   * @param pagedResourcesAssembler must not be {@literal null}.
   * @param entityLinks must not be {@literal null}.
   * @param mappings must not be {@literal null}.
   * @param config must not be {@literal null}.
   */
  @Autowired
  public SurveyController(final SurveySearchService surveySearchService,
      final PagedResourcesAssembler<Object> pagedResourcesAssembler,
      final RepositoryEntityLinks entityLinks, final ResourceMappings mappings,
      final RepositoryRestConfiguration config) {
    super(pagedResourcesAssembler, entityLinks, mappings, config);

    Assert.notNull(surveySearchService);

    this.surveySearchService = surveySearchService;
    this.metadata = mappings.getMetadataFor(Survey.class);
  }

  /**
   * Performs search using specified criteria.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.GET)
  @ResponseBody
  public Resources<?> getSurveys(final SurveySearchOptions criteria,
      final DefaultedPageable defaultedPageable,
      final PersistentEntityResourceAssembler assembler) {

    final Pageable pageable = defaultedPageable.getPageable();
    final Page<?> page = surveySearchService.findAll(criteria, pageable);
    final Link linkSelf = getDefaultSelfLink();
    @SuppressWarnings("unchecked")
    final Resources<? extends Resource<Object>> resources =
        entitiesToResources((Page<Object>) page, assembler, linkSelf);
    resources.add(getProfileLink(metadata));
    resources.add(getSearchLink(metadata));

    return resources;
  }

}
