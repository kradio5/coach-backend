package com.ydw.survey.search.model;

import static com.ydw.survey.ElasticsearchConfiguration.DOMAIN_INDEX;

import com.ydw.common.data.model.LocalizedString;
// import com.ydw.common.security.model.EntityGroupAccess;
import com.ydw.common.spring.search.AbstractSearchableAuditable;
import com.ydw.survey.content.model.Category;
import com.ydw.survey.model.Survey;

import org.springframework.beans.BeanUtils;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Searchable wrapper for {@link Survey}.
 */
@Document(type = "survey", indexName = DOMAIN_INDEX)
public class SurveySearch extends AbstractSearchableAuditable<Long> {

  private static final long serialVersionUID = 2238534681038867495L;

  /**
   * Collects all slugs from all parents for search indexing.
   */
  static List<String> collectCategoryPath(final Category category) {
    final List<String> path = new ArrayList<>();
    Category parent = category;
    while (null != parent) {
      path.add(parent.getSlug());
      parent = parent.getParent();
    }
    return path;
  }

  /**
   * Category slugs including all parents.
   */
  @Field(type = FieldType.String, index = FieldIndex.not_analyzed, store = false)
  private final Set<String> categoryPathSlugs = new HashSet<>();

  /**
   * Category slugs.
   */
  @Field(type = FieldType.String, index = FieldIndex.not_analyzed, store = false)
  private Set<String> categorySlugs = new HashSet<>();

  private Boolean deleted;

  @Field(type = FieldType.Object)
  private LocalizedString description;

  @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
  private String slug;

  @Field(type = FieldType.Object)
  private LocalizedString title;

  SurveySearch() {}

  /**
   * Constructs searchable document for the specified delegate.
   */
  public SurveySearch(final Survey survey) {
    Assert.notNull(survey, "Source entity required");
    Assert.notNull(survey.getId(), "Source entity must be persistent");

    BeanUtils.copyProperties(survey, this, "categories", "slug");

    this.slug = survey.getSlug().toString();

    survey.getCategories().forEach(item -> {
      categoryPathSlugs.addAll(collectCategoryPath(item));
      categorySlugs.add(item.getSlug());
    });
  }

  public Set<String> getCategoryPathSlugs() {
    return categoryPathSlugs;
  }

  public Set<String> getCategorySlugs() {
    return categorySlugs;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public LocalizedString getDescription() {
    return description;
  }

  public String getSlug() {
    return slug;
  }

  public LocalizedString getTitle() {
    return title;
  }

  public void setCategorySlugs(final Set<String> categorySlugs) {
    this.categorySlugs = categorySlugs;
  }

  public void setDeleted(final Boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setSlug(final String slug) {
    this.slug = slug;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

}
