package com.ydw.survey.search.model;

import java.util.HashSet;
import java.util.Set;

public class SurveySearchOptions {

  private String author;

  private Set<String> categories = new HashSet<>();

  private Boolean showDeleted = false;

  private boolean showPublishedOnly = false;

  private String text;

  /**
   * Adds a category.
   */
  public void addCategory(final String category) {
    categories.add(category);
  }

  public String getAuthor() {
    return author;
  }

  public Set<String> getCategories() {
    return categories;
  }

  public Boolean getShowDeleted() {
    return showDeleted;
  }

  public boolean getShowPublishedOnly() {
    return showPublishedOnly;
  }

  public String getText() {
    return text;
  }

  public void setAuthor(final String author) {
    this.author = author;
  }

  public void setCategories(final Set<String> categories) {
    this.categories = categories;
  }

  public void setShowDeleted(final Boolean showDeleted) {
    this.showDeleted = showDeleted;
  }

  public void setShowPublishedOnly(final boolean showPublishedOnly) {
    this.showPublishedOnly = showPublishedOnly;
  }

  public void setText(final String text) {
    this.text = text;
  }

}
