package com.ydw.survey.search;

import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.search.dao.SurveySearchDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Survey search index maintenance service.
 */
@Service
public class SurveySearchIndexService {

  private static final Logger LOG = LoggerFactory.getLogger(SurveySearchIndexService.class);

  @Autowired
  private SurveyDao dao;

  @Autowired
  private SurveySearchIndexExecutor executor;

  @Autowired
  private SurveySearchDao searchDao;

  private final Object indexLock = new Object();

  /**
   * Checks index health and recreates index, if necessary.
   */
  public void maintainIndex() {
    if (searchDao.count() != dao.count()) {
      LOG.warn("Survey index is not healthy");
      reindexAll();
    }
  }

  /**
   * Recreates index.
   */
  public void reindexAll() {
    synchronized (indexLock) {
      LOG.info("Survey reindex starting...");
      searchDao.deleteAll();
      Pageable pageable = new PageRequest(0, 250);
      while (pageable != null) {
        final Page<Survey> page = executor.reindexAll(pageable);
        LOG.debug("Survey reindex batch " + (page.getNumber() + 1) + " of "
            + page.getTotalPages() + " finished");
        pageable = page.nextPageable();
      }
    }
  }

}
