package com.ydw.survey.search;

import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.search.dao.SurveySearchDao;
import com.ydw.survey.search.model.SurveySearch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
class SurveySearchIndexExecutor {

  @Autowired
  private SurveySearchDao searchDao;

  @Autowired
  private SurveyDao dao;

  /**
   * Re-indexes the specified entities.
   */
  @Transactional
  public Page<Survey> reindexAll(final Pageable pageable) {
    final Page<Survey> page = dao.findAll(pageable);
    if (page.hasContent()) {
      searchDao.save(page.map(entity -> new SurveySearch(entity)).getContent());
    }
    return page;
  }

}
