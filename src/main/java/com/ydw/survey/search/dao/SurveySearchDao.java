package com.ydw.survey.search.dao;

import com.ydw.survey.search.model.SurveySearch;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface SurveySearchDao extends ElasticsearchRepository<SurveySearch, Long> {

}
