package com.ydw.survey.search;

import static com.ydw.survey.search.CommonSearchFilters.localizedStringQuery;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;

import com.ydw.common.i18n.I18nConfiguration.I18nProperties;
import com.ydw.survey.common.utils.SearchUtils;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.QSurvey;
import com.ydw.survey.model.Survey;
import com.ydw.survey.search.dao.SurveySearchDao;
import com.ydw.survey.search.model.SurveySearch;
import com.ydw.survey.search.model.SurveySearchOptions;

import com.querydsl.core.BooleanBuilder;

import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

@Service
@Transactional(readOnly = true)
public class SurveySearchService {

  @Autowired
  private SurveyDao dao;

  @Autowired
  private I18nProperties i18n;

  @Autowired
  private SurveySearchIndexService indexService;

  @Autowired
  private SurveySearchDao searchDao;

  /**
   * Converts list of searchable documents to list of entities.
   */
  private List<Survey> convert(final List<SurveySearch> content) {
    if (content.isEmpty()) {
      return emptyList();
    }
    final List<Long> ids = content.stream().map(item -> item.getId()).collect(toList());
    final QSurvey survey = QSurvey.survey;
    final BooleanBuilder query = new BooleanBuilder(survey.id.in(ids));
    final Iterable<Survey> iterable = dao.findAll(query.getValue());
    return SearchUtils.iterableToList(iterable);
  }

  /**
   * Performs search using search options.
   */
  public Page<Survey> findAll(final SurveySearchOptions options, final Pageable pageable) {
    final BoolQueryBuilder query = boolQuery();

    // Categories
    if (!CollectionUtils.isEmpty(options.getCategories())) {
      query.filter(termsQuery("categorySlugs", options.getCategories()));
    }

    // Deleted
    query.filter(termQuery("deleted", options.getShowDeleted()));

    // Text in all languages
    if (StringUtils.hasText(options.getText())) {
      query.must(localizedStringQuery("title", options.getText(), i18n.getAcceptableLocales())
          .fuzziness(Fuzziness.AUTO));
    }

    // By default, return none
    if (!query.hasClauses()) {
      return new PageImpl<>(Collections.emptyList());
    }

    // Search
    final Page<SurveySearch> results = searchDao.search(query, pageable);
    return new PageImpl<>(convert(results.getContent()), pageable, results.getTotalElements());
  }

  /**
   * Initializes the service.
   */
  @PostConstruct
  public void init() {
    indexService.maintainIndex();
  }

}
