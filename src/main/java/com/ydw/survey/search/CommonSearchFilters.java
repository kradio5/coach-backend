package com.ydw.survey.search;

import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;

import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Locale;

/**
 * Common search filter builders.
 */
public class CommonSearchFilters {

  /**
   * Builds filter to search entities having one or more specified category slugs including .
   *
   * @param field The categories field in the target entity.
   * @param slug The collection of category slugs to search against.
   * @return The filter.
   */
  public static QueryBuilder categoryHierarchyQuery(final String field,
      final Iterable<String> slug) {
    return nestedQuery(field, termsQuery(field + ".parentSlugs", slug));
  }

  /**
   * Builds filter to search entities having assigned to any of the specified categories matched by
   * slugs.
   *
   * @param field The categories field in the target entity.
   * @param slug The collection of category slugs to search against.
   * @return The filter.
   */
  public static QueryBuilder categoryQuery(final String field, final Iterable<String> slug) {
    return nestedQuery(field, termsQuery(field + ".slug", slug));
  }

  /**
   * Build fuzzy multi-match query on the specified localized string field.
   *
   * @param field The field of type {@link com.ydw.common.data.model.LocalizedString}
   * @param text The search text
   * @param locales The collection of locales
   */
  public static MultiMatchQueryBuilder localizedStringQuery(final String field,
      final String text,
      final Collection<Locale> locales) {
    Assert.notNull(field, "Field name must not be null");
    Assert.notNull(text, "Text must not be null");
    Assert.notNull(locales, "Locales must not be null");

    final String[] localizedFields = locales.stream()
        .map(locale -> field + "." + locale.toLanguageTag())
        .toArray(String[]::new);

    return multiMatchQuery(text, localizedFields);
  }
}
