package com.ydw.survey;

import io.searchbox.client.JestClient;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.github.vanroy.springdata.jest.JestElasticsearchTemplate;
import com.github.vanroy.springdata.jest.mapper.DefaultJestResultsMapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.data.mapping.context.MappingContext;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.io.IOException;

@Configuration
public class ElasticsearchConfiguration {

  /**
   * Custom DocumentMapper using jackson.
   */
  public static class ElasticsearchEntityMapper implements EntityMapper {

    private final ObjectMapper elasticsearchObjectMapper;

    /**
     * Constructs elasticsearch entity mapper with the specified Jackson2 {@link ObjectMapper}.
     */
    public ElasticsearchEntityMapper(final ObjectMapper elasticsearchObjectMapper) {
      this.elasticsearchObjectMapper = elasticsearchObjectMapper;
    }

    @Override
    public <T> T mapToObject(final String source, final Class<T> clazz) throws IOException {
      return elasticsearchObjectMapper.readValue(source, clazz);
    }

    @Override
    public String mapToString(final Object object) throws IOException {
      return elasticsearchObjectMapper.writeValueAsString(object);
    }
  }

  /**
   * The primary application domain index.
   *
   * @author Andrew Usov
   */
  public static final String DOMAIN_INDEX = "ydw-survey";

  /**
   * Exposes elasticsearch mapping context for managed persistent entities.
   */
  @SuppressWarnings("rawtypes")
  @Bean
  public MappingContext elasticsearchMappingContext(
      final ElasticsearchOperations elasticsearchTemplate) {
    return elasticsearchTemplate.getElasticsearchConverter().getMappingContext();
  }

  /**
   * Creates specific Jackson {@link ObjectMapper} configured for use in Elasticsearch.
   */
  protected ObjectMapper elasticsearchObjectMapper(final Jackson2ObjectMapperBuilder builder) {
    final ObjectMapper objectMapper = builder.build();
    // Default settings
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    // Disable indented output to prevent errors in Bulk index requests
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
    return objectMapper;
  }

  /**
   * Creates custom {@link ElasticsearchOperations} to customize JSON object mapper properties.
   *
   * @param client The {@link JestClient}.
   * @param builder The default Jackson2 ObjectMapper builder.
   */
  @Bean
  public ElasticsearchOperations elasticsearchTemplate(final JestClient client,
      final Jackson2ObjectMapperBuilder builder) {
    final ObjectMapper objectMapper = elasticsearchObjectMapper(builder);
    final EntityMapper entityMapper = new ElasticsearchEntityMapper(objectMapper);
    return new JestElasticsearchTemplate(client, new DefaultJestResultsMapper(entityMapper));
  }

}
