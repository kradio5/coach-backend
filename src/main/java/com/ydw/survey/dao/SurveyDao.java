package com.ydw.survey.dao;

import com.ydw.survey.model.Survey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

public interface SurveyDao
    extends PagingAndSortingRepository<Survey, Long>, QueryDslPredicateExecutor<Survey> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends Survey> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Survey entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Description("Lookups a Surveys by creating user")
  Page<Survey> findByCreatedByOrderByCreated(@Param("createdBy") String createdBy,
      Pageable pageable);

  @RestResource(rel = "bySlug", path = "by-slug")
  // @Query("from Survey o where o.id = :slug")
  Survey findById(@Param("slug") Long slug);

  @RestResource(rel = "completedByUser", path = "completedByUser")
  @PreAuthorize("hasAuthority('EXPERT') || #accountId."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  @Query("from Survey s where exists "
      + "(select sr.id from SurveyResult sr where sr.survey = s and "
      + "sr.createdBy = :accountId and sr.completed = true) order by s.created")
  List<Survey> findCompletedByCreatedBy(@Param("accountId") String accountId);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends Survey> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends Survey> S save(S entity);

}
