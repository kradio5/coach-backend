package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.SurveyResult;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswer.Excerpt.class)
public interface QuestionAnswerDao extends CrudRepository<QuestionAnswer, Long> {

  @Override
  default List<QuestionAnswer> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  @PostAuthorize("hasAuthority('EXPERT') || returnObject.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  QuestionAnswer findById(@Param("slug") Long slug);

  @Description("Lookups questions answers by survey result")
  @RestResource(rel = "bySurveyResult", path = "by-survey-result")
  @PreAuthorize("hasAuthority('EXPERT') || #surveyResult.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<QuestionAnswer> findBySectionSurveyResultOrderBySortOrder(
      @Param("surveyResult") SurveyResult surveyResult);
}
