package com.ydw.survey.dao;

import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = SurveyResultCalculation.Excerpt.class)
public interface SurveyResultCalculationDao
    extends CrudRepository<SurveyResultCalculation, Long> {

  @Override
  default List<SurveyResultCalculation> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  @PostAuthorize("hasAuthority('EXPERT') || returnObject.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  SurveyResultCalculation findById(@Param("slug") Long slug);

  @Description("Lookups visible SurveyResultCalculations by SurveyResult")
  // provide back compatibility
  @RestResource(rel = "findBySurveyResultVisible", path = "findBySurveyResultVisible")

  @PreAuthorize("hasAuthority('EXPERT') || #surveyResult.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResultCalculation> findBySurveyResultAndSurveyCalculationVisibleTrueOrderBySortOrder(
      @Param("surveyResult") SurveyResult surveyResult);

  @Description("Lookups a SurveyResultCalculations by SurveyResult")
  // provide back compatibility
  @RestResource(rel = "findBySurveyResult", path = "findBySurveyResult")
  @PreAuthorize("hasAuthority('EXPERT') || #surveyResult.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResultCalculation> findBySurveyResultOrderBySortOrder(
      @Param("surveyResult") SurveyResult surveyResult);

  /**
   * Do not allow saving via REST since objects are automatically created in survey calculation.
   */
  @Override
  @RestResource(exported = false)
  <S extends SurveyResultCalculation> Iterable<S> save(Iterable<S> entities);

  /**
   * Do not allow saving via REST since objects are automatically created in survey calculation.
   */
  @Override
  @RestResource(exported = false)
  <S extends SurveyResultCalculation> S save(S entity);
}
