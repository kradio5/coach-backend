package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerBoolean;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerBoolean.Excerpt.class)
public interface QuestionAnswerBooleanDao extends CrudRepository<QuestionAnswerBoolean, Long> {

  @Override
  default List<QuestionAnswerBoolean> findAll() {
    return Collections.emptyList();
  }

}
