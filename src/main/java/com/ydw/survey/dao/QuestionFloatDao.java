package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionFloat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionFloat.Excerpt.class)
public interface QuestionFloatDao extends CrudRepository<QuestionFloat, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends QuestionFloat> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(QuestionFloat entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<QuestionFloat> findAll() {
    return Collections.emptyList();
  }

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionFloat> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionFloat> S save(S entity);

}
