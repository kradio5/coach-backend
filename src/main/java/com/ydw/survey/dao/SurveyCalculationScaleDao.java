package com.ydw.survey.dao;

import com.ydw.survey.model.SurveyCalculationScale;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

public interface SurveyCalculationScaleDao
    extends CrudRepository<SurveyCalculationScale, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends SurveyCalculationScale> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(SurveyCalculationScale entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<SurveyCalculationScale> findAll() {
    return Collections.emptyList();
  }

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCalculationScale> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCalculationScale> S save(S entity);

}
