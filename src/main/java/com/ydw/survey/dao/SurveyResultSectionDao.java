package com.ydw.survey.dao;

import com.ydw.survey.model.SurveyResultSection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collections;
import java.util.List;

public interface SurveyResultSectionDao extends CrudRepository<SurveyResultSection, Long> {

  @Override
  default List<SurveyResultSection> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  // @Query("from SurveyResultSection o where o.id = :slug")
  SurveyResultSection findById(@Param("slug") Long slug);
}
