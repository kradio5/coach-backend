package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerInteger;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerInteger.Excerpt.class)
public interface QuestionAnswerIntegerDao extends CrudRepository<QuestionAnswerInteger, Long> {

  @Override
  default List<QuestionAnswerInteger> findAll() {
    return Collections.emptyList();
  }

}
