package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionBoolean;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionBoolean.Excerpt.class)
public interface QuestionBooleanDao extends CrudRepository<QuestionBoolean, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends QuestionBoolean> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(QuestionBoolean entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<QuestionBoolean> findAll() {
    return Collections.emptyList();
  }

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionBoolean> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionBoolean> S save(S entity);

}
