package com.ydw.survey.dao;

import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculation;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource
public interface SurveyCalculationDao extends CrudRepository<SurveyCalculation, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends SurveyCalculation> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(SurveyCalculation entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<SurveyCalculation> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  SurveyCalculation findById(@Param("slug") Long slug);

  @Description("Lookups a SurveyCalculation by Survey")
  @RestResource(rel = "findBySurvey", path = "findBySurvey") // provide back compatibility
  List<SurveyCalculation> findBySurveyOrderBySortOrder(@Param("survey") Survey survey);

  @Description("Lookups the latest sortOrder value")
  @Query(value = "select max(o.sortOrder) from SurveyCalculation o where o.survey = :survey")
  Object findMaxSortOrderBySurvey(@Param("survey") Survey survey);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCalculation> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCalculation> S save(S entity);

}
