package com.ydw.survey.dao;

import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveySection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

public interface SurveySectionDao extends CrudRepository<SurveySection, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  void delete(Iterable<? extends SurveySection> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  void delete(SurveySection entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  void deleteAll();

  @Override
  default List<SurveySection> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  // @Query("from SurveySection o where o.id = :slug")
  SurveySection findById(@Param("slug") Long slug);

  @Description("Lookups the latest sortOrder value")
  @Query(value = "select max(o.sortOrder) from SurveySection o where o.survey = :survey")
  Object findMaxSortOrderBySurvey(@Param("survey") Survey survey);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveySection> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveySection> S save(S entity);

}
