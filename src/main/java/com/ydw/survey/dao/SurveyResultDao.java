package com.ydw.survey.dao;

import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public interface SurveyResultDao extends CrudRepository<SurveyResult, Long> {

  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends SurveyResult> entities);

  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @Override
  @RestResource(exported = false)
  void delete(SurveyResult entity);

  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<SurveyResult> findAll() {
    return Collections.emptyList();
  }

  @RestResource(rel = "completedByUser", path = "completedByUser")
  @PreAuthorize("hasAuthority('EXPERT') || #accountId."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResult> findByCreatedByAndCompletedTrueOrderByCreated(
      @Param("accountId") String accountId);

  @Description("Lookups a SurveyResult by created by")
  @PreAuthorize("hasAuthority('EXPERT') || #createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  @RestResource(exported = false)
  Page<SurveyResult> findByCreatedByOrderByCreated(@Param("createdBy") String createdBy,
      Pageable pageable);

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  @PostAuthorize("hasAuthority('EXPERT') || returnObject.createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  SurveyResult findById(@Param("slug") Long slug);

  @Description("Lookups a SurveyResult by Survey")
  @PreAuthorize("hasAuthority('EXPERT')")
  List<SurveyResult> findBySurveyAndCompletedTrueOrderByCreatedDesc(
      @Param("survey") Survey survey);

  @Description("Lookups a SurveyResult by Survey and created by")
  @RestResource(exported = false)
  List<SurveyResult> findBySurveyAndCreatedByAndCompletedTrueOrderByCreatedDesc(
      @Param("survey") Survey survey, @Param("createdBy") String createdBy);

  @Description("Lookups a SurveyResult by Survey and created by")
  @PreAuthorize("hasAuthority('EXPERT') || #createdBy."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResult> findBySurveyAndCreatedByOrderByCreated(@Param("survey") Survey survey,
      @Param("createdBy") String createdBy);

  @Description("Lookups SurveyResult objects by SurveyCampaign")
  @RestResource(exported = false)
  List<SurveyResult> findBySurveyCampaignAndCompletedTrueOrderByCreated(
      @Param("surveyCampaign") final SurveyCampaign surveyCampaign);

  @Description("Lookups a SurveyResult by SurveyCampaign and created by")
  @RestResource(exported = false)
  List<SurveyResult> findBySurveyCampaignAndCreatedByAndCompletedTrueOrderByCreated(
      @Param("surveyCampaign") SurveyCampaign surveyCampaign,
      @Param("createdBy") String createdBy);

  @Description("Lookups a SurveyResult by SurveyCampaign and created by")
  @RestResource(exported = false)
  List<SurveyResult> findBySurveyCampaignAndCreatedByOrderByCreated(
      @Param("surveyCampaign") SurveyCampaign surveyCampaign,
      @Param("createdBy") String createdBy);

  @RestResource(rel = "completedBySurveyCampaignSlugAndUser",
      path = "completedBySurveyCampaignSlugAndUser")
  @PreAuthorize("hasAuthority('EXPERT') || #accountId."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResult> findBySurveyCampaignUuidAndCreatedByAndCompletedTrueOrderByCreated(
      @Param("surveyCampaignSlug") UUID surveyCampaignSlug,
      @Param("accountId") String accountId);

  @RestResource(rel = "completedBySurveySlugAndUser", path = "completedBySurveySlugAndUser")
  @PreAuthorize("hasAuthority('EXPERT') || #accountId."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  List<SurveyResult> findBySurveyIdAndCreatedByAndCompletedTrueOrderByCreated(
      @Param("surveySlug") Long surveySlug, @Param("accountId") String accountId);

  @Description("Lookups a SurveyResult by Survey")
  @PreAuthorize("hasAuthority('EXPERT')")
  List<SurveyResult> findBySurveyOrderByCreated(@Param("survey") Survey survey);
}
