package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionScale;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionScale.Excerpt.class)
public interface QuestionScaleDao extends CrudRepository<QuestionScale, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends QuestionScale> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(QuestionScale entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<QuestionScale> findAll() {
    return Collections.emptyList();
  }

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionScale> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends QuestionScale> S save(S entity);

}
