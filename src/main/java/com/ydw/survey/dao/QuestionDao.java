package com.ydw.survey.dao;

import com.ydw.survey.model.Question;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveySection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = Question.Excerpt.class)
public interface QuestionDao extends CrudRepository<Question, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends Question> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Question entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Override
  default List<Question> findAll() {
    return Collections.emptyList();
  }

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  Question findById(@Param("slug") Long slug);

  @Description("Lookups questions by survey")
  @RestResource(rel = "bySurvey", path = "by-survey")
  List<Question> findBySectionSurveyOrderBySortOrder(@Param("survey") Survey survey);

  @Description("Lookups the latest sortOrder value")
  @Query(value = "select max(o.sortOrder) from Question o where o.section = :section")
  Object findMaxSortOrderBySection(@Param("section") SurveySection section);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends Question> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends Question> S save(S entity);

}
