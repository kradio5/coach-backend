package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerScale;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerScale.Excerpt.class)
public interface QuestionAnswerScaleDao extends CrudRepository<QuestionAnswerScale, Long> {

  @Override
  default List<QuestionAnswerScale> findAll() {
    return Collections.emptyList();
  }

}
