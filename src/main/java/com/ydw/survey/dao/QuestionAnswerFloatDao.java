package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerFloat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerFloat.Excerpt.class)
public interface QuestionAnswerFloatDao extends CrudRepository<QuestionAnswerFloat, Long> {

  @Override
  default List<QuestionAnswerFloat> findAll() {
    return Collections.emptyList();
  }

}
