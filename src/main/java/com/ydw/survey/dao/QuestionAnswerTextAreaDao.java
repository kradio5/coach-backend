package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerTextArea;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerTextArea.Excerpt.class)
public interface QuestionAnswerTextAreaDao
    extends CrudRepository<QuestionAnswerTextArea, Long> {

  @Override
  default List<QuestionAnswerTextArea> findAll() {
    return Collections.emptyList();
  }

}
