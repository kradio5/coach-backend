package com.ydw.survey.dao;

import com.ydw.survey.model.SurveyCampaign;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.Description;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;
import java.util.UUID;

public interface SurveyCampaignDao extends PagingAndSortingRepository<SurveyCampaign, Long> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends SurveyCampaign> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(SurveyCampaign entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @Description("Lookups a resource by its unique slug")
  @RestResource(rel = "bySlug", path = "by-slug")
  SurveyCampaign findByUuid(@Param("slug") UUID slug);

  @RestResource(rel = "completedByUser", path = "completedByUser")
  @PreAuthorize("hasAuthority('EXPERT') || #accountId."
      + "equals(T(com.ydw.common.security.utils.SecurityUtils).getMyPrincipal())")
  @Query("from SurveyCampaign sc where exists "
      + "(select sr.id from SurveyResult sr where sr.surveyCampaign = sc and "
      + "sr.createdBy = :accountId and sr.completed = true) order by sc.created")
  List<SurveyCampaign> findCompletedByCreatedBy(@Param("accountId") String accountId);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCampaign> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCampaign> S save(S entity);

}
