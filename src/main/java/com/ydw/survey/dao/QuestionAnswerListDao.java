package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerList.Excerpt.class)
public interface QuestionAnswerListDao extends CrudRepository<QuestionAnswerList, Long> {

  @Override
  default List<QuestionAnswerList> findAll() {
    return Collections.emptyList();
  }

}
