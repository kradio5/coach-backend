package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerTextFloat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerTextFloat.Excerpt.class)
public interface QuestionAnswerTextFloatDao
    extends CrudRepository<QuestionAnswerTextFloat, Long> {

  @Override
  default List<QuestionAnswerTextFloat> findAll() {
    return Collections.emptyList();
  }

}
