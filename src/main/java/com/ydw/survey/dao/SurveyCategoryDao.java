package com.ydw.survey.dao;

import com.ydw.survey.content.dao.CategoryDaoBase;
import com.ydw.survey.model.SurveyCategory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.access.prepost.PreAuthorize;

public interface SurveyCategoryDao extends CategoryDaoBase<SurveyCategory> {

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Iterable<? extends SurveyCategory> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(Long id);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void delete(SurveyCategory entity);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  @RestResource(exported = false)
  void deleteAll();

  @RestResource(rel = "byParentSlug", path = "byParentSlug")
  Page<SurveyCategory> findByParentSlug(@Param("slug") String slug, Pageable pageable);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCategory> Iterable<S> save(Iterable<S> entities);

  @PreAuthorize("hasAuthority('EXPERT')")
  @Override
  <S extends SurveyCategory> S save(S entity);

}
