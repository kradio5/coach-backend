package com.ydw.survey.dao;

import com.ydw.survey.model.QuestionAnswerText;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Collections;
import java.util.List;

@RepositoryRestResource(excerptProjection = QuestionAnswerText.Excerpt.class)
public interface QuestionAnswerTextDao extends CrudRepository<QuestionAnswerText, Long> {

  @Override
  default List<QuestionAnswerText> findAll() {
    return Collections.emptyList();
  }

}
