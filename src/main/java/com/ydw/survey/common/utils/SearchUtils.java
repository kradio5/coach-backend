package com.ydw.survey.common.utils;

import java.util.ArrayList;
import java.util.List;

public abstract class SearchUtils {

  /**
   * Converts iterable to list.
   *
   * @param iterable The source iterable
   * @return The result list
   */
  public static <E> List<E> iterableToList(final Iterable<E> iterable) {
    final List<E> list = new ArrayList<E>();
    for (final E item : iterable) {
      list.add(item);
    }
    return list;
  }

}
