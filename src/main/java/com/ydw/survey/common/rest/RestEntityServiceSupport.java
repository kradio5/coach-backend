package com.ydw.survey.common.rest;

import com.ydw.common.data.model.AbstractPersistable;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.event.AfterCreateEvent;
import org.springframework.data.rest.core.event.AfterSaveEvent;
import org.springframework.data.rest.core.event.BeforeCreateEvent;
import org.springframework.data.rest.core.event.BeforeSaveEvent;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

public class RestEntityServiceSupport<T extends AbstractPersistable<K>, K extends Serializable> {

  private final ApplicationEventPublisher eventPublisher;

  private final CrudRepository<T, K> repository;

  /**
   * Constructs support class with the required dependencies.
   *
   * @param repository Cannot be {@code null}
   * @param eventPublisher Cannot be {@code null}
   */
  public RestEntityServiceSupport(final CrudRepository<T, K> repository,
      final ApplicationEventPublisher eventPublisher) {
    this.repository = repository;
    this.eventPublisher = eventPublisher;
  }

  /**
   * Saves entity to repository invoking REST handlers.
   */
  @Transactional
  public <S extends T> S save(final S entity) {
    final boolean create = entity.isNew();
    eventPublisher
        .publishEvent(create ? new BeforeCreateEvent(entity) : new BeforeSaveEvent(entity));
    final S saved = repository.save(entity);
    eventPublisher
        .publishEvent(create ? new AfterCreateEvent(entity) : new AfterSaveEvent(entity));
    return saved;
  }

}
