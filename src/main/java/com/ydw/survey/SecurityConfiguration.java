package com.ydw.survey;

import com.ydw.survey.SecurityConfiguration.SurveyServerSecurityProperties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Web security configuration.
 */
@Configuration
@Profile("server")
@EnableWebSecurity
@EnableConfigurationProperties(SurveyServerSecurityProperties.class)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @ConfigurationProperties(prefix = "survey.server.security")
  public static class SurveyServerSecurityProperties {

    /**
     * CORS allowed origin list.
     */
    private List<String> allowedOrigins = new ArrayList<>();

    public List<String> getAllowedOrigins() {
      return allowedOrigins;
    }

    public void setAllowedOrigins(final List<String> allowedOrigins) {
      this.allowedOrigins = allowedOrigins;
    }
  }

  @Autowired(required = true)
  SurveyServerSecurityProperties surveyServerSecurityProperties;

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    http.authorizeRequests() //
        .antMatchers("/").permitAll() //
        .antMatchers(HttpMethod.OPTIONS, "/api/**").permitAll() //
        .anyRequest().authenticated() //
        .and().logout().permitAll() //
        .and().csrf().disable() //
        .httpBasic();
  }

  /**
   * Configures {@link CorsFilter} that is also available for Spring DATA REST requests.
   *
   * @return The filter bean
   */
  @Bean
  @ConditionalOnProperty("survey.server.security.allowed-origins")
  public FilterRegistrationBean corsFilter() {
    final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

    final CorsConfiguration config = new CorsConfiguration();
    for (final String allowedOrigin : surveyServerSecurityProperties.getAllowedOrigins()) {
      config.addAllowedOrigin(allowedOrigin);
    }
    config.setAllowCredentials(true);
    config.addAllowedHeader("*");
    config.addAllowedMethod(HttpMethod.OPTIONS);
    config.addAllowedMethod(HttpMethod.HEAD);
    config.addAllowedMethod(HttpMethod.GET);
    config.addAllowedMethod(HttpMethod.PUT);
    config.addAllowedMethod(HttpMethod.POST);
    config.addAllowedMethod(HttpMethod.DELETE);
    config.addAllowedMethod(HttpMethod.PATCH);
    source.registerCorsConfiguration("/**", config);

    final FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
    bean.setOrder(0);
    return bean;
  }

}
