package com.ydw.survey;

import com.ydw.common.security.utils.SecurityUtils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@Configuration
public class AuditConfiguration {

  /**
   * Creates auditor user ID provider.
   */
  @Bean
  public AuditorAware<String> auditorAware() {
    return () -> SecurityUtils.getMyPrincipal();
  }
}
