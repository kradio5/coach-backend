package com.ydw.survey.data.initializer;

import static com.ydw.common.security.model.AssignedAuthorities.ADMIN;
import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

import com.ydw.common.i18n.I18nConfiguration.I18nProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

@Component
@ConditionalOnProperty(prefix = "application.data", value = "initializer",
    matchIfMissing = true)
public class ApplicationDataInitializer {

  private static final Logger LOG = LoggerFactory.getLogger(ApplicationDataInitializer.class);

  @Autowired(required = false)
  List<DataInitializer> dataInitializers;

  @Autowired
  private I18nProperties i18n;

  @Autowired
  private SecurityProperties security;

  /**
   * Initializes data.
   */
  @PostConstruct
  public void init() {
    // waitForAuthServerIsReady();

    // System locale
    final Locale defaultLocale = i18n.getAcceptableLocales().get(0);
    Locale.setDefault(defaultLocale);
    LocaleContextHolder.setLocale(defaultLocale);
    LOG.info("Set default locale: " + defaultLocale);

    // Initializers
    if (null == dataInitializers || dataInitializers.isEmpty()) {
      return;
    }

    final User user = new User(security.getUser().getName(), "N/A",
        commaSeparatedStringToAuthorityList(ADMIN));

    final PreAuthenticatedAuthenticationToken systemAuthentication =
        new PreAuthenticatedAuthenticationToken(user, user.getPassword(),
            user.getAuthorities());

    final SecurityContext origCtx = SecurityContextHolder.getContext();
    SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
    SecurityContextHolder.getContext().setAuthentication(systemAuthentication);

    for (final DataInitializer dataInitializer : dataInitializers) {
      dataInitializer.initializeData();
    }

    SecurityContextHolder.setContext(origCtx);
  }

}
