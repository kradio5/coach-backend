package com.ydw.survey.data.initializer;

public interface DataInitializer {

  void initializeData();

}
