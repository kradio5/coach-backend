package com.ydw.survey;

import com.github.vanroy.springboot.autoconfigure.data.jest.ElasticsearchJestAWSAutoConfiguration;

import org.springframework.cloud.aws.autoconfigure.cache.ElastiCacheAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.context.config.annotation.ContextResourceLoaderConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * Configuration for AWS cloud services auto-discovery.
 */
@Profile("aws")
@Configuration
@Import({
    ElastiCacheAutoConfiguration.class,
    ContextRegionProviderAutoConfiguration.class,
    ContextResourceLoaderAutoConfiguration.class,
    ContextResourceLoaderConfiguration.class,
    ContextInstanceDataAutoConfiguration.class,
    ContextCredentialsAutoConfiguration.class,
    ElasticsearchJestAWSAutoConfiguration.class })
public class AwsConfiguration {

}
