package com.ydw.survey;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan
@EntityScan(basePackageClasses = { com.ydw.survey.model.PackageMarker.class })
@EnableJpaRepositories(basePackageClasses = { com.ydw.survey.dao.PackageMarker.class })
@EnableElasticsearchRepositories(
    basePackageClasses = com.ydw.survey.search.dao.PackageMarker.class)
@Configuration
public class SurveyConfiguration {

}
