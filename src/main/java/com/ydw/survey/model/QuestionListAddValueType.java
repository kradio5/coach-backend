package com.ydw.survey.model;

/**
 * Question types.
 */
public enum QuestionListAddValueType {

  TEXT(Short.valueOf((short) 1)),

  INTEGER(Short.valueOf((short) 3)),

  FLOAT(Short.valueOf((short) 4));

  private Short questionListAddValueType;

  private QuestionListAddValueType(final Short questionListAddValueType) {
    this.questionListAddValueType = questionListAddValueType;
  }

  public Short getQuestionListAddValueType() {
    return questionListAddValueType;
  }

}
