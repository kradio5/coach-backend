package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;
import com.ydw.common.data.model.LocalizedString;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "survey_section")
public class SurveySection extends AbstractAuditableEntity<Long> {

  private static final long serialVersionUID = 8187631558301135163L;

  @Lob
  @Column(name = "title")
  private LocalizedString title;

  @Lob
  @Column(name = "description")
  private LocalizedString description;

  @Lob
  @Column(name = "note")
  private LocalizedString note;

  @Column(name = "random_questions_order")
  private boolean randomQuestionsOrder;

  @Column(name = "sort_order")
  private int sortOrder;

  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_id")
  private Survey survey;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "section", orphanRemoval = true)
  @OrderBy("sortOrder")
  private List<Question> questions = new ArrayList<Question>();

  /**
   * Add question link to survey section.
   */
  public void addQuestion(final Question question) {
    if (!questions.contains(question)) {
      question.setSortOrder(questions.size());
      questions.add(question);
    }
    question.setSection(this);
  }

  public LocalizedString getDescription() {
    return description;
  }

  public LocalizedString getNote() {
    return note;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public Long getSlug() {
    return super.getId();
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public Survey getSurvey() {
    return survey;
  }

  public LocalizedString getTitle() {
    return title;
  }

  public boolean isRandomQuestionsOrder() {
    return randomQuestionsOrder;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setNote(final LocalizedString note) {
    this.note = note;
  }

  /**
   * Update questions of section.
   */
  public void setQuestions(final List<Question> questions) {
    if (this.questions == null) {
      this.questions = questions;
    } else {
      this.questions.clear();
      int sortOrder = 0;
      for (final Question question : questions) {
        question.setSection(this);
        question.setSortOrder(sortOrder++);
      }
      this.questions.addAll(questions);
    }
  }

  public void setRandomQuestionsOrder(final boolean randomQuestionsOrder) {
    this.randomQuestionsOrder = randomQuestionsOrder;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

}
