package com.ydw.survey.model;

import com.ydw.cloud.auth.model.AuthUser;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "list", types = AuthUser.class)
public interface AuthUserListProjection {

  String getAccountId();

  String getDisplayName();

  String getImageUrl();

  String getUsername();

}
