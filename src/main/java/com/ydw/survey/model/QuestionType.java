package com.ydw.survey.model;

/**
 * Question types.
 */
public enum QuestionType {

  TEXT(Short.valueOf((short) 1)),

  TEXTAREA(Short.valueOf((short) 2)),

  INTEGER(Short.valueOf((short) 3)),

  FLOAT(Short.valueOf((short) 4)),

  LIST(Short.valueOf((short) 5)),

  BOOLEAN(Short.valueOf((short) 6)),

  SCALE(Short.valueOf((short) 7)),

  TEXT_FLOAT(Short.valueOf((short) 8));

  private Short questionType;

  private QuestionType(final Short questionType) {
    this.questionType = questionType;
  }

  public Short getQuestionType() {
    return questionType;
  }

}
