package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question_text")
@DiscriminatorValue("QuestionText")
public class QuestionText extends Question {

  @Projection(types = QuestionText.class)
  public interface Excerpt extends Question.Excerpt {

  }

  private static final long serialVersionUID = -1086049022416733411L;

  public QuestionText() {
    setQuestionType(QuestionType.TEXT);
  }

}
