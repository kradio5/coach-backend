package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question_text_area")
@DiscriminatorValue("QuestionTextArea")
public class QuestionTextArea extends Question {

  @Projection(types = QuestionTextArea.class)
  public interface Excerpt extends Question.Excerpt {

  }

  private static final long serialVersionUID = 4435398051108001084L;

  public QuestionTextArea() {
    setQuestionType(QuestionType.TEXTAREA);
  }

}
