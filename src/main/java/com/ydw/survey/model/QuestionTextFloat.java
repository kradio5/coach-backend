package com.ydw.survey.model;

import com.ydw.common.data.model.LocalizedString;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "question_text_float")
@DiscriminatorValue("QuestionTextFloat")
public class QuestionTextFloat extends Question {

  @Projection(types = QuestionTextFloat.class)
  public interface Excerpt extends Question.Excerpt {

    LocalizedString getFloatTitle();

    LocalizedString getTextTitle();

  }

  private static final long serialVersionUID = 4164120641855224080L;

  @Lob
  @Column(name = "text_title")
  private LocalizedString textTitle;

  @Lob
  @Column(name = "float_title")
  private LocalizedString floatTitle;

  public QuestionTextFloat() {
    setQuestionType(QuestionType.TEXT_FLOAT);
  }

  public LocalizedString getFloatTitle() {
    return floatTitle;
  }

  public LocalizedString getTextTitle() {
    return textTitle;
  }

  public void setFloatTitle(final LocalizedString floatTitle) {
    this.floatTitle = floatTitle;
  }

  public void setTextTitle(final LocalizedString textTitle) {
    this.textTitle = textTitle;
  }

}
