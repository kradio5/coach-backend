package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_text_area")
@DiscriminatorValue("QuestionAnswerTextArea")
public class QuestionAnswerTextArea extends QuestionAnswer {

  @Projection(types = QuestionAnswerTextArea.class)
  public interface Excerpt extends QuestionAnswerText.Excerpt {

    @Override
    String getAnswerValue();

  }

  private static final long serialVersionUID = 486391334737473008L;

  @Lob
  @Column(name = "answer_value")
  private String answerValue;

  public QuestionAnswerTextArea() {}

  public QuestionAnswerTextArea(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public String getAnswerValue() {
    return answerValue;
  }

  @Override
  @Transient
  public Object getValue() {
    return answerValue;
  }

  public void setAnswerValue(final String answerValue) {
    this.answerValue = answerValue;
  }

}
