package com.ydw.survey.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "survey_calculation_text")
@DiscriminatorValue("SurveyCalculationText")
public class SurveyCalculationText extends SurveyCalculation {

  private static final long serialVersionUID = 3673487086508572507L;

  public SurveyCalculationText() {
    setSurveyCalculationType(SurveyCalculationType.TEXT);
  }

}
