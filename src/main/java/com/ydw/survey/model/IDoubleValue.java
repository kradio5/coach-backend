package com.ydw.survey.model;

public interface IDoubleValue {

  double getValue();

}
