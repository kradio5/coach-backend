package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "survey_result_section")
public class SurveyResultSection extends AbstractAuditableEntity<Long> {

  private static final long serialVersionUID = 8187631558301135163L;

  @Column(name = "sort_order")
  private int sortOrder;

  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_result_id")
  private SurveyResult surveyResult;

  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_section_id")
  private SurveySection surveySection;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "section", orphanRemoval = true)
  @OrderBy("sortOrder")
  private final List<QuestionAnswer> questionAnswers = new ArrayList<QuestionAnswer>();

  public SurveyResultSection() {}

  public SurveyResultSection(final SurveyResult surveyResult,
      final SurveySection surveySection) {
    setSurveyResult(surveyResult);
    setSurveySection(surveySection);
  }

  /**
   * Add question answer link to survey result section.
   */
  public void addQuestionAnswer(final QuestionAnswer questionAnswer) {
    if (!questionAnswers.contains(questionAnswer)) {
      questionAnswer.setSortOrder(questionAnswers.size());
      questionAnswers.add(questionAnswer);
    }
    questionAnswer.setSection(this);
  }

  public List<QuestionAnswer> getQuestionAnswers() {
    return questionAnswers;
  }

  public Long getSlug() {
    return super.getId();
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public SurveyResult getSurveyResult() {
    return surveyResult;
  }

  public SurveySection getSurveySection() {
    return surveySection;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setSurveyResult(final SurveyResult surveyResult) {
    this.surveyResult = surveyResult;
  }

  /**
   * Set survey section, default sortOrder basing on section sort order.
   */
  public void setSurveySection(final SurveySection surveySection) {
    this.surveySection = surveySection;
    if (surveySection != null) {
      setSortOrder(surveySection.getSortOrder());
    }
  }
}
