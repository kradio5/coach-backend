package com.ydw.survey.model;

public interface HasVariableName {

  String getVariableName();

}
