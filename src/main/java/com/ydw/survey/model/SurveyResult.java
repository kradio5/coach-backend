package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "survey_result")
public class SurveyResult extends AbstractAuditableEntity<Long> {

  @Projection(types = SurveyResult.class, name = "audit")
  public interface Audit extends AuditProjection {
  }

  @Projection(types = SurveyResult.class, name = "list")
  public interface ListProjection {

    ZonedDateTime getCreatedDateTime();

    Long getSlug();

    Survey getSurvey();

    @Value("#{@authService.users().findByAccountId(target.createdBy).content}")
    AuthUserListProjection getUserCreatedBy();

    boolean isDeleted();

  }

  private static final long serialVersionUID = 5633990333058853624L;

  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_id")
  private Survey survey;

  @Column(name = "deleted")
  private boolean deleted;

  @ManyToOne
  @JoinColumn(name = "survey_campaign_id")
  private SurveyCampaign surveyCampaign;

  @Transient
  private boolean lastInCampaign;

  @Column(name = "campaign_answer_counter")
  private int campaignAnswerCounter = 0;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "surveyResult",
      orphanRemoval = true)
  @OrderBy("sortOrder")
  private final List<SurveyResultSection> sections = new ArrayList<SurveyResultSection>();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "surveyResult",
      orphanRemoval = true)
  @OrderBy("sortOrder")
  private final List<SurveyResultCalculation> calculations =
      new ArrayList<SurveyResultCalculation>();

  @Column(name = "completed")
  private boolean completed;

  /**
   * Do not store, designates that calculation should be started.
   */
  @Transient
  private boolean calculate;

  /**
   * Add Calculation to survey.
   */
  public void addCalculation(final SurveyResultCalculation calculation) {
    if (!calculations.contains(calculation)) {
      calculation.setSortOrder(calculations.size());
      calculations.add(calculation);
    }
    calculation.setSurveyResult(this);
  }

  /**
   * Add section to survey.
   */
  public void addSection(final SurveyResultSection section) {
    if (!sections.contains(section)) {
      section.setSortOrder(sections.size());
      sections.add(section);
    }
    section.setSurveyResult(this);
  }

  public List<SurveyResultCalculation> getCalculations() {
    return calculations;
  }

  public int getCampaignAnswerCounter() {
    return campaignAnswerCounter;
  }

  @Transient
  @DateTimeFormat(iso = ISO.DATE_TIME)
  public ZonedDateTime getCreatedDateTime() {
    return super.getCreated();
  }

  public List<SurveyResultSection> getSections() {
    return sections;
  }

  public Long getSlug() {
    return super.getId();
  }

  public Survey getSurvey() {
    return survey;
  }

  public SurveyCampaign getSurveyCampaign() {
    return surveyCampaign;
  }

  @Transient
  public String getUserCreatedBy() {
    return super.getCreatedBy();
  }

  public boolean isCalculate() {
    return calculate;
  }

  public boolean isCompleted() {
    return completed;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public boolean isLastInCampaign() {
    return lastInCampaign;
  }

  public void setCalculate(final boolean calculate) {
    this.calculate = calculate;
  }

  public void setCampaignAnswerCounter(final int campaignAnswerCounter) {
    this.campaignAnswerCounter = campaignAnswerCounter;
  }

  public void setCompleted(final boolean completed) {
    this.completed = completed;
  }

  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  public void setLastInCampaign(final boolean lastInCampaign) {
    this.lastInCampaign = lastInCampaign;
  }

  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  public void setSurveyCampaign(final SurveyCampaign surveyCampaign) {
    this.surveyCampaign = surveyCampaign;
  }
}
