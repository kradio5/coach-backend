package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_text_float")
@DiscriminatorValue("QuestionAnswerTextFloat")
public class QuestionAnswerTextFloat extends QuestionAnswer {

  @Projection(types = QuestionAnswerTextFloat.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    double getAnswerValue();

    String getAnswerValueText();

  }

  private static final long serialVersionUID = -5615416281854463250L;

  @Column(name = "answer_value")
  private double answerValue;

  @Column(name = "answer_value_text", length = 1000)
  private String answerValueText;

  public QuestionAnswerTextFloat() {}

  public QuestionAnswerTextFloat(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public double getAnswerValue() {
    return answerValue;
  }

  public String getAnswerValueText() {
    return answerValueText;
  }

  @Override
  @Transient
  public Object getValue() {
    return new TextFloatValue(answerValue, answerValueText);
  }

  /**
   * Set answer value.
   */
  public void setAnswerValue(final double answerValue) {
    this.answerValue = answerValue;
  }

  /**
   * Set answer text.
   */
  public void setAnswerValueText(final String answerValueText) {
    this.answerValueText = answerValueText;
  }

}
