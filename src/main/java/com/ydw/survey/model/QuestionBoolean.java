package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question_boolean")
@DiscriminatorValue("QuestionBoolean")
public class QuestionBoolean extends Question {

  @Projection(types = QuestionBoolean.class)
  public interface Excerpt extends Question.Excerpt {

  }

  private static final long serialVersionUID = -6206916833339631190L;

  public QuestionBoolean() {
    setQuestionType(QuestionType.BOOLEAN);
  }

}
