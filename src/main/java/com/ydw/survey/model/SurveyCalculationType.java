package com.ydw.survey.model;

/**
 * Survey calculation types.
 */
public enum SurveyCalculationType {

  TEXT(Short.valueOf((short) 1)),

  SCALE(Short.valueOf((short) 2));

  private Short surveyCalculationType;

  private SurveyCalculationType(final Short surveyCalculationType) {
    this.surveyCalculationType = surveyCalculationType;
  }

  public Short getSurveyCalculationType() {
    return surveyCalculationType;
  }
}
