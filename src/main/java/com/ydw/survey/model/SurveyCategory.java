package com.ydw.survey.model;

import com.ydw.survey.content.model.Category;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(SurveyCategory.TAXONOMY)
@JsonInclude(Include.NON_NULL)
public class SurveyCategory extends Category {

  @Projection(types = SurveyCategory.class, name = "list")
  @JsonInclude(Include.NON_NULL)
  public interface SurveyCategoryListProjection extends CategoryListProjection {
  }

  private static final long serialVersionUID = -6065524903172399760L;

  public static final String TAXONOMY = "SurveyCategory";

  public SurveyCategory() {
    super(TAXONOMY);
  }

  public SurveyCategory(final String title) {
    super(TAXONOMY, title);
  }

  public SurveyCategory(final String title, final Category parent) {
    super(TAXONOMY, title, parent);
  }

}
