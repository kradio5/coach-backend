package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_boolean")
@DiscriminatorValue("QuestionAnswerBoolean")
public class QuestionAnswerBoolean extends QuestionAnswer {

  @Projection(types = QuestionAnswerBoolean.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    boolean getAnswerValue();

  }

  private static final long serialVersionUID = -1178395947916264768L;

  @Column(name = "answer_value")
  private boolean answerValue;

  public QuestionAnswerBoolean() {}

  public QuestionAnswerBoolean(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  @Override
  @Transient
  public Object getValue() {
    // yes: 1, no: 0
    return answerValue ? 1 : 0;
  }

  public boolean isAnswerValue() {
    return answerValue;
  }

  public void setAnswerValue(final boolean answerValue) {
    this.answerValue = answerValue;
  }
}
