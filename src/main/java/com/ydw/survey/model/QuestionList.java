package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "question_list")
@DiscriminatorValue("QuestionList")
public class QuestionList extends Question {

  @Projection(types = QuestionList.class)
  public interface Excerpt extends Question.Excerpt {

    QuestionListAddValueType getAddValueType();

    List<QuestionListValue> getListValues();

    boolean isAddValueEditable();

  }

  private static final long serialVersionUID = -1485251975062171163L;

  /**
   * Define type of additional value.
   */
  @Enumerated(EnumType.STRING)
  @Column(name = "add_value_type")
  private QuestionListAddValueType addValueType = QuestionListAddValueType.INTEGER;

  /**
   * Define if additional value could be edited by user (during being interviewed).
   */
  @Column(name = "add_value_editable")
  private boolean addValueEditable;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  @JoinColumn(name = "question_list_id")
  // @ElementCollection
  // @CollectionTable(name = "question_list_value",
  // joinColumns = @JoinColumn(name = "question_list_id") ,
  // uniqueConstraints = @UniqueConstraint(columnNames = { "question_list_id", "title" }) )
  // // @OrderColumn(name = "sort_order")
  @OrderBy("listValueIndex") // TODO: remove field in QuestionListValue (see issue #18)
  private List<QuestionListValue> listValues = new ArrayList<>();

  public QuestionList() {
    setQuestionType(QuestionType.LIST);
  }

  /**
   * Add value to list.
   */
  public void addListValue(final QuestionListValue listValue) {
    if (!listValues.contains(listValue)) {
      listValue.setListValueIndex(listValues.size());
      listValues.add(listValue);
    }
  }

  public QuestionListAddValueType getAddValueType() {
    return addValueType;
  }

  public List<QuestionListValue> getListValues() {
    return listValues;
  }

  public boolean isAddValueEditable() {
    return addValueEditable;
  }

  public void setAddValueEditable(final boolean addValueEditable) {
    this.addValueEditable = addValueEditable;
  }

  public void setAddValueType(final QuestionListAddValueType addValueType) {
    this.addValueType = addValueType;
  }

  public void setListValues(final List<QuestionListValue> listValues) {
    this.listValues = listValues;
  }
}
