package com.ydw.survey.model;

import com.ydw.common.data.model.LocalizedString;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "question_scale")
@DiscriminatorValue("QuestionScale")
public class QuestionScale extends Question {

  @Projection(types = QuestionScale.class)
  public interface Excerpt extends Question.Excerpt {

    int getMaxValue();

    String getMaxValueIcon();

    LocalizedString getMaxValueTitle();

    int getMinValue();

    String getMinValueIcon();

    LocalizedString getMinValueTitle();

    int getStepAmount();

  }

  private static final long serialVersionUID = 377100691792125042L;

  @Lob
  @Column(name = "min_value_title")
  private LocalizedString minValueTitle;

  @Lob
  @Column(name = "max_value_title")
  private LocalizedString maxValueTitle;

  @Column(name = "min_value_icon")
  private String minValueIcon;

  @Column(name = "max_value_icon")
  private String maxValueIcon;

  @NotNull
  @Column(name = "min_value")
  private int minValue = 0;

  @NotNull
  @Column(name = "max_value")
  private int maxValue = 1;

  @NotNull
  @Min(1)
  @Column(name = "step_amount")
  private int stepAmount = 1;

  public QuestionScale() {
    setQuestionType(QuestionType.SCALE);
  }

  public int getMaxValue() {
    return maxValue;
  }

  public String getMaxValueIcon() {
    return maxValueIcon;
  }

  public LocalizedString getMaxValueTitle() {
    return maxValueTitle;
  }

  public int getMinValue() {
    return minValue;
  }

  public String getMinValueIcon() {
    return minValueIcon;
  }

  public LocalizedString getMinValueTitle() {
    return minValueTitle;
  }

  public int getStepAmount() {
    return stepAmount;
  }

  /**
   * Performs simple field cleanups before entity is saved.
   */
  @Override
  @PrePersist
  @PreUpdate
  public void preSave() {
    if (minValue >= maxValue) {
      maxValue = minValue + 1;
    }
  }

  public void setMaxValue(final int maxValue) {
    this.maxValue = maxValue;
  }

  public void setMaxValueIcon(final String maxValueIcon) {
    this.maxValueIcon = maxValueIcon;
  }

  public void setMaxValueTitle(final LocalizedString maxValueTitle) {
    this.maxValueTitle = maxValueTitle;
  }

  public void setMinValue(final int minValue) {
    this.minValue = minValue;
  }

  public void setMinValueIcon(final String minValueIcon) {
    this.minValueIcon = minValueIcon;
  }

  public void setMinValueTitle(final LocalizedString minValueTitle) {
    this.minValueTitle = minValueTitle;
  }

  public void setStepAmount(final int stepAmount) {
    this.stepAmount = stepAmount;
  }

}
