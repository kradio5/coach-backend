package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "question_answer", uniqueConstraints = @UniqueConstraint(
    columnNames = { "question_id", "survey_result_section_id" }))
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "dtype")
public abstract class QuestionAnswer extends AbstractAuditableEntity<Long>
    implements HasVariable {

  @Projection(types = QuestionAnswer.class)
  public interface Excerpt {

    Question getQuestion();

    Long getSlug();

    int getSortOrder();
  }

  private static final long serialVersionUID = 7310086400770892638L;

  @Column(name = "sort_order")
  private int sortOrder;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_result_section_id")
  private SurveyResultSection section;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "question_id")
  private Question question;

  public QuestionAnswer() {}

  public QuestionAnswer(final Question question, final SurveyResultSection section) {
    setQuestion(question);
    setSection(section);
  }

  public Question getQuestion() {
    return question;
  }

  public SurveyResultSection getSection() {
    return section;
  }

  public Long getSlug() {
    return super.getId();
  }

  public int getSortOrder() {
    return sortOrder;
  }

  @Override
  @Transient
  @JsonIgnore
  public abstract Object getValue();

  @Override
  @JsonIgnore
  public HasVariableName getVariableHolder() {
    return question;
  }

  /**
   * Assign question to the answer.
   */
  public void setQuestion(final Question question) {
    this.question = question;
    if (question != null) {
      setSortOrder(question.getSortOrder());
    }
  }

  public void setSection(final SurveyResultSection section) {
    this.section = section;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

}
