package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question_integer")
@DiscriminatorValue("QuestionInteger")
public class QuestionInteger extends Question {

  @Projection(types = QuestionInteger.class)
  public interface Excerpt extends Question.Excerpt {

  }

  private static final long serialVersionUID = 6155400946292379756L;

  public QuestionInteger() {
    setQuestionType(QuestionType.INTEGER);
  }

}
