package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;

import org.springframework.data.rest.core.config.Projection;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "survey_result_calculation")
public class SurveyResultCalculation extends AbstractAuditableEntity<Long>
    implements HasVariable {

  @Projection(types = SurveyResultCalculation.class)
  public interface Excerpt {

    String getResultCalculation();

    int getSortOrder();

    SurveyCalculation getSurveyCalculation();

  }

  private static final long serialVersionUID = -2186427030009437586L;

  private static final String INTEGER_REGEXP = "^(-?)\\d+$";

  private static final String DOUBLE_REGEXP = "^(-?)\\d+\\.\\d+$";

  @Column(name = "sort_order")
  private int sortOrder;

  @Lob
  @Column(name = "result_calculation")
  private String resultCalculation;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_result_id")
  private SurveyResult surveyResult;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_calculation_id")
  private SurveyCalculation surveyCalculation;

  public SurveyResultCalculation() {}

  /**
   * Initialize basic references.
   */
  public SurveyResultCalculation(final SurveyResult surveyResult,
      final SurveyCalculation surveyCalculation) {
    this();
    this.surveyResult = surveyResult;
    this.surveyCalculation = surveyCalculation;
    this.sortOrder = surveyCalculation.getSortOrder();
  }

  public String getResultCalculation() {
    return resultCalculation;
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public SurveyCalculation getSurveyCalculation() {
    return surveyCalculation;
  }

  public SurveyResult getSurveyResult() {
    return surveyResult;
  }

  @Override
  @JsonIgnore
  public Object getValue() {
    if (null == resultCalculation || resultCalculation.isEmpty()) {
      return resultCalculation;
    }
    Object value;
    try {
      if (resultCalculation.matches(INTEGER_REGEXP)) {
        value = Integer.parseInt(resultCalculation);
      } else if (resultCalculation.matches(DOUBLE_REGEXP)) {
        value = Double.parseDouble(resultCalculation);
      } else {
        value = resultCalculation;
      }
    } catch (final Exception exc) {
      value = resultCalculation;
    }
    return value;
  }

  @Override
  @JsonIgnore
  public HasVariableName getVariableHolder() {
    return surveyCalculation;
  }

  public void setResultCalculation(final String resultCalculation) {
    this.resultCalculation = resultCalculation;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setSurveyCalculation(final SurveyCalculation surveyCalculation) {
    this.surveyCalculation = surveyCalculation;
  }

  public void setSurveyResult(final SurveyResult surveyResult) {
    this.surveyResult = surveyResult;
  }
}
