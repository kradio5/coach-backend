package com.ydw.survey.model;

import java.io.Serializable;

public class QuestionListValueHelper implements Serializable {

  private static final long serialVersionUID = 5741841060856827031L;

  private String title;

  private int listValueIndex;

  private String listValueAddValue;

  public QuestionListValueHelper() {}

  /**
   * Initialize object.
   */
  public QuestionListValueHelper(final String title, final int listValueIndex,
      final String listValueAddValue) {
    this.title = title;
    this.listValueIndex = listValueIndex;
    this.listValueAddValue = listValueAddValue;
  }

  public String getListValueAddValue() {
    return listValueAddValue;
  }

  public int getListValueIndex() {
    return listValueIndex;
  }

  public String getTitle() {
    return title;
  }

  public void setListValueAddValue(final String listValueAddValue) {
    this.listValueAddValue = listValueAddValue;
  }

  public void setListValueIndex(final int listValueIndex) {
    this.listValueIndex = listValueIndex;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

}
