package com.ydw.survey.model;

import com.ydw.common.data.model.LocalizedString;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "survey_calculation_scale")
@DiscriminatorValue("SurveyCalculationScale")
public class SurveyCalculationScale extends SurveyCalculation {

  private static final long serialVersionUID = 1064854099217971842L;

  @Lob
  @Column(name = "min_value_title")
  private LocalizedString minValueTitle;

  @Lob
  @Column(name = "max_value_title")
  private LocalizedString maxValueTitle;

  @Column(name = "min_value_icon")
  private String minValueIcon;

  @Column(name = "max_value_icon")
  private String maxValueIcon;

  @NotNull
  @Column(name = "min_value")
  private int minValue = 0;

  @NotNull
  @Column(name = "max_value")
  private int maxValue = 1;

  @Column(name = "show_value")
  private boolean showValue = true;

  @Column(name = "fraction_digit_amount")
  private int fractionDigitAmount = 0;

  public SurveyCalculationScale() {
    setSurveyCalculationType(SurveyCalculationType.SCALE);
  }

  /**
   * Initialize object.
   */
  public SurveyCalculationScale(final int minValue, final String minValueTitle,
      final int maxValue, final String maxValueTitle) {
    this();
    this.minValue = minValue;
    this.minValueTitle = new LocalizedString(minValueTitle);
    this.maxValue = maxValue;
    this.maxValueTitle = new LocalizedString(maxValueTitle);
  }

  public int getFractionDigitAmount() {
    return fractionDigitAmount;
  }

  public int getMaxValue() {
    return maxValue;
  }

  public String getMaxValueIcon() {
    return maxValueIcon;
  }

  public LocalizedString getMaxValueTitle() {
    return maxValueTitle;
  }

  public int getMinValue() {
    return minValue;
  }

  public String getMinValueIcon() {
    return minValueIcon;
  }

  public LocalizedString getMinValueTitle() {
    return minValueTitle;
  }

  public boolean isShowValue() {
    return showValue;
  }

  public void setFractionDigitAmount(final int fractionDigitAmount) {
    this.fractionDigitAmount = fractionDigitAmount;
  }

  public void setMaxValue(final int maxValue) {
    this.maxValue = maxValue;
  }

  public void setMaxValueIcon(final String maxValueIcon) {
    this.maxValueIcon = maxValueIcon;
  }

  public void setMaxValueTitle(final LocalizedString maxValueTitle) {
    this.maxValueTitle = maxValueTitle;
  }

  public void setMinValue(final int minValue) {
    this.minValue = minValue;
  }

  public void setMinValueIcon(final String minValueIcon) {
    this.minValueIcon = minValueIcon;
  }

  public void setMinValueTitle(final LocalizedString minValueTitle) {
    this.minValueTitle = minValueTitle;
  }

  public void setShowValue(final boolean showValue) {
    this.showValue = showValue;
  }

}
