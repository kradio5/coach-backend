package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_float")
@DiscriminatorValue("QuestionAnswerFloat")
public class QuestionAnswerFloat extends QuestionAnswer {

  @Projection(types = QuestionAnswerFloat.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    double getAnswerValue();

  }

  private static final long serialVersionUID = -5615416281854463250L;

  @Column(name = "answer_value")
  private double answerValue;

  public QuestionAnswerFloat() {}

  public QuestionAnswerFloat(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public double getAnswerValue() {
    return answerValue;
  }

  @Override
  @Transient
  public Object getValue() {
    return answerValue;
  }

  public void setAnswerValue(final double answerValue) {
    this.answerValue = answerValue;
  }

}
