package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;
import com.ydw.common.data.model.LocalizedString;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.rest.core.config.Projection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "survey")
public class Survey extends AbstractAuditableEntity<Long> /* implements HasGroupAccess */ {

  @Projection(types = Survey.class, name = "audit")
  public interface Audit extends AuditProjection {
  }

  private static final long serialVersionUID = 8552671746597246390L;

  // @JsonIgnore
  // @Embedded
  // private EntityGroupAccess access = new EntityGroupAccess();

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "survey",
      orphanRemoval = true)
  @OrderBy("sortOrder")
  private List<SurveyCalculation> calculations = new ArrayList<SurveyCalculation>();

  @Lob
  @Column(name = "description")
  private LocalizedString description;

  @Lob
  @Column(name = "note")
  private LocalizedString note;

  /**
   * TODO: remove column from DB.
   */
  // @Column(name = "is_public")
  // private boolean isPublic;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "survey",
      orphanRemoval = true)
  @OrderBy("sortOrder")
  @Size(min = 1)
  private List<SurveySection> sections = new ArrayList<SurveySection>();

  @Field(type = FieldType.Nested)
  @ManyToMany
  @JoinTable(name = "survey_categories", joinColumns = @JoinColumn(name = "survey_id"),
      inverseJoinColumns = @JoinColumn(name = "category_id"))
  private final Set<SurveyCategory> categories = new HashSet<>();

  @Transient
  private String singleCategorySlug;

  @Column(name = "deleted")
  private boolean deleted;

  @Lob
  @NotNull
  @Column(name = "title", columnDefinition = "LONGTEXT")
  private LocalizedString title;

  /**
   * Do not store, designates that calculation should be validated.
   */
  @Transient
  private boolean validate;

  /**
   * Add calculation to survey.
   */
  public void addCalculation(final SurveyCalculation calculation) {
    if (!calculations.contains(calculation)) {
      calculation.setSortOrder(calculations.size());
      calculations.add(calculation);
    }
    calculation.setSurvey(this);
  }

  /**
   * Adds a category.
   */
  public void addCategory(final SurveyCategory category) {
    if (!categories.contains(category)) {
      categories.add(category);
    }
  }

  /**
   * Add section to survey.
   */
  public void addSection(final SurveySection section) {
    if (!sections.contains(section)) {
      section.setSortOrder(sections.size());
      sections.add(section);
    }
    section.setSurvey(this);
  }

  public List<SurveyCalculation> getCalculations() {
    return calculations;
  }

  // @Override
  // public EntityGroupAccess getAccess() {
  // return access;
  // }

  public Set<SurveyCategory> getCategories() {
    return categories;
  }

  public LocalizedString getDescription() {
    return description;
  }

  public LocalizedString getNote() {
    return note;
  }

  public List<SurveySection> getSections() {
    return sections;
  }

  public String getSingleCategorySlug() {
    return singleCategorySlug;
  }

  public Long getSlug() {
    return super.getId();
  }

  public LocalizedString getTitle() {
    return title;
  }

  public boolean isDeleted() {
    return deleted;
  }

  // public void setAccess(final EntityGroupAccess access) {
  // this.access = access;
  // }

  public boolean isValidate() {
    return validate;
  }

  /**
   * Update calculations of survey.
   */
  public void setCalculations(final List<SurveyCalculation> calculations) {
    if (this.calculations == null) {
      this.calculations = calculations;
    } else {
      this.calculations.clear();
      int sortOrder = 0;
      for (final SurveyCalculation calculation : calculations) {
        calculation.setSurvey(this);
        calculation.setSortOrder(sortOrder++);
      }
      this.calculations.addAll(calculations);
    }
  }

  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setNote(final LocalizedString note) {
    this.note = note;
  }

  /**
   * Update sections of survey.
   */
  public void setSections(final List<SurveySection> sections) {
    if (this.sections == null) {
      this.sections = sections;
    } else {
      this.sections.clear();
      int sortOrder = 0;
      for (final SurveySection section : sections) {
        section.setSurvey(this);
        section.setSortOrder(sortOrder++);
      }
      this.sections.addAll(sections);
    }
  }

  public void setSingleCategorySlug(final String singleCategorySlug) {
    this.singleCategorySlug = singleCategorySlug;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

  public void setValidate(final boolean validate) {
    this.validate = validate;
  }
}
