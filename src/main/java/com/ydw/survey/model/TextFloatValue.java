package com.ydw.survey.model;

public class TextFloatValue implements Comparable<Object>, IDoubleValue {

  /**
   * Keep variables public to allow accessing them via Spel.
   */
  public double value;

  public String text;

  public TextFloatValue(final double value, final String text) {
    this.value = value;
    this.text = text;
  }

  /**
   * Compare current object with every other object that can be considered as double.
   */
  @Override
  public int compareTo(final Object anotherObj) {
    return Double.compare(value, getDoubleValue(anotherObj));
  }

  /**
   * Check equality of current object with every other object that can be considered as double.
   * <br />
   * Implementation of method is needed to conform to java requirements that method "compareTo"
   * returns 0 only if method "equals" returns 0
   */
  @Override
  public boolean equals(final Object anotherObj) {
    return value == getDoubleValue(anotherObj);
  }

  @Override
  public double getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  private double getDoubleValue(final Object anotherObj) {
    double anotherValue;
    if (anotherObj == null) {
      anotherValue = 0d;
    } else if (IDoubleValue.class.isAssignableFrom(anotherObj.getClass())) {
      anotherValue = ((IDoubleValue) anotherObj).getValue();
    } else if (Number.class.isAssignableFrom(anotherObj.getClass())) {
      anotherValue = ((Number) anotherObj).doubleValue();
    } else {
      anotherValue = Double.parseDouble(anotherObj.toString());
    }
    return anotherValue;
  }

}
