package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_integer")
@DiscriminatorValue("QuestionAnswerInteger")
public class QuestionAnswerInteger extends QuestionAnswer {

  @Projection(types = QuestionAnswerInteger.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    int getAnswerValue();

  }

  private static final long serialVersionUID = -5809718062693097470L;

  @Column(name = "answer_value")
  private int answerValue;

  public QuestionAnswerInteger() {}

  public QuestionAnswerInteger(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public int getAnswerValue() {
    return answerValue;
  }

  @Override
  @Transient
  public Object getValue() {
    return answerValue;
  }

  public void setAnswerValue(final int answerValue) {
    this.answerValue = answerValue;
  }

}
