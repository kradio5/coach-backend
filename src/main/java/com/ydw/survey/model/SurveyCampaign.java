package com.ydw.survey.model;

import com.ydw.common.data.IdentityUtils;
import com.ydw.common.data.model.AbstractAuditableEntity;
import com.ydw.common.data.model.LocalizedString;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "survey_campaign")
public class SurveyCampaign extends AbstractAuditableEntity<Long> {

  private static final long serialVersionUID = 788259270012374790L;

  @Lob
  @NotNull
  @Column(name = "title")
  private LocalizedString title = new LocalizedString();

  @Column(name = "deleted")
  private boolean deleted;

  @Lob
  @Column(name = "description")
  private LocalizedString description;

  @Column(name = "active")
  private boolean active = true;

  @Column(name = "allow_answer_changes")
  private boolean allowAnswerChanges;

  @Column(name = "uuid", unique = true, nullable = false)
  private UUID uuid = IdentityUtils.uuid();

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @Column(name = "start_date")
  private ZonedDateTime startDate;

  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  @Column(name = "expire_date")
  private ZonedDateTime expireDate;

  @ManyToMany
  @JoinTable(name = "campaign_surveys", joinColumns = @JoinColumn(name = "survey_campaign_id"),
      inverseJoinColumns = @JoinColumn(name = "survey_id"))
  @OrderColumn(name = "sort_order")
  private List<Survey> surveys = new ArrayList<Survey>();

  /**
   * Adds a survey.
   */
  public void addSurvey(final Survey survey) {
    if (!getSurveys().contains(survey)) {
      this.getSurveys().add(survey);
    }
  }

  public LocalizedString getDescription() {
    return description;
  }

  public ZonedDateTime getExpireDate() {
    return expireDate;
  }

  public String getSlug() {
    return String.valueOf(getUuid());
  }

  public ZonedDateTime getStartDate() {
    return startDate;
  }

  public List<Survey> getSurveys() {
    return surveys;
  }

  public LocalizedString getTitle() {
    return title;
  }

  public UUID getUuid() {
    return uuid;
  }

  public boolean isActive() {
    return active;
  }

  public boolean isAllowAnswerChanges() {
    return allowAnswerChanges;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public void setActive(final boolean active) {
    this.active = active;
  }

  public void setAllowAnswerChanges(final boolean allowAnswerChanges) {
    this.allowAnswerChanges = allowAnswerChanges;
  }

  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setExpireDate(final ZonedDateTime expireDate) {
    this.expireDate = expireDate;
  }

  public void setStartDate(final ZonedDateTime startDate) {
    this.startDate = startDate;
  }

  public void setSurveys(final List<Survey> surveys) {
    this.surveys = surveys;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

  public void setUuid(final UUID uuid) {
    this.uuid = uuid;
  }

}
