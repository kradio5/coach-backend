package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_list")
@DiscriminatorValue("QuestionAnswerList")
public class QuestionAnswerList extends QuestionAnswer {

  @Projection(types = QuestionAnswerList.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    String getAnswerAddValue();

    String getAnswerValue();

  }

  private static final long serialVersionUID = -2099918976462778415L;

  @Lob
  @Column(name = "answer_value")
  private String answerValue;

  @Column(name = "answer_add_value", length = 1000)
  private String answerAddValue;

  public QuestionAnswerList() {}

  public QuestionAnswerList(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public String getAnswerAddValue() {
    return answerAddValue;
  }

  public String getAnswerValue() {
    return answerValue;
  }

  @Override
  @Transient
  public Object getValue() {
    final QuestionList question = (QuestionList) getQuestion();
    Object value = null;
    switch (question.getAddValueType()) {
      case INTEGER:
        try {
          value = Integer.parseInt(getAnswerAddValue());
        } catch (final Exception exc) {
          value = 0;
        }
        break;
      case FLOAT:
        try {
          value = Double.parseDouble(getAnswerAddValue());
        } catch (final Exception exc) {
          value = 0d;
        }
        break;
      default:
        value = null != getAnswerAddValue() ? getAnswerAddValue() : "";
    }
    return value;
  }

  public void setAnswerAddValue(final String answerAddValue) {
    this.answerAddValue = answerAddValue;
  }

  public void setAnswerValue(final String answerValue) {
    this.answerValue = answerValue;
  }
}
