package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "question_float")
@DiscriminatorValue("QuestionFloat")
public class QuestionFloat extends Question {

  @Projection(types = QuestionFloat.class)
  public interface Excerpt extends Question.Excerpt {

  }

  private static final long serialVersionUID = 3948253182075035637L;

  public QuestionFloat() {
    setQuestionType(QuestionType.FLOAT);
  }

}
