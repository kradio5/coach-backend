package com.ydw.survey.model;

import com.ydw.common.data.model.LocalizedString;

import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.hateoas.Identifiable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "survey_calculation")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "dtype")
public abstract class SurveyCalculation extends AbstractPersistable<Long>
    implements Identifiable<Long>, HasVariableName {

  private static final long serialVersionUID = -4236743052264523047L;

  @Column(name = "sort_order")
  private int sortOrder;

  @Lob
  @Column(name = "title")
  private LocalizedString title;

  @Column(name = "deleted")
  private boolean deleted;

  @Lob
  @Column(name = "template")
  private LocalizedString template;

  @Column(name = "calc_condition", length = 1000)
  private String condition;

  @Column(name = "variableName")
  private String variableName;

  @Enumerated(EnumType.STRING)
  @Column(name = "survey_calculation_type")
  private SurveyCalculationType surveyCalculationType;

  @Column(name = "visible")
  private boolean visible;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_id")
  private Survey survey;

  public String getCondition() {
    return condition;
  }

  public Long getSlug() {
    return super.getId();
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public Survey getSurvey() {
    return survey;
  }

  public SurveyCalculationType getSurveyCalculationType() {
    return surveyCalculationType;
  }

  public LocalizedString getTemplate() {
    return template;
  }

  public LocalizedString getTitle() {
    return title;
  }

  public SurveyCalculationType getType() {
    return surveyCalculationType;
  }

  @Override
  public String getVariableName() {
    return variableName;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public boolean isVisible() {
    return visible;
  }

  public void setCondition(final String condition) {
    this.condition = condition;
  }

  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setSurvey(final Survey survey) {
    this.survey = survey;
  }

  public void setSurveyCalculationType(final SurveyCalculationType surveyCalculationType) {
    this.surveyCalculationType = surveyCalculationType;
  }

  public void setTemplate(final LocalizedString template) {
    this.template = template;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

  public void setVariableName(final String variableName) {
    this.variableName = variableName;
  }

  public void setVisible(final boolean visible) {
    this.visible = visible;
  }

}
