package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_text")
@DiscriminatorValue("QuestionAnswerText")
public class QuestionAnswerText extends QuestionAnswer {

  @Projection(types = QuestionAnswerText.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    String getAnswerValue();

  }

  private static final long serialVersionUID = -2099918976462778415L;

  @Lob
  @Column(name = "answer_value")
  private String answerValue;

  public QuestionAnswerText() {}

  public QuestionAnswerText(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public String getAnswerValue() {
    return answerValue;
  }

  @Override
  @Transient
  public Object getValue() {
    return answerValue;
  }

  public void setAnswerValue(final String answerValue) {
    this.answerValue = answerValue;
  }

}
