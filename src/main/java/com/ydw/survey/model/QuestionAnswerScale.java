package com.ydw.survey.model;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "question_answer_scale")
@DiscriminatorValue("QuestionAnswerScale")
public class QuestionAnswerScale extends QuestionAnswer {

  @Projection(types = QuestionAnswerScale.class)
  public interface Excerpt extends QuestionAnswer.Excerpt {

    double getAnswerValue();

    int getStep();

  }

  private static final double ANSWER_VALUE_ACCURACY = 0.00001d;

  private static final long serialVersionUID = -92682528414837659L;

  @Column(name = "answer_value")
  private double answerValue;

  public QuestionAnswerScale() {}

  public QuestionAnswerScale(final Question question, final SurveyResultSection section) {
    super(question, section);
  }

  public double getAnswerValue() {
    return answerValue;
  }

  /**
   * Calculate current step basing on answerValue.
   */
  @Transient
  public int getStep() {
    int step;
    final QuestionScale question = (QuestionScale) getQuestion();
    if (answerValue < question.getMinValue() - ANSWER_VALUE_ACCURACY
        || answerValue > question.getMaxValue() + ANSWER_VALUE_ACCURACY) {
      step = 0;
    } else {
      final double stepValue = (double) (question.getMaxValue() - question.getMinValue())
          / (double) (question.getStepAmount());
      for (step = 0; step <= question.getStepAmount(); step++) {
        if (Math.abs(
            answerValue - stepValue * step - question.getMinValue()) < ANSWER_VALUE_ACCURACY) {
          break;
        }
      }
      if (step > question.getStepAmount()) {
        step = 0;
      }
    }
    return step;
  }

  @Override
  @Transient
  public Object getValue() {
    if ((int) answerValue == answerValue) {
      return (int) answerValue;
    } else {
      return answerValue;
    }
  }

  public void setAnswerValue(final double answerValue) {
    this.answerValue = answerValue;
  }

  /**
   * Calculate value basing on step.
   */
  public void setStep(final int step) {
    final QuestionScale question = (QuestionScale) getQuestion();
    if (step >= 0 && step <= question.getStepAmount()) {
      final double stepValue = (double) (question.getMaxValue() - question.getMinValue())
          / (double) (question.getStepAmount());
      setAnswerValue(question.getMinValue() + stepValue * step);
    }
  }

}
