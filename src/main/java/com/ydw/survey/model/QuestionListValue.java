package com.ydw.survey.model;

import com.ydw.common.data.IdentityUtils;
import com.ydw.common.data.model.AbstractEntity;
import com.ydw.common.data.model.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

// @Embeddable
@Entity
@Table(name = "question_list_value")
public class QuestionListValue extends AbstractEntity<Long> {

  private static final long serialVersionUID = 5188055796011209311L;

  @Lob
  @NotNull
  @Column(name = "title")
  private LocalizedString title;

  /**
   * Key to compare existing object with coming from UI.<br />
   * Cannot use Id here since any change on QuestionList.listValues causes cascade removal of all
   * previous listValues and attempt to save detached object -> OptimisticLockingException.
   */
  @Column(name = "list_value_id", nullable = false)
  private String listValueId;

  /**
   * Store index of list value (can be repeatable).
   */
  // TODO: remove field (see issue #18)
  // @Transient
  @NotNull
  @Column(name = "list_value_index")
  private int listValueIndex;

  /**
   * Store additional value (string, integer or double are stored in this field).
   */
  @Column(name = "list_value_add_value", length = 1000)
  private String listValueAddValue;

  public QuestionListValue() {}

  /**
   * Initializes list value.
   */
  public QuestionListValue(final String title, final int listValueIndex,
      final String addValue) {
    this.title = new LocalizedString(title);
    this.listValueIndex = listValueIndex;
    if (addValue != null) {
      this.listValueAddValue = addValue;
    } else {
      this.listValueAddValue = String.valueOf(listValueIndex);
    }
  }

  public String getListValueAddValue() {
    return listValueAddValue;
  }

  public String getListValueId() {
    return listValueId;
  }

  public int getListValueIndex() {
    return listValueIndex;
  }

  public LocalizedString getTitle() {
    return title;
  }

  /**
   * Generates default list value ID.
   */
  @PrePersist
  public void preCreate() {
    if (listValueId == null) {
      listValueId = IdentityUtils.generateKey();
    }
  }

  public void setListValueAddValue(final String listValueAddValue) {
    this.listValueAddValue = listValueAddValue;
  }

  public void setListValueId(final String listValueId) {
    this.listValueId = listValueId;
  }

  public void setListValueIndex(final int listValueIndex) {
    this.listValueIndex = listValueIndex;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }
}
