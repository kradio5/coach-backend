package com.ydw.survey.model;

import com.ydw.common.data.model.AbstractAuditableEntity;
import com.ydw.common.data.model.LocalizedString;

import org.springframework.data.rest.core.config.Projection;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "question")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "dtype")
public abstract class Question extends AbstractAuditableEntity<Long>
    implements HasVariableName {

  @Projection(types = Question.class)
  public interface Excerpt {

    long getCopyFromSlug();

    LocalizedString getDescription();

    QuestionType getQuestionType();

    Long getSlug();

    int getSortOrder();

    LocalizedString getTitle();

    String getVariableName();

    boolean isDeleted();

    boolean isMandatory();

  }

  private static final long serialVersionUID = -1086049022416733411L;

  @Lob
  @NotNull
  @Column(name = "title")
  private LocalizedString title;

  @Lob
  @Column(name = "description")
  private LocalizedString description;

  @Enumerated(EnumType.STRING)
  @Column(name = "question_type")
  private QuestionType questionType;

  @Column(name = "variable_name")
  private String variableName;

  @Column(name = "deleted")
  private boolean deleted;

  @Column(name = "is_mandatory")
  private boolean mandatory = true;

  @Column(name = "sort_order")
  private int sortOrder;

  @Column(name = "copy_from_slug")
  private long copyFromSlug;

  @NotNull
  @ManyToOne(optional = false)
  @JoinColumn(name = "survey_section_id")
  private SurveySection section;

  public long getCopyFromSlug() {
    return copyFromSlug;
  }

  public LocalizedString getDescription() {
    return description;
  }

  public QuestionType getQuestionType() {
    return questionType;
  }

  public SurveySection getSection() {
    return section;
  }

  public Long getSlug() {
    return super.getId();
  }

  public int getSortOrder() {
    return sortOrder;
  }

  public LocalizedString getTitle() {
    return title;
  }

  @Override
  public String getVariableName() {
    return variableName;
  }

  public boolean isDeleted() {
    return deleted;
  }

  public boolean isMandatory() {
    return mandatory;
  }

  /**
   * Performs simple field calculations and cleanups before entity is saved.
   */
  @PrePersist
  @PreUpdate
  public void preSave() {
    if (null != variableName && variableName.trim().isEmpty()) {
      variableName = null;
    }
  }

  public void setCopyFromSlug(final long copyFromSlug) {
    this.copyFromSlug = copyFromSlug;
  }

  public void setDeleted(final boolean deleted) {
    this.deleted = deleted;
  }

  public void setDescription(final LocalizedString description) {
    this.description = description;
  }

  public void setMandatory(final boolean mandatory) {
    this.mandatory = mandatory;
  }

  public void setQuestionType(final QuestionType questionType) {
    this.questionType = questionType;
  }

  public void setSection(final SurveySection section) {
    this.section = section;
  }

  public void setSortOrder(final int sortOrder) {
    this.sortOrder = sortOrder;
  }

  public void setTitle(final LocalizedString title) {
    this.title = title;
  }

  public void setVariableName(final String variableName) {
    this.variableName = variableName;
  }

}
