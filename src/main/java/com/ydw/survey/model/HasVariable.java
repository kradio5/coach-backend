package com.ydw.survey.model;

public interface HasVariable {

  Object getValue();

  HasVariableName getVariableHolder();
}
