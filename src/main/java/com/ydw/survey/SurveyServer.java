package com.ydw.survey;

import com.ydw.cloud.auth.EnableAuthService;
import com.ydw.common.data.EnableYoudowellCommonData;
import com.ydw.common.data.slug.EnableSlugServices;
import com.ydw.common.i18n.EnableI18n;
import com.ydw.common.jpa.eclipselink.EnableEclipselinkJpa;

import com.github.vanroy.springboot.autoconfigure.data.jest.ElasticsearchJestAWSAutoConfiguration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.cache.ElastiCacheAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextCredentialsAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextInstanceDataAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextResourceLoaderAutoConfiguration;
import org.springframework.cloud.aws.context.config.annotation.ContextResourceLoaderConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The main Youdowell Survey server application.
 * <p>
 * Can be run stand-alone or included into other server applications.
 * </p>
 */
@EnableTransactionManagement
@EnableYoudowellCommonData
@EnableI18n
@EnableSlugServices
@EnableEclipselinkJpa
@EnableAuthService
@SpringBootApplication(exclude = {
    // Disable repositories auto-configuration
    ElasticsearchRepositoriesAutoConfiguration.class,
    RedisRepositoriesAutoConfiguration.class,
    JpaRepositoriesAutoConfiguration.class,
    // Disable Elasticsearch auto-configuration in favor of JEST
    ElasticsearchAutoConfiguration.class,
    ElasticsearchDataAutoConfiguration.class,
    // Disable AWS auto-configuration in favor of "aws" profile
    ElastiCacheAutoConfiguration.class,
    ContextRegionProviderAutoConfiguration.class,
    ContextResourceLoaderAutoConfiguration.class,
    ContextResourceLoaderConfiguration.class,
    ContextInstanceDataAutoConfiguration.class,
    ContextCredentialsAutoConfiguration.class,
    ElasticsearchJestAWSAutoConfiguration.class })
public class SurveyServer {

  public static void main(final String[] args) {
    SpringApplication.run(SurveyServer.class, args);
  }

}
