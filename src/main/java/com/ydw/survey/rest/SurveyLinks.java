package com.ydw.survey.rest;

import static com.ydw.common.data.rest.LinkUtils.addQueryParams;
import static com.ydw.common.data.rest.LinkUtils.buildQueryParams;

import com.ydw.survey.model.Survey;
import com.ydw.survey.search.model.SurveySearchOptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.TemplateVariables;
import org.springframework.hateoas.UriTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;

@Component
public class SurveyLinks {

  public static final String SURVEYS = "surveys";

  public static final String SEARCH = "search";

  public static final String FIND_MY = "findMy";

  public static final String FIND_ALL = "findAll";

  public static final String COLLECTION_REL = "surveys";

  public static final String ITEM_REL = "survey";

  @Autowired
  private RepositoryEntityLinks entityLinks;

  @Autowired
  private HateoasPageableHandlerMethodArgumentResolver pagingResolver;

  /**
   * Builds link to surveys search.
   */
  public Link linkToSearchAll(final SurveySearchOptions options, final Pageable pageable,
      final Sort sort) {
    final Link link = entityLinks.linkToPagedResource(Survey.class, pageable);
    return addQueryParams(link, buildQueryParams(SurveySearchOptions.class, options));
  }

  /**
   * Builds link to survey result search.
   */
  public Link linkToSearchMySurveys(final Pageable pageable) {
    final String uri =
        entityLinks.linkFor(Survey.class).slash(SEARCH).slash(FIND_MY).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);
    if (null != pageable) {
      pagingResolver.enhance(uriBuilder, null, pageable);
    }

    final UriComponents uriComponents = uriBuilder.build();

    final TemplateVariables variables = new TemplateVariables(Collections.emptyList())
        .concat(pagingResolver.getPaginationTemplateVariables(null, uriComponents));

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables), Link.REL_SELF);
  }

}
