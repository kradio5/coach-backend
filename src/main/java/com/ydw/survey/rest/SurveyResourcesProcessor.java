package com.ydw.survey.rest;

import com.ydw.survey.model.Survey;
import com.ydw.survey.security.SurveyAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class SurveyResourcesProcessor implements ResourceProcessor<Resource<Survey>> {

  @Autowired
  private SurveyResultLinks surveyResultLinks;

  @Autowired
  private SurveyAuthService surveyAuthService;

  @Override
  public Resource<Survey> process(final Resource<Survey> resource) {
    if (surveyAuthService.hasAuthority("EXPERT")) {
      resource.add(surveyResultLinks.linkToDownload(resource.getContent())
          .withRel(SurveyResultLinks.DOWNLOAD));
    }
    return resource;
  }

}
