package com.ydw.survey.rest;

import com.ydw.common.security.utils.SecurityUtils;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.services.SurveyResultService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.support.DefaultedPageable;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Links;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Collections;

@BasePathAwareController
public class SurveyResultSearchController {

  private static final String BASE_MAPPING =
      "/" + SurveyResultLinks.PATH + "/" + SurveyResultLinks.SEARCH;

  private final RepositoryEntityLinks entityLinks;

  private final SurveyResultLinks surveyResultLinks;

  private final SurveyResultService surveyResultService;

  private final SurveyResultDao surveyResultDao;

  private final PagedResourcesAssembler<Object> pagedResourcesAssembler;

  /**
   * Instantiates controller.
   */
  @Autowired
  public SurveyResultSearchController(final SurveyResultService surveyResultService,
      final SurveyResultDao surveyResultDao, final RepositoryEntityLinks entityLinks,
      final SurveyResultLinks surveyResultLinks,
      final PagedResourcesAssembler<Object> pagedResourcesAssembler) {
    this.surveyResultService = surveyResultService;
    this.surveyResultDao = surveyResultDao;
    this.entityLinks = entityLinks;
    this.surveyResultLinks = surveyResultLinks;
    this.pagedResourcesAssembler = pagedResourcesAssembler;
  }

  /**
   * <code>HEAD /{repository}/search</code> - Checks whether the search resource is present.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.HEAD)
  public HttpEntity<?> headForSearches() {
    return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
  }

  /**
   * <code>GET /{repository}/search</code> - Exposes links to the individual search resources
   * exposed by the backing repository.
   */
  @ResponseBody
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.GET)
  public ResourceSupport listSearches() {
    final Links queryMethodLinks = entityLinks.linksToSearchResources(SurveyResult.class);

    final ResourceSupport result = new ResourceSupport();

    // Find by survey campaing
    result.add(surveyResultLinks.linkToSearchBySurveyCampaignSlug(null)
        .withRel(SurveyResultLinks.FIND_BY_SURVEYCAMPAIGNSLUG));

    // Find by survey
    result.add(surveyResultLinks.linkToSearchBySurveySlug(null)
        .withRel(SurveyResultLinks.FIND_BY_SURVEYSLUG_LAST));

    // Find my surveys
    result.add(
        surveyResultLinks.linkToSearchMySurveyResults(null).withRel(SurveyResultLinks.FIND_MY));

    if (!queryMethodLinks.isEmpty()) {
      result.add(queryMethodLinks);
    }
    result.add(getDefaultSelfLink());
    return result;
  }

  /**
   * <code>OPTIONS /{repository}/search</code>.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.OPTIONS)
  public HttpEntity<?> optionsForSearches() {
    final HttpHeaders headers = new HttpHeaders();
    headers.setAllow(Collections.singleton(HttpMethod.GET));
    return new ResponseEntity<Object>(headers, HttpStatus.OK);
  }

  protected Link getDefaultSelfLink() {
    return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString());
  }

  @RequestMapping(value = BASE_MAPPING + "/" + SurveyResultLinks.FIND_BY_SURVEYCAMPAIGNSLUG,
      method = RequestMethod.GET)
  ResponseEntity<ResourceSupport> findBySurveyCampaignSlug(
      @RequestParam final String surveyCampaignSlug,
      final PersistentEntityResourceAssembler assembler) {

    final String accountId = SecurityUtils.getMyPrincipal();

    final PersistentEntityResource surveyResult = surveyResultService
        .findBySurveyCampaignSlugAndCreatedBy(surveyCampaignSlug, accountId, assembler);

    final HttpHeaders httpHeaders = new HttpHeaders();
    return new ResponseEntity<ResourceSupport>(surveyResult, httpHeaders, HttpStatus.OK);
  }

  @RequestMapping(value = BASE_MAPPING + "/" + SurveyResultLinks.FIND_BY_SURVEYSLUG_LAST,
      method = RequestMethod.GET)
  ResponseEntity<ResourceSupport> findBySurveySlugLast(
      @RequestParam final Long surveySlug,
      final PersistentEntityResourceAssembler assembler) {

    final String accountId = SecurityUtils.getMyPrincipal();

    final PersistentEntityResource surveyResult = surveyResultService
        .findBySurveySlugAndCreatedByLast(surveySlug, accountId, assembler);

    final HttpHeaders httpHeaders = new HttpHeaders();
    return new ResponseEntity<ResourceSupport>(surveyResult, httpHeaders, HttpStatus.OK);
  }

  @RequestMapping(value = BASE_MAPPING + "/" + SurveyResultLinks.FIND_MY,
      method = RequestMethod.GET)
  @ResponseBody
  @SuppressWarnings("unchecked")
  Resources<?> findMy(final DefaultedPageable defaultedPageable,
      final PersistentEntityResourceAssembler assembler) {

    final String accountId = SecurityUtils.getMyPrincipal();
    Resources<? extends Resource<Object>> resources = null;
    if (!StringUtils.isEmpty(accountId)) {
      final Pageable pageable = defaultedPageable.getPageable();
      final Page<?> surveyResultsPage =
          surveyResultDao.findByCreatedByOrderByCreated(accountId, pageable);
      final Link selfLink = surveyResultLinks.linkToSearchMySurveyResults(pageable);
      resources = pagedResourcesAssembler.toResource((Page<Object>) surveyResultsPage,
          assembler, selfLink);
    }
    return resources;
  }

}
