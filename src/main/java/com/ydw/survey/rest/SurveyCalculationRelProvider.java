package com.ydw.survey.rest;

import com.ydw.survey.model.SurveyCalculation;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SurveyCalculationRelProvider implements RelProvider {

  @Override
  public String getCollectionResourceRelFor(final Class<?> type) {
    return "calculations";
  }

  @Override
  public String getItemResourceRelFor(final Class<?> type) {
    return "calculation";
  }

  @Override
  public boolean supports(final Class<?> delimiter) {
    return ClassUtils.isAssignable(delimiter, SurveyCalculation.class);
  }
}
