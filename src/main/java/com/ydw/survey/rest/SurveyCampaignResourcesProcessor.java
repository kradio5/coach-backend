package com.ydw.survey.rest;

import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.security.SurveyAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class SurveyCampaignResourcesProcessor
    implements ResourceProcessor<Resource<SurveyCampaign>> {

  @Autowired
  private SurveyResultLinks surveyResultLinks;

  @Autowired
  private SurveyAuthService surveyAuthService;

  @Override
  public Resource<SurveyCampaign> process(final Resource<SurveyCampaign> resource) {
    if (surveyAuthService.hasAuthority("EXPERT")) {
      resource.add(surveyResultLinks.linkToDownload(resource.getContent())
          .withRel(SurveyResultLinks.DOWNLOAD));
    }
    return resource;
  }

}
