package com.ydw.survey.rest;

import com.ydw.survey.search.model.SurveySearchOptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
@Order(0)
public class SurveyAwareRootResourceProcessor
    implements ResourceProcessor<RepositoryLinksResource> {

  @Autowired
  private SurveyLinks surveyLinks;

  @Override
  public RepositoryLinksResource process(final RepositoryLinksResource resource) {
    // Replace plan events link with filtered plan events search
    final String surveys = SurveyLinks.SURVEYS;
    resource.getLinks().removeIf(link -> link.getRel().equals(surveys));
    resource.add(surveyLinks.linkToSearchAll((SurveySearchOptions) null, null, null)
        .withRel(surveys));
    return resource;
  }

}
