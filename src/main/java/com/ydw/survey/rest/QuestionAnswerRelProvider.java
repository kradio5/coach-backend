package com.ydw.survey.rest;

import com.ydw.survey.model.QuestionAnswer;

import org.apache.commons.lang3.ClassUtils;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.hateoas.RelProvider;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class QuestionAnswerRelProvider implements RelProvider {

  @Override
  public String getCollectionResourceRelFor(final Class<?> type) {
    return "questionAnswers";
  }

  @Override
  public String getItemResourceRelFor(final Class<?> type) {
    return "questionAnswer";
  }

  @Override
  public boolean supports(final Class<?> delimiter) {
    return ClassUtils.isAssignable(delimiter, QuestionAnswer.class);
  }
}
