package com.ydw.survey.rest;

import com.ydw.common.security.oauth2.sso.JwtStateTokenConverter;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.TemplateVariables;
import org.springframework.hateoas.UriTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
public class SurveyResultLinks {

  public static final String PATH = "surveyResults";

  public static final String SEARCH = "search";

  public static final String FIND_BY_SURVEYCAMPAIGNSLUG = "findBySurveyCampaignSlug";

  public static final String FIND_BY_SURVEYSLUG_LAST = "findBySurveySlugLast";

  public static final String DOWNLOAD = "download";

  public static final String COLLECTION_REL = "surveyResults";

  public static final String ITEM_REL = "surveyResult";

  public static final String FIND_MY = "findMy";

  private static final String PARAM_USER_ID = "userId";

  private static final String PARAM_SURVEYRESULT_SLUG = "surveyResultSlug";

  private static final String PARAM_SURVEY_SLUG = "surveySlug";

  private static final String PARAM_SURVEYCAMPAIGN_SLUG = "surveyCampaignSlug";

  private static final String PARAM_TOKEN = "token";

  private final RepositoryEntityLinks entityLinks;

  private final HateoasPageableHandlerMethodArgumentResolver pagingResolver;

  @Autowired
  public JwtStateTokenConverter stateTokenConverter;

  /**
   * Constructs bean with required dependencies.
   */
  @Autowired
  public SurveyResultLinks(final RepositoryEntityLinks entityLinks,
      final HateoasPageableHandlerMethodArgumentResolver pagingResolver) {
    super();
    this.entityLinks = entityLinks;
    this.pagingResolver = pagingResolver;
  }

  /**
   * Builds link to download survey results by survey.
   */
  public Link linkToDownload(final Survey survey) {
    final String uri =
        entityLinks.linkFor(SurveyResult.class).slash(SurveyResultLinks.DOWNLOAD).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);
    uriBuilder.replaceQueryParam(PARAM_SURVEY_SLUG, survey.getSlug());
    uriBuilder.replaceQueryParam(PARAM_TOKEN, generateToken(survey.getSlug()));

    final UriComponents uriComponents = uriBuilder.build();
    // Collect variables.
    final List<TemplateVariable> params = new ArrayList<>();
    params
        .add(new TemplateVariable(PARAM_USER_ID, TemplateVariable.VariableType.REQUEST_PARAM));

    final TemplateVariables variables = new TemplateVariables(params);

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables),
        SurveyResultLinks.DOWNLOAD);
  }

  /**
   * Builds link to download survey results by survey.
   */
  public Link linkToDownload(final SurveyCampaign surveyCampaign) {
    final String uri =
        entityLinks.linkFor(SurveyResult.class).slash(SurveyResultLinks.DOWNLOAD).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);
    uriBuilder.replaceQueryParam(PARAM_SURVEYCAMPAIGN_SLUG, surveyCampaign.getSlug());
    uriBuilder.replaceQueryParam(PARAM_TOKEN, generateToken(surveyCampaign.getSlug()));

    final UriComponents uriComponents = uriBuilder.build();
    // Collect variables.
    final List<TemplateVariable> params = new ArrayList<>();
    params
        .add(new TemplateVariable(PARAM_USER_ID, TemplateVariable.VariableType.REQUEST_PARAM));

    final TemplateVariables variables = new TemplateVariables(params);

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables),
        SurveyResultLinks.DOWNLOAD);
  }

  /**
   * Builds link to download survey result.
   */
  public Link linkToDownload(final SurveyResult surveyResult) {
    final String uri = entityLinks.linkFor(SurveyResult.class).slash(DOWNLOAD).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);
    uriBuilder.replaceQueryParam(PARAM_SURVEYRESULT_SLUG, surveyResult.getSlug());
    uriBuilder.replaceQueryParam(PARAM_TOKEN, generateToken(surveyResult.getSlug()));
    final UriComponents uriComponents = uriBuilder.build();
    // Create link
    return new Link(uriComponents.toString());
  }

  /**
   * Builds link to survey result search.
   */
  public Link linkToSearchBySurveyCampaignSlug(final Object parameters) {
    final String uri = entityLinks.linkFor(SurveyResult.class).slash(SEARCH)
        .slash(FIND_BY_SURVEYCAMPAIGNSLUG).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);

    final UriComponents uriComponents = uriBuilder.build();

    // Collect variables.
    final List<TemplateVariable> params = new ArrayList<>();
    params.add(new TemplateVariable("surveyCampaignSlug",
        TemplateVariable.VariableType.REQUEST_PARAM));

    final TemplateVariables variables = new TemplateVariables(params);

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables), Link.REL_SELF);
  }

  /**
   * Builds link to survey result search.
   */
  public Link linkToSearchBySurveySlug(final Object parameters) {
    final String uri = entityLinks.linkFor(SurveyResult.class).slash(SEARCH)
        .slash(FIND_BY_SURVEYSLUG_LAST).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);

    final UriComponents uriComponents = uriBuilder.build();

    // Collect variables.
    final List<TemplateVariable> params = new ArrayList<>();
    params.add(new TemplateVariable("surveySlug",
        TemplateVariable.VariableType.REQUEST_PARAM));

    final TemplateVariables variables = new TemplateVariables(params);

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables), Link.REL_SELF);
  }

  /**
   * Builds link to survey result search.
   */
  public Link linkToSearchMySurveyResults(final Pageable pageable) {
    final String uri =
        entityLinks.linkFor(SurveyResult.class).slash(SEARCH).slash(FIND_MY).toString();
    final UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(uri);
    if (null != pageable) {
      pagingResolver.enhance(uriBuilder, null, pageable);
    }

    final UriComponents uriComponents = uriBuilder.build();

    final TemplateVariables variables = new TemplateVariables(Collections.emptyList())
        .concat(pagingResolver.getPaginationTemplateVariables(null, uriComponents));

    // Create link
    return new Link(new UriTemplate(uriComponents.toString(), variables), Link.REL_SELF);
  }

  private String generateToken(final Object slug) {
    final List<Object> tokenObject = new ArrayList<>(2);
    tokenObject.add(slug);
    tokenObject.add(new Date().getTime());
    final String token = stateTokenConverter.encode(tokenObject);
    return token;
  }

}
