package com.ydw.survey.rest;

import static org.springframework.hateoas.TemplateVariable.VariableType.REQUEST_PARAM;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.data.rest.core.config.ProjectionDefinitionConfiguration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.hateoas.TemplateVariable;
import org.springframework.hateoas.TemplateVariables;
import org.springframework.web.util.UriComponentsBuilder;

import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Support class with useful methods for generating HATEOAS links.
 *
 * @author Andrew Usov
 */
public class HateoasLinksSupport {

  protected final RepositoryRestConfiguration rest;

  /**
   * Constructs support class with required dependencies.
   */
  public HateoasLinksSupport(final RepositoryRestConfiguration rest) {
    this.rest = rest;
  }

  /**
   * Adds all non-null properties of the specified object to the URI builder parameters.
   *
   * @return Collection of added property names
   */
  protected Set<String> addParameters(final UriComponentsBuilder builder,
      final Map<String, ?> value) {
    final Set<String> propertyNames = new HashSet<>();
    if (null == value) {
      return propertyNames;
    }
    for (final Entry<String, ?> prop : value.entrySet()) {
      final String propertyName = prop.getKey();
      final Object propertyValue = prop.getValue();
      if (null != propertyValue) {
        if (propertyValue instanceof Collection) {
          final Collection<?> collection = (Collection<?>) propertyValue;
          if (collection.isEmpty()) {
            continue;
          }
        } else if (propertyValue instanceof Map) {
          // Not supported.
          continue;
        }
        builder.replaceQueryParam(propertyName, propertyValue);
        propertyNames.add(propertyName);
      }
    }
    return propertyNames;
  }

  /**
   * Adds all non-null properties of the specified object to the URI builder parameters.
   *
   * @return Collection of added property names
   */
  protected Set<String> addParameters(final UriComponentsBuilder builder,
      final Object value) {
    final Set<String> propertyNames = new HashSet<>();
    if (null == value) {
      return propertyNames;
    }
    final BeanWrapper wrapper = new BeanWrapperImpl(value);
    final PropertyDescriptor[] props = wrapper.getPropertyDescriptors();
    for (final PropertyDescriptor prop : props) {
      if (null != prop.getWriteMethod() && null != prop.getReadMethod() && !prop.isHidden()) {
        final String propertyName = prop.getName();
        final Object propertyValue = wrapper.getPropertyValue(propertyName);
        if (null != propertyValue) {
          if (propertyValue instanceof Collection) {
            final Collection<?> collection = (Collection<?>) propertyValue;
            if (collection.isEmpty()) {
              continue;
            }
          } else if (propertyValue instanceof Map) {
            // Not supported.
            continue;
          }
          builder.replaceQueryParam(propertyName, propertyValue);
          propertyNames.add(propertyName);
        }
      }
    }
    return propertyNames;
  }

  /**
   * Returns the {@link TemplateVariables} for the projection parameter if projections are
   * vonfigured for the given type.
   *
   * @param type must not be {@literal null}.
   * @return will never be {@literal null}.
   */
  protected TemplateVariables getProjectionVariable(final Class<?> type) {

    final ProjectionDefinitionConfiguration projectionConfiguration =
        rest.getProjectionConfiguration();

    if (projectionConfiguration.hasProjectionFor(type)) {
      return new TemplateVariables(
          new TemplateVariable(projectionConfiguration.getParameterName(), REQUEST_PARAM));
    } else {
      return TemplateVariables.NONE;
    }
  }

}
