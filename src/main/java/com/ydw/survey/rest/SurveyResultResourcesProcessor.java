package com.ydw.survey.rest;

import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.security.SurveyAuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
public class SurveyResultResourcesProcessor
    implements ResourceProcessor<Resource<SurveyResult>> {

  @Autowired
  private SurveyResultLinks surveyResultLinks;

  @Autowired
  private SurveyAuthService surveyAuthService;

  @Override
  public Resource<SurveyResult> process(final Resource<SurveyResult> resource) {
    if (surveyAuthService.hasAuthority("EXPERT") && resource.getContent().isCompleted()) {
      resource.add(surveyResultLinks.linkToDownload(resource.getContent())
          .withRel(SurveyResultLinks.DOWNLOAD));
    }
    return resource;
  }

}
