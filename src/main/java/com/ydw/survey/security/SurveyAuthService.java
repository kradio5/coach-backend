package com.ydw.survey.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

/**
 * Security tools.<br />
 * TODO: consider moving of functionality to common AuthService
 */
@Service
public class SurveyAuthService {

  @Autowired(required = false)
  private RoleHierarchy roleHierarchy;

  /**
   * Check if user has authority.
   */
  public boolean hasAuthority(final String authority) {

    final Authentication authentication =
        SecurityContextHolder.getContext().getAuthentication();
    if (authentication == null) {
      return false;
    }
    Collection<? extends GrantedAuthority> userAuthorities = authentication.getAuthorities();
    if (roleHierarchy != null) {
      userAuthorities = roleHierarchy.getReachableGrantedAuthorities(userAuthorities);
    }
    final Set<String> roles = AuthorityUtils.authorityListToSet(userAuthorities);

    return roles.contains(authority);

  }

}
