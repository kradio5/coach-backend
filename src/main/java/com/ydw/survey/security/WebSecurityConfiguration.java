package com.ydw.survey.security;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.TRACE;

import com.ydw.common.security.cors.EnableCors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;

import javax.servlet.Filter;

/**
 * Web security configuration.
 */
@Configuration
@EnableCors
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  // private static final Logger LOG =
  // LoggerFactory.getLogger(SsoWebSecurityConfiguration.class);

  @Autowired
  @Qualifier("corsFilter")
  private Filter corsFilter;

  @Override
  protected void configure(final HttpSecurity http) throws Exception {

    // CORS
    http.addFilterBefore(corsFilter, ChannelProcessingFilter.class);

    // Path based restrictions
    http.authorizeRequests().antMatchers(GET, "/docs/**").permitAll()
        // .antMatchers(GET, MediaLinks.MEDIA_BASE_MAPPING + "/**").permitAll()
        .antMatchers(TRACE).denyAll().anyRequest().authenticated();
  }

}
