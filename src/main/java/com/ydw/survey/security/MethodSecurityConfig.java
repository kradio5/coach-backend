package com.ydw.survey.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.intercept.RunAsImplAuthenticationProvider;
import org.springframework.security.access.intercept.RunAsManager;
import org.springframework.security.access.intercept.RunAsManagerImpl;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.oauth2.provider.expression.OAuth2MethodSecurityExpressionHandler;

import java.util.UUID;

/**
 * Global method security configuration.
 * <p>
 * Note, the @{@link Secured} annotation is used for easier "run-as" authentication.
 * </p>
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

  @Autowired
  private ApplicationContext applicationContext;

  private final String key = UUID.randomUUID().toString();

  @Autowired(required = false)
  private PermissionEvaluator permissionEvaluator;

  @Autowired(required = false)
  private RoleHierarchy roleHierarchy;

  @Autowired(required = false)
  private AuthenticationTrustResolver trustResolver;

  /**
   * Configures a global authentication manager instance that is reused across multiple
   * configurations.
   */
  @Autowired
  public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(runAsAuthenticationProvider());
  }

  /**
   * Creates "run-as" authentication provider.
   */
  @Bean
  public RunAsImplAuthenticationProvider runAsAuthenticationProvider() {
    final RunAsImplAuthenticationProvider authenticationProvider =
        new RunAsImplAuthenticationProvider();
    authenticationProvider.setKey(key);
    return authenticationProvider;
  }

  @Override
  protected MethodSecurityExpressionHandler createExpressionHandler() {
    final OAuth2MethodSecurityExpressionHandler handler =
        new OAuth2MethodSecurityExpressionHandler();
    handler.setApplicationContext(applicationContext);
    if (trustResolver != null) {
      handler.setTrustResolver(trustResolver);
    }
    if (permissionEvaluator != null) {
      handler.setPermissionEvaluator(permissionEvaluator);
    }
    if (roleHierarchy != null) {
      handler.setRoleHierarchy(roleHierarchy);
    }
    return handler;
  }

  @Bean
  @Override
  protected RunAsManager runAsManager() {
    final RunAsManagerImpl runAsManager = new RunAsManagerImpl();
    runAsManager.setKey(key);
    return runAsManager;
  }

}
