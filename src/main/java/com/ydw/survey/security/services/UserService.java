package com.ydw.survey.security.services;

import com.ydw.cloud.auth.model.AuthUser;
import com.ydw.cloud.auth.services.AuthService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class UserService {

  @Autowired
  private AuthService authService;

  /**
   * Finds user by accountId.
   */
  public AuthUser findByAccountId(final String accountId) {
    if (!StringUtils.hasText(accountId)) {
      return null;
    }
    return authService.users().findByAccountId(accountId).getContent();
  }
}
