package com.ydw.survey.security;

import com.ydw.common.security.oauth2.sso.JwtStateTokenConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

/**
 * Configures SSO security.
 *
 * @author Andrew Usov
 */
@Configuration
public class SsoConfiguration {

  @Autowired
  private ResourceServerProperties resource;

  @Value("${security.role-hierarchy}")
  private String roleHierarchySpecs;

  @Bean
  @ConfigurationProperties("security.oauth2.client")
  public ClientCredentialsResourceDetails oauth2SsoClient() {
    final ClientCredentialsResourceDetails details = new ClientCredentialsResourceDetails();
    return details;
  }

  /**
   * Creates role hierarchy.
   */
  @Bean
  public RoleHierarchy roleHierarchy() {
    final RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();
    roleHierarchy.setHierarchy(roleHierarchySpecs);
    return roleHierarchy;
  }

  /**
   * Create a stateless state converter that stores state in a JWT token.
   */
  @Bean
  public JwtStateTokenConverter stateTokenConverter() {
    final JwtStateTokenConverter jwtOAuth2StateConverter =
        new JwtStateTokenConverter();
    jwtOAuth2StateConverter.setSigningKey(resource.getJwt().getKeyValue());
    return jwtOAuth2StateConverter;
  }

}
