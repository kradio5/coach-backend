package com.ydw.survey.security;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.HEAD;
import static org.springframework.http.HttpMethod.OPTIONS;
import static org.springframework.http.HttpMethod.TRACE;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

import com.ydw.common.security.cors.EnableCors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy;
import org.springframework.security.web.savedrequest.NullRequestCache;

import javax.servlet.Filter;

/**
 * Configuration for providing REST services with OAuth2 authentication.
 *
 * @author Andrew Usov
 */
@Configuration
@EnableCors
@EnableResourceServer
public class RestSecurityConfiguration extends ResourceServerConfigurerAdapter {

  public static final String SURVEY_RESOURCE_ID = "survey";

  @Autowired
  @Qualifier("corsFilter")
  private Filter corsFilter;

  @Autowired
  private RepositoryRestConfiguration rest;

  @Override
  public void configure(final HttpSecurity http) throws Exception {
    final String apiPath = rest.getBasePath().toString();
    http.antMatcher(apiPath + "/**").authorizeRequests()

        // HAL browser & documentation
        .antMatchers(GET, apiPath + "/browser/**").permitAll()
        .antMatchers(GET, apiPath, apiPath + "/").permitAll()

        // API requests
        .antMatchers(GET, apiPath + "/**").permitAll().antMatchers(HEAD, apiPath + "/**")
        .permitAll()
        // .antMatchers(GET, apiPath + "/**").authenticated().antMatchers(HEAD, apiPath + "/**")
        // .authenticated()

        // "Preflight" CORS requests
        .antMatchers(OPTIONS, apiPath + "/**").permitAll()

        // Hardening
        .antMatchers(TRACE).denyAll().anyRequest().authenticated();

    // CORS
    http.addFilterBefore(corsFilter, ChannelProcessingFilter.class);

    // Sessions
    http.httpBasic().disable().csrf().disable().sessionManagement()
        .sessionCreationPolicy(STATELESS)
        .sessionAuthenticationStrategy(new NullAuthenticatedSessionStrategy())
        .enableSessionUrlRewriting(false).sessionFixation().none().and().requestCache()
        .requestCache(new NullRequestCache());
  }

  @Override
  public void configure(final ResourceServerSecurityConfigurer resources) throws Exception {
    resources.resourceId(SURVEY_RESOURCE_ID);
  }

}
