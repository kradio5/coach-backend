package com.ydw.survey.export.controllers;

import static org.springframework.util.StringUtils.isEmpty;

import com.ydw.common.security.oauth2.sso.JwtStateTokenConverter;
import com.ydw.survey.export.services.SurveyResultCsvWriter;
import com.ydw.survey.rest.SurveyResultLinks;
import com.ydw.survey.services.InvalidParametersException;
import com.ydw.survey.services.SurveyErrorCodes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

@BasePathAwareController
public class SurveyResultExportController {

  private static final String BASE_MAPPING =
      "/" + SurveyResultLinks.PATH + "/" + SurveyResultLinks.DOWNLOAD;

  private static int TOKEN_LIFE_TIME_MINUTES = 30;

  @Autowired
  private SurveyResultCsvWriter surveyResultCsvWriter;

  @Autowired
  private SecurityProperties security;

  @Autowired
  public JwtStateTokenConverter stateTokenConverter;

  /**
   * Download CSV export file for given parameters.<br />
   * Either surveyCampaignSlug or surveySlug or surveyResultSlug to be filled. <br />
   * In case if userId is empty then answers of all users are exported.
   */
  @RequestMapping(value = BASE_MAPPING, method = RequestMethod.GET)
  public void downloadCsv(final HttpServletResponse response,
      @RequestParam(defaultValue = "") final String surveyCampaignSlug,
      @RequestParam(defaultValue = "0") final Long surveySlug,
      @RequestParam(defaultValue = "0") final Long surveyResultSlug,
      @RequestParam(defaultValue = "") final String accountId, @RequestParam final String token)
      throws IOException {

    validateRequestParams(surveyCampaignSlug, surveySlug, surveyResultSlug, token);

    final SecurityContext origCtx = preAuthorizeExpert();

    final String csvFileName =
        composeCsvFileName(surveyCampaignSlug, surveySlug, surveyResultSlug);

    response.setContentType("text/csv");
    response.setCharacterEncoding("UTF-8");

    // creates mock data
    response.setHeader("Content-Type", "application/octet-stream; charset=UTF-8");
    response.setHeader("Content-Disposition",
        String.format("attachment; filename=\"%s\"", csvFileName));
    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Expires", "0");

    // uses the Super CSV API to generate CSV data from the model data
    final OutputStream outputStream = response.getOutputStream();
    surveyResultCsvWriter.writeWithCsvMapWriter(
        new OutputStreamWriter(outputStream, StandardCharsets.UTF_8),
        surveyCampaignSlug, surveySlug, surveyResultSlug, accountId);

    outputStream.flush();
    outputStream.close();

    SecurityContextHolder.setContext(origCtx);
  }

  private String composeCsvFileName(final String surveyCampaignSlug, final Long surveySlug,
      final Long surveyResultSlug) {
    final StringBuilder csvFileNameBuilder = new StringBuilder("answers_");
    if (!isEmpty(surveyCampaignSlug)) {
      csvFileNameBuilder.append("campaign_");
      csvFileNameBuilder.append(surveyCampaignSlug);
    } else if (!surveySlug.equals(0L)) {
      csvFileNameBuilder.append("survey_");
      csvFileNameBuilder.append(surveySlug);
    } else if (!surveyResultSlug.equals(0L)) {
      csvFileNameBuilder.append("surveyresult_");
      csvFileNameBuilder.append(surveyResultSlug);
    }
    csvFileNameBuilder.append(".csv");
    return csvFileNameBuilder.toString();
  }

  private SecurityContext preAuthorizeExpert() {
    // authority of user is checked in validateRequestParams, force performing next actions with
    // expert since some used dao methods are secured
    // (downloading is performed via window.location therefore auth header is not available)
    final User user = new User(security.getUser().getName(), "N/A",
        AuthorityUtils.commaSeparatedStringToAuthorityList("EXPERT"));

    final PreAuthenticatedAuthenticationToken systemAuthentication =
        new PreAuthenticatedAuthenticationToken(user, user.getPassword(),
            user.getAuthorities());

    final SecurityContext origCtx = SecurityContextHolder.getContext();
    SecurityContextHolder.setContext(SecurityContextHolder.createEmptyContext());
    SecurityContextHolder.getContext().setAuthentication(systemAuthentication);
    return origCtx;
  }

  private void validateRequestParams(final String surveyCampaignSlug, final Long surveySlug,
      final Long surveyResultSlug, final String token) {

    @SuppressWarnings("unchecked")
    final List<Object> tokenClaims = (List<Object>) stateTokenConverter.decode(token);
    final Object slug = tokenClaims.get(0);
    final Date date = new Date((Long) tokenClaims.get(1));
    boolean validate = false;
    if (!isEmpty(surveyCampaignSlug)) {
      validate = surveyCampaignSlug.equals(slug);
    } else if (!surveySlug.equals(0L)) {
      validate = surveySlug.equals(Long.valueOf(slug.toString()));
    } else if (!surveyResultSlug.equals(0L)) {
      validate = surveyResultSlug.equals(Long.valueOf(slug.toString()));
    }
    if (validate) {
      final Date currentDate = new Date();
      final GregorianCalendar calendar = new GregorianCalendar();
      calendar.setTime(date);
      calendar.add(Calendar.MINUTE, TOKEN_LIFE_TIME_MINUTES);
      validate = !currentDate.after(calendar.getTime());
    }
    if (!validate) {
      throw new InvalidParametersException(SurveyErrorCodes.INVALID_DATA);
    }
  }

}
