package com.ydw.survey.export.services;

import static org.springframework.util.StringUtils.isEmpty;

import com.ydw.cloud.auth.model.AuthUser;
import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultCalculation;
import com.ydw.survey.model.SurveyResultSection;
import com.ydw.survey.model.SurveySection;
import com.ydw.survey.security.services.UserService;
import com.ydw.survey.services.InvalidParametersException;
import com.ydw.survey.services.SurveyErrorCodes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.cellprocessor.time.FmtZonedDateTime;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.IOException;
import java.io.Writer;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Service
public class SurveyResultCsvWriter {

  static class CsvExportData {

    private String[] header;

    private CellProcessor[] processors;

    private final List<SurveyResult> surveyResults;

    private final List<Survey> surveys;

    CsvExportData(final List<Survey> surveys, final List<SurveyResult> surveyResults) {
      this.surveyResults = surveyResults;
      this.surveys = surveys;
    }

    public List<Survey> getSurveys() {
      return surveys;
    }

    private String checkVariableName(String variableName, final int surveyIndex,
        final int index, final Set<String> columnNames, final boolean isCampaign,
        final String prefix) {
      if (StringUtils.isEmpty(variableName)) {
        variableName = (isCampaign ? SURVEY_PREFIX + surveyIndex + "_" : "") + prefix + index;
      } else if (columnNames.contains(variableName)) { // conflict - non-unique variable
        if (isCampaign) {
          variableName = SURVEY_PREFIX + surveyIndex + "_" + variableName; // use with survey index
          if (columnNames.contains(variableName)) { // if non-unique variable in one survey
            variableName = SURVEY_PREFIX + surveyIndex + "_" + prefix + index;
          }
        } else {
          variableName = prefix + index;
        }
      }
      return variableName;
    }

    int getColumnAmount() {
      return header != null ? header.length : 0;
    }

    String[] getHeader() {
      return header;
    }

    CellProcessor[] getProcessors() {
      return processors;
    }

    List<SurveyResult> getSurveyResults() {
      return surveyResults;
    }

    boolean isEmpty() {
      return surveyResults == null || surveyResults.isEmpty();
    }

    void prepareColumnProcessors() {
      final int columnAmount = getColumnAmount();
      final List<CellProcessor> processors = new ArrayList<CellProcessor>(columnAmount);
      processors.add(new Optional()); // user display name
      processors.add(new Optional()); // user name
      // started
      processors
          .add(new FmtZonedDateTime(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
      // finished
      processors
          .add(new FmtZonedDateTime(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));

      for (int index = 2; index < columnAmount; index++) {
        // TODO: consider using different processors depending on questionType
        processors.add(new Optional());
      }
      this.processors = processors.stream().toArray(CellProcessor[]::new);
    }

    void prepareHeader() {
      final List<String> header = new ArrayList<>();
      header.add("user");
      header.add("username");
      header.add("started");
      header.add("finished");
      // final List<QuestionType> questionTypes = new ArrayList<>();
      int surveyIndex = 1;
      final Set<String> columnNames = new HashSet<>();
      final boolean isCampaign = this.surveys.size() > 1;
      for (final Survey survey : this.surveys) {
        for (final SurveySection section : survey.getSections()) {
          int questionIndex = 1;
          for (final Question question : section.getQuestions()) {
            if (question.isDeleted()) {
              continue;
            }
            final String variableName = checkVariableName(question.getVariableName(),
                surveyIndex, questionIndex, columnNames, isCampaign, QUESTION_PREFIX);
            header.add(variableName);
            // questionTypes.add(question.getQuestionType());
            questionIndex++;
          }
        }
        final List<SurveyCalculation> surveyCalculations = survey.getCalculations();
        if (!surveyCalculations.isEmpty()) {
          int calculationIndex = 1;
          for (final SurveyCalculation calculation : survey.getCalculations()) {
            if (calculation.isDeleted()) {
              continue;
            }
            final String variableName =
                checkVariableName(calculation.getVariableName(), surveyIndex, calculationIndex,
                    columnNames, isCampaign, SURVEY_CALCULATION_PREFIX);
            header.add(variableName);
            calculationIndex++;
          }
        }
        surveyIndex++;
      }
      this.header = header.stream().toArray(String[]::new);
    }

    // void removeSurveyResultHistory() {
    // final Set<Pair<Long, Long>> userSurveyPairs = new HashSet<>();
    // for (final Iterator<SurveyResult> surveyResultsIterator =
    // surveyResults.iterator(); surveyResultsIterator.hasNext();) {
    // final SurveyResult surveyResult = surveyResultsIterator.next();
    // final Pair<Long, Long> userSurveyPair =
    // Pair.of(surveyResult.getSurvey().getSlug(), Long.valueOf(surveyResult.getUserId()));
    // if (userSurveyPairs.contains(userSurveyPair)) {
    // surveyResultsIterator.remove();
    // } else {
    // userSurveyPairs.add(userSurveyPair);
    // }
    // }
    // }

  }

  static class UserInfo {

    final String displayName;

    final String userName;

    UserInfo(final String displayName, final String userName) {
      this.displayName = !StringUtils.isEmpty(displayName) ? displayName : "";
      this.userName = !StringUtils.isEmpty(userName) ? userName : "";
    }

    public String getDisplayName() {
      return displayName;
    }

    public String getUserName() {
      return userName;
    }

  }

  private static final Logger LOG = LoggerFactory.getLogger(SurveyResultCsvWriter.class);

  private static final String QUESTION_PREFIX = "Q";

  private static final String SURVEY_CALCULATION_PREFIX = "R";

  private static final String SURVEY_PREFIX = "S";

  @Autowired
  private SurveyCampaignDao surveyCampaignDao;

  @Autowired
  private SurveyDao surveyDao;

  @Autowired
  private SurveyResultDao surveyResultDao;

  @Autowired
  private UserService userService;

  /**
   * Prepare CSV export file for given parameters.<br />
   * Either surveyCampaignSlug or surveySlug or surveyResultSlug to be filled. <br />
   * In case if userId is empty then answers of all users are exported.
   */
  @Transactional
  public void writeWithCsvMapWriter(final Writer writer, final String surveyCampaignSlug,
      final Long surveySlug, final Long surveyResultSlug, final String accountId)
      throws IOException {

    final CsvExportData exportData =
        prepareExportData(surveyCampaignSlug, surveySlug, surveyResultSlug, accountId);

    final ICsvListWriter listWriter =
        new CsvListWriter(writer, CsvPreference.EXCEL_PREFERENCE);
    try {
      // write the header
      listWriter.writeHeader(exportData.getHeader());

      if (!exportData.isEmpty()) {
        // write question answers
        final Map<String, Map<Long, SurveyResult>> surveyResultBySurveyIdRows = new HashMap<>();
        final List<String> answeredAccountIds = new ArrayList<>();
        Map<Long, SurveyResult> currentSurveyResultBySurveyIdRow = null;
        String currentRowChangeTrigger = null;
        for (final SurveyResult surveyResult : exportData.getSurveyResults()) {
          // campaign: change row for each user (user cannot have more than 1 answer for campaign)
          // survey: change row for each survey result
          final String rowChangeTrigger = !isEmpty(surveyCampaignSlug)
              ? surveyResult.getCreatedBy() : surveyResult.getId().toString();
          if (null == currentRowChangeTrigger || null != rowChangeTrigger
              && !currentRowChangeTrigger.equals(rowChangeTrigger)) {
            currentSurveyResultBySurveyIdRow =
                surveyResultBySurveyIdRows.get(surveyResult.getCreatedBy());
            if (currentSurveyResultBySurveyIdRow == null) {
              currentSurveyResultBySurveyIdRow = new HashMap<>();
              surveyResultBySurveyIdRows.put(surveyResult.getCreatedBy(),
                  currentSurveyResultBySurveyIdRow);
              answeredAccountIds.add(surveyResult.getCreatedBy());
            }
            currentRowChangeTrigger = rowChangeTrigger;
          }
          currentSurveyResultBySurveyIdRow.put(surveyResult.getSurvey().getId(), surveyResult);
        }

        // export answers according to survey position in campaign
        final List<Object> answers = new ArrayList<>(exportData.getHeader().length);
        for (final String answeredAccountId : answeredAccountIds) {
          final Map<Long, SurveyResult> surveyResultBySurveyIdRow =
              surveyResultBySurveyIdRows.get(answeredAccountId);
          ZonedDateTime answerStartDate = null;
          ZonedDateTime answerFinishDate = null;
          for (final Survey survey : exportData.getSurveys()) {
            final SurveyResult surveyResult = surveyResultBySurveyIdRow.get(survey.getId());
            // write question answers
            writeSurveyResultAnswers(answers, surveyResult, survey);
            writeSurveyResultCalculations(answers, surveyResult, survey);
            if (answerStartDate == null
                || answerStartDate.isAfter(surveyResult.getCreated())) {
              answerStartDate = surveyResult.getCreated();
            }
            if (answerFinishDate == null
                || answerFinishDate.isBefore(surveyResult.getCreated())) {
              answerFinishDate = surveyResult.getCreated();
            }
          }
          if (answerStartDate != null) { // otherwise means no answers by the user
            final UserInfo userInfo = getUserInfo(answeredAccountId);
            answers.add(0, userInfo.getDisplayName());
            answers.add(1, userInfo.getUserName());
            answers.add(2, answerStartDate);
            answers.add(3, answerFinishDate);
            writeCsvAnswersRow(exportData, listWriter, answers);
          }
          answers.clear();
        }
      }
    } finally {
      if (listWriter != null) {
        listWriter.close();
      }
    }
  }

  private UserInfo getUserInfo(final String accountId) {
    UserInfo userInfo = null;
    AuthUser user;
    try {
      user = userService.findByAccountId(accountId);
      if (user != null) {
        userInfo = new UserInfo(user.getDisplayName(), user.getUsername());
      }
    } catch (final Exception exc) {
      LOG.warn("user with accountId '" + accountId + "' is not found", exc);
    }
    if (userInfo == null) {
      userInfo = new UserInfo(accountId, accountId);
    }
    return userInfo;
  }

  private CsvExportData prepareExportData(final String surveyCampaignSlug,
      final Long surveySlug, final Long surveyResultSlug, final String accountId) {
    final List<Survey> surveys;
    final List<SurveyResult> surveyResults;
    if (!isEmpty(surveyCampaignSlug)) {
      final SurveyCampaign surveyCampaign =
          surveyCampaignDao.findByUuid(UUID.fromString(surveyCampaignSlug));
      surveys = surveyCampaign.getSurveys();
      if (isEmpty(accountId)) {
        surveyResults =
            surveyResultDao
                .findBySurveyCampaignAndCompletedTrueOrderByCreated(surveyCampaign);
      } else {
        surveyResults =
            surveyResultDao.findBySurveyCampaignAndCreatedByAndCompletedTrueOrderByCreated(
                surveyCampaign, accountId);
      }
    } else if (!surveySlug.equals(0L)) {
      final Survey survey = surveyDao.findById(surveySlug);
      surveys = Collections.singletonList(survey);
      if (isEmpty(accountId)) {
        surveyResults =
            surveyResultDao.findBySurveyAndCompletedTrueOrderByCreatedDesc(survey);
      } else {
        surveyResults = surveyResultDao
            .findBySurveyAndCreatedByAndCompletedTrueOrderByCreatedDesc(survey, accountId);
      }
    } else if (!surveyResultSlug.equals(0L)) {
      final SurveyResult surveyResult = surveyResultDao.findById(surveyResultSlug);
      if (!surveyResult.isCompleted()) {
        throw new InvalidParametersException(SurveyErrorCodes.SURVEY_RESULT_NOT_COMPLETED);
      }
      surveys = Collections.singletonList(surveyResult.getSurvey());
      surveyResults = Collections.singletonList(surveyResult);
    } else {
      throw new InvalidParametersException(SurveyErrorCodes.INVALID_DATA);
    }

    final CsvExportData csvExportData = new CsvExportData(surveys, surveyResults);
    csvExportData.prepareHeader();
    csvExportData.prepareColumnProcessors();

    // remove old survey results (keep just one per survey to avoid history)
    // if (surveyResultSlug.equals(0L)) {
    // csvExportData.removeSurveyResultHistory();
    // }

    return csvExportData;
  }

  private void writeCsvAnswersRow(final CsvExportData exportData,
      final ICsvListWriter listWriter, final List<Object> answers) throws IOException {
    // fill all left columns with empty values
    final int columnLength = exportData.getProcessors().length;
    for (int answerIndex = answers.size(); answerIndex < columnLength; answerIndex++) {
      answers.add(null);
    }
    // write CSV row
    listWriter.write(answers, exportData.getProcessors());
  }

  /**
   * Write answers of survey.
   *
   * @param surveyResult - can be null (if was answered in campaign but survey was removed from
   *        campaign afterwards)
   * @param survey - if null, taken from surveyResult
   */
  private void writeSurveyResultAnswers(final List<Object> answers,
      final SurveyResult surveyResult, Survey survey) {
    // write result calculations (note: due to changes could be less answers than
    // defined in survey --> leave gaps for missing answers)
    final Map<Long, QuestionAnswer> questionAnswersMap = new HashMap<>();
    if (surveyResult != null) {
      if (survey == null) {
        survey = surveyResult.getSurvey();
      }
      for (final SurveyResultSection resultSection : surveyResult.getSections()) {
        for (final QuestionAnswer questionAnswer : resultSection.getQuestionAnswers()) {
          questionAnswersMap.put(questionAnswer.getQuestion().getId(), questionAnswer);
        }
      }
    }
    final List<SurveySection> surveySections = survey.getSections();
    for (final SurveySection surveySection : surveySections) {
      for (final Question question : surveySection.getQuestions()) {
        if (question.isDeleted()) {
          continue;
        }
        final QuestionAnswer questionAnswer = questionAnswersMap.get(question.getId());
        answers.add(questionAnswer != null ? questionAnswer.getValue() : "");
      }
    }
  }

  /**
   * Write calculation results of survey.
   *
   * @param surveyResult - can be null (if was answered in campaign but survey was removed from
   *        campaign afterwards)
   * @param survey - if null, taken from surveyResult
   */
  private void writeSurveyResultCalculations(final List<Object> answers,
      final SurveyResult surveyResult, Survey survey) {
    // write result calculations (note: due to conditions could be less calculations than
    // defined in survey --> leave gaps for missing calculations)
    if (surveyResult != null && survey == null) {
      survey = surveyResult.getSurvey();
    }
    final List<SurveyCalculation> surveyCalculations =
        survey.getCalculations();
    if (!surveyCalculations.isEmpty()) {
      final Map<Long, SurveyResultCalculation> resultCalculationsMap = new HashMap<>();
      if (surveyResult != null) {
        for (final SurveyResultCalculation resultCalculation : surveyResult.getCalculations()) {
          resultCalculationsMap.put(resultCalculation.getSurveyCalculation().getId(),
              resultCalculation);
        }
      }
      for (final SurveyCalculation surveyCalculation : surveyCalculations) {
        if (surveyCalculation.isDeleted()) {
          continue;
        }
        final SurveyResultCalculation resultCalculation =
            resultCalculationsMap.get(surveyCalculation.getId());
        answers.add(resultCalculation != null ? resultCalculation.getValue() : "");
      }
    }
  }

}
