package com.ydw.survey.initializer;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.common.spring.Profiles;
import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionBoolean;
import com.ydw.survey.model.QuestionFloat;
import com.ydw.survey.model.QuestionInteger;
import com.ydw.survey.model.QuestionList;
import com.ydw.survey.model.QuestionListValue;
import com.ydw.survey.model.QuestionScale;
import com.ydw.survey.model.QuestionText;
import com.ydw.survey.model.QuestionTextArea;
import com.ydw.survey.model.QuestionTextFloat;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculation;
import com.ydw.survey.model.SurveyCalculationScale;
import com.ydw.survey.model.SurveyCalculationText;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveySection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;

@Service
@Profile(Profiles.DEMO)
public class SurveyDemoDataPopulator {

  private static final Logger LOG = LoggerFactory.getLogger(SurveyDemoDataPopulator.class);

  private final SurveyDao surveyDao;

  private final SurveyCampaignDao surveyCampaignDao;

  /**
   * Constructor.
   */
  @Autowired
  public SurveyDemoDataPopulator(final SurveyDao surveyDao,
      final SurveyCampaignDao surveyCampaignDao) {
    this.surveyDao = surveyDao;
    this.surveyCampaignDao = surveyCampaignDao;
  }

  /**
   * Creates some demo surveys objects to test.
   */
  @Transactional
  public void perform() {

    if (surveyDao.count() > 0) {
      return;
    }

    final Survey welcomeSurvey = createWelcomeSurvey();
    final Survey survey1 = createSurvey1ForPresentation();
    final Survey survey2 = createSurvey2ForPresentation();
    final Survey survey3 = createSurvey3ForPresentation();

    final SurveyCampaign surveyCampaignChangeable =
        prepareDemoCampaign("Test survey campaign (changeable)", true);
    surveyCampaignChangeable.addSurvey(welcomeSurvey);
    surveyCampaignChangeable.addSurvey(survey1);
    // surveyCampaignChangeable.addSurvey(survey2);
    surveyCampaignChangeable.addSurvey(survey3);
    surveyCampaignDao.save(surveyCampaignChangeable);

    final SurveyCampaign surveyCampaignUnchangeable =
        prepareDemoCampaign("Test survey campaign (unchangeable)", false);
    surveyCampaignUnchangeable.addSurvey(welcomeSurvey);
    surveyCampaignUnchangeable.addSurvey(survey1);
    surveyCampaignUnchangeable.addSurvey(survey2);
    surveyCampaignUnchangeable.addSurvey(survey3);
    surveyCampaignDao.save(surveyCampaignUnchangeable);

    for (int surveyIndex = 0; surveyIndex < 1; surveyIndex++) {
      createSurveyForTesting(surveyIndex);
    }

  }

  private Survey createSurvey1ForPresentation() {
    // create presentation Survey
    Survey survey = prepareDemoSurvey("Lebenssituation", null, null);

    final SurveySection surveySection = prepareDemoSurveySection(survey, "Page 1", null, null);

    int variableNameIndex = 1;

    prepareDemoIntegerQuestion(surveySection,
        "Wie viele Personen leben in Ihrem Haushalt (Sie eingeschlossen)?", "Zahl eingeben",
        "question" + variableNameIndex++);

    prepareDemoBooleanQuestion(surveySection,
        "Leben Sie mit einem/einer Partner/Partnerin zusammen?", null,
        "question" + variableNameIndex++);

    prepareDemoIntegerQuestion(surveySection, "Wie viele Kinder (unter 18 Jahren) haben Sie?",
        "Zahl eingeben", "question" + variableNameIndex++);

    prepareDemoListQuestion(surveySection, "Sind Sie erwerbstaetig?", null,
        "question" + variableNameIndex++, true, "Nein, zurzeit nicht erwerbstaetig", "0",
        "Ja, Vollzeit", "100", "Ja, Teilzeit und zwar zu", "50");

    // prepareSurveyCalculationText(survey, "Result 1",
    // "There are {{#question1 - #question3}} adults in your family", null, "result1");

    // prepareSurveyCalculationScale(survey, "Size of family", "{{#question1}}", null, "result2",
    // 1, "small", 10, "big");

    // save survey with sections and calculation settings
    survey = surveyDao.save(survey);

    return survey;
  }

  private Survey createSurvey2ForPresentation() {
    // create presentation Survey
    Survey survey = prepareDemoSurvey("Gesundheitsbezogene Lebensqualitaet", null, null);

    final SurveySection surveySection1 = prepareDemoSurveySection(survey, "Page 1",
        "Nun stellen wir Ihnen einige Fragen zu Ihrem Gesundheitszustand. Wir bitten Sie auch "
            + "hier, moeglichst spontan und offen Antwort zu geben.",
        null);

    final SurveySection surveySection2 = prepareDemoSurveySection(survey, "Page 2",
        "Sind Sie durch Ihren derzeitigen Gesundheitszustand bei diesen "
            + "Taetigkeiten eingeschraenkt? Wenn ja, wie stark?",
        null);

    final SurveySection surveySection3 = prepareDemoSurveySection(survey, "Page 3",
        "Hatten Sie in der vergangenen Woche aufgrund Ihrer koerperlichen Gesundheit "
            + "irgendwelche Schwierigkeiten bei der Arbeit oder anderen alltaeglichen Taetigkeiten "
            + "im Beruf bzw. zu Hause?",
        null);

    final SurveySection surveySection4 = prepareDemoSurveySection(survey, "Page 4",
        "Hatten Sie in der vergangenen Woche aufgrund seelischer Probleme irgendwelche "
            + "Schwierigkeiten bei der Arbeit oder anderen alltaeglichen Taetigkeiten im Beruf "
            + "bzw. zu Hause (z.B. weil Sie sich niedergeschlagen oder aengstlich fuehlten)?",
        null);

    final SurveySection surveySection5 = prepareDemoSurveySection(survey, "Page 5", null, null);

    final SurveySection surveySection6 = prepareDemoSurveySection(survey, "Page 6",
        "In diesen Fragen geht es darum, wie Sie sich fuehlen und wie "
            + "es Ihnen in der vergangenen Woche gegangen ist. Wie oft waren Sie in der "
            + "vergangenen Woche ...",
        null);

    final SurveySection surveySection7 = prepareDemoSurveySection(survey, "Page 7", null, null);

    prepareDemoListQuestion(surveySection1,
        "Wie wuerden Sie Ihren Gesundheitszustand im Allgemeinen beschreiben?", null, "sf12_1",
        false, "Schlecht", "1", "Weniger gut", "2", "Gut", "3", "Sehr gut", "4",
        "Ausgezeichnet", "5");

    prepareDemoListQuestion(surveySection2,
        "a. mittelschwere Taetigkeiten, z.B. einen Tisch verschieben, staubsaugen, kegeln, "
            + "Golf spielen",
        null, "sf12_2a", false, "Ja, stark eingeschraenkt", "1", "Ja, etwas eingeschraenkt",
        "2", "Nein, ueberhaupt nicht einge-schraenkt", "3");

    prepareDemoListQuestion(surveySection2, "b. mehrere Treppenabsaetze steigen", null,
        "sf12_2b", false, "Ja, stark eingeschraenkt", "1", "Ja, etwas eingeschraenkt", "2",
        "Nein, ueberhaupt nicht einge-schraenkt", "3");

    prepareDemoBooleanQuestion(surveySection3, "a. Ich habe weniger geschafft als ich wollte.",
        null, "sf12_3a");

    prepareDemoBooleanQuestion(surveySection3, "b. Ich konnte nur bestimmte Dinge tun.", null,
        "sf12_3b");

    prepareDemoBooleanQuestion(surveySection4, "a. Ich habe weniger geschafft als ich wollte.",
        null, "sf12_4a");

    prepareDemoBooleanQuestion(surveySection4,
        "b. Ich konnte nicht so sorgfaeltig wie ueblich arbeiten.", null, "sf12_4b");

    prepareDemoListQuestion(surveySection5,
        "Inwieweit haben Schmerzen Sie in der vergangenen Woche bei der Ausuebung Ihrer "
            + "Alltagstaetigkeiten zu Hause oder im Beruf behindert?",
        null, "sf12_5", false, "Ueberhaupt nicht", "5", "Ein bisschen", "4", "Maessig", "3",
        "Ziemlich", "2", "Sehr", "1");

    prepareDemoListQuestion(surveySection6, "a. ruhig und gelassen?", null, "sf12_6a", false,
        "Immer", "6", "Meistens", "5", "Ziemlich oft", "4", "Manchmal", "3", "Selten", "2",
        "Nie", "1");

    prepareDemoListQuestion(surveySection6, "b. voller Energie?", null, "sf12_6b", false,
        "Immer", "6", "Meistens", "5", "Ziemlich oft", "4", "Manchmal", "3", "Selten", "2",
        "Nie", "1");

    prepareDemoListQuestion(surveySection6, "c. entmutigt und traurig?", null, "sf12_6c", false,
        "Immer", "6", "Meistens", "5", "Ziemlich oft", "4", "Manchmal", "3", "Selten", "2",
        "Nie", "1");

    prepareDemoListQuestion(surveySection7,
        "Wie haeufig haben Ihre koerperliche Gesundheit oder seelischen Probleme in der "
            + "vergangenen Woche Ihre Kontakte zu anderen Menschen (Besuche bei Freunden, "
            + "Verwandten usw.) beeintraechtigt?",
        null, "sf12_7", false, "Immer", "1", "Meistens", "2", "Manchmal", "3", "Selten", "4",
        "Nie", "5");

    prepareSurveyCalculationScale(survey, "Gesamte gesundheitsbezogene Lebensqualitaet",
        "#sf12_1 + #sf12_2a + #sf12_2b + #sf12_3a + #sf12_3b + #sf12_5", null, null, 12,
        "schlecht", 47, "perfekt");
    prepareSurveyCalculationScale(survey, "Koerperliche gesundheitsbezogene Lebensqualitaet",
        "#sf12_4a + #sf12_4b + #sf12_6a + #sf12_6b + #sf12_6c + #sf12_7", null, null, 6,
        "schlecht", 20, "perfekt");
    prepareSurveyCalculationScale(survey, "Seelische gesundheitsbezogene Lebensqualitaet",
        "#sf12_1 + #sf12_2a + #sf12_2b + #sf12_3a + #sf12_3b + #sf12_4a + #sf12_4b + "
            + "#sf12_5 + #sf12_6a + #sf12_6b + #sf12_6c + #sf12_7",
        null, null, 6, "schlecht", 27, "perfekt");

    // save survey with sections and calculation settings
    survey = surveyDao.save(survey);

    return survey;
  }

  private Survey createSurvey3ForPresentation() {
    // create presentation Survey
    Survey survey = prepareDemoSurvey("Fitness und Gesundheit", null, null);
    final SurveySection surveySection1 = prepareDemoSurveySection(survey, "Page 1", null, null);

    prepareDemoBooleanQuestion(surveySection1,
        "Denken Sie, dass Sie sich fuer Ihre Gesundheit genug bewegen?", null, "Fitn1");

    prepareDemoScaleQuestion(surveySection1, "Wie schaetzen Sie Ihre Fitness ein?", null,
        "Fitn2", 1, 10, 9, "sehr schlecht", "ausgezeichnet");

    prepareDemoListQuestion(surveySection1,
        "Was denken Sie: Wie viel muesste man sich bewegen, damit man gesundheitlich davon "
            + "profitiert?",
        null, "Fitn3", false, "Jede Bewegung tut gut", "1", "10min taeglich", "2",
        "Eine halbe Std. taeglich", "3", "Mindestens zwei Mal pro Woche eine Stunde", "4",
        "5h pro Woche", "5");

    prepareDemoListQuestion(surveySection1,
        "Wie anstrengend muss Bewegung sein, damit Ihre Gesundheit davon profitiert?", null,
        "Fitn4", false, "Jede Bewegung tut gut", "1", "Nicht sitzen oder liegen reicht aus",
        "2", "Mindestens so, dass man dabei leicht ausser Atem kommt", "3",
        "Mindestens so, dass man dabei ins Schwitzen kommt", "4",
        "Mindestens so, dass man ausser Atem kommt und nicht mehr reden kann", "5");

    // prepareSurveyCalculationText(survey, "Result 1", "Vielen Dank fuer Ihre Antworten.", null,
    // null);
    // prepareSurveyCalculationText(survey, "Result 2",
    // "Gemaess Ihrer eigenen Einschaetzung bewegen sie sich genug fuer Ihre Gesundheit.",
    // "#Fitn1 == 1", null);
    // prepareSurveyCalculationText(survey, "Result 3",
    // "Gemaess Ihrer eigenen Einschaetzung bewegen sie sich noch nicht genug fuer Ihre "
    // + "Gesundheit.",
    // "#Fitn1 == 2", null);
    // prepareSurveyCalculationText(survey, "Result 4",
    // "Ihrer Fitness geben sie eine {{#Fitn2}}, auf einer Skala von 1 bis 10.", null, null);
    // prepareSurveyCalculationScale(survey, "Result 5", "{{#Fitn2}}", null, null, 1, "schlecht",
    // 10, "perfekt");
    // prepareSurveyCalculationText(survey, "Result 6",
    // "Sie liegen richtig damit, dass fuer Innaktive Personen jede Bewegung gesundheitsfoerdernd "
    // + "ist. 10 Minuten taeglich sind zwar sinnvoll - aus gesundheitlicher Sicht aber noch "
    // + "nicht ausreichend.",
    // "#Fitn3 == 1 or #Fitn3 == 2", null);
    // prepareSurveyCalculationText(survey, "Result 7",
    // "Sie wissen wie viel man sich fuer die Gesundheit bewegen sollte.",
    // "#Fitn3 == 3 or #Fitn3 == 4", null);
    // prepareSurveyCalculationText(survey, "Result 8",
    // "Sie wissen wie viel man sich fuer die Gesundheit bewegen sollte.", "#Fitn3 == 5",
    // null);

    // save survey with sections and calculation settings
    survey = surveyDao.save(survey);

    return survey;

  }

  private Survey createSurveyForTesting(final int surveyIndex) {
    // create demo Survey
    Survey survey = prepareDemoSurvey("test survey " + (surveyIndex + 1),
        "test survey " + (surveyIndex + 1) + " description",
        "test survey " + (surveyIndex + 1) + " note");

    int variableNameIndex = 1;

    for (int surveySectionIndex = 0; surveySectionIndex < 1; surveySectionIndex++) {

      // create demo Survey sections
      final SurveySection surveySection = prepareDemoSurveySection(survey,
          "test survey section " + (surveyIndex + 1) + "." + (surveySectionIndex + 1),
          "test survey section " + (surveyIndex + 1) + "." + (surveySectionIndex + 1)
              + " description",
          "test survey section " + (surveyIndex + 1) + "." + (surveySectionIndex + 1)
              + " note");

      // create demo Text Questions, connect to Survey Section
      int questionIndex = 0;
      for (; questionIndex < 1; questionIndex++) {
        prepareDemoTextQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

      // create demo List Questions, connect to Survey Section
      for (; questionIndex < 2; questionIndex++) {
        prepareDemoQuestionList(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++, "list value 1", "list value 2", "list value 3");
      }

      // create demo TextArea Questions, connect to Survey Section
      for (; questionIndex < 3; questionIndex++) {
        prepareDemoTextAreaQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

      // create demo Integer Questions, connect to Survey Section
      for (; questionIndex < 4; questionIndex++) {
        prepareDemoIntegerQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

      // create demo Float Questions, connect to Survey Section
      for (; questionIndex < 5; questionIndex++) {
        prepareDemoFloatQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

      // create demo Boolean Questions, connect to Survey Section
      for (; questionIndex < 6; questionIndex++) {
        prepareDemoBooleanQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

      // create demo Scale Questions, connect to Survey Section
      for (; questionIndex < 7; questionIndex++) {
        prepareDemoScaleQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++, 0, 100, 10, "min", "max");
      }

      // create demo TextFloat Questions, connect to Survey Section
      for (; questionIndex < 8; questionIndex++) {
        prepareDemoTextFloatQuestion(surveySection,
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1),
            "test question " + (surveyIndex + 1) + "." + (surveySectionIndex + 1) + "."
                + (questionIndex + 1) + " description",
            "question" + variableNameIndex++);
      }

    }

    int surveyCalculationIndex = 0;
    for (; surveyCalculationIndex < 2; surveyCalculationIndex++) {
      prepareSurveyCalculationText(survey, "survey calculation text " + surveyCalculationIndex,
          "test", null, "surveyCalculation" + (surveyCalculationIndex + 1));
    }

    // save survey with sections and calculation settings
    survey = surveyDao.save(survey);

    LOG.info("created survey: " + survey.getId() + " with " + survey.getSections().size()
        + " sections and " + survey.getCalculations().size() + " calculations");

    return survey;
  }

  private Survey createWelcomeSurvey() {

    // create welcome Survey
    Survey survey = prepareDemoSurvey("Welcome", null, null);
    prepareDemoSurveySection(survey, "Welcome", "Welcome Dear User", null);
    survey = surveyDao.save(survey);

    return survey;
  }

  private void fillQuestion(final Question question, final SurveySection section,
      final String title, final String description, final String variableName) {
    question.setSection(section);
    question.setTitle(new LocalizedString(title));
    question.setDescription(new LocalizedString(description));
    question.setVariableName(variableName);
  }

  private void fillSurveyCalculation(final SurveyCalculation surveyCalculation,
      final Survey survey, final String title, final String template, final String condition,
      final String variableName) {
    surveyCalculation.setSurvey(survey);
    surveyCalculation.setTitle(new LocalizedString(title));
    surveyCalculation.setTemplate(new LocalizedString(template));
    surveyCalculation.setCondition(condition);
    surveyCalculation.setVariableName(variableName);
    surveyCalculation.setVisible(true);
  }

  private QuestionBoolean prepareDemoBooleanQuestion(final SurveySection section,
      final String title, final String description, final String variableName) {
    final QuestionBoolean question = new QuestionBoolean();
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private SurveyCampaign prepareDemoCampaign(final String title,
      final boolean allowAswerChanges) {
    final SurveyCampaign surveyCampaignChangeable = new SurveyCampaign();
    surveyCampaignChangeable.setTitle(new LocalizedString(title));
    surveyCampaignChangeable.setAllowAnswerChanges(allowAswerChanges);
    surveyCampaignChangeable.setStartDate(ZonedDateTime.now());
    return surveyCampaignChangeable;
  }

  private QuestionFloat prepareDemoFloatQuestion(final SurveySection section,
      final String title, final String description, final String variableName) {
    final QuestionFloat question = new QuestionFloat();
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private QuestionInteger prepareDemoIntegerQuestion(final SurveySection section,
      final String title, final String description, final String variableName) {
    final QuestionInteger question = new QuestionInteger();
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private Question prepareDemoListQuestion(final SurveySection section, final String title,
      final String description, final String variableName, final boolean addValueEditable,
      final Object... values) {
    final QuestionList question = new QuestionList();
    fillQuestion(question, section, title, description, variableName);
    question.setAddValueEditable(addValueEditable);
    if (values != null && values.length > 0) {
      int itemIndex = 0;
      final int valuesLength = values.length - values.length % 2;
      for (int index = 0; index < valuesLength; index += 2) {
        question.addListValue(new QuestionListValue(values[index].toString(), itemIndex++,
            (String) values[index + 1]));
      }
    }
    section.addQuestion(question);
    return question;
  }

  private QuestionList prepareDemoQuestionList(final SurveySection section, final String title,
      final String description, final String variableName, final String... values) {
    final QuestionList question = new QuestionList();
    fillQuestion(question, section, title, description, variableName);
    if (values != null && values.length > 0) {
      int itemIndex = 0;
      for (final String value : values) {
        question.addListValue(new QuestionListValue(value, itemIndex++, null));
      }
    }
    section.addQuestion(question);
    return question;
  }

  private QuestionScale prepareDemoScaleQuestion(final SurveySection section,
      final String title, final String description, final String variableName,
      final int minValue, final int maxValue, final int stepAmount, final String minValueTitle,
      final String maxValueTitle) {
    final QuestionScale question = new QuestionScale();
    fillQuestion(question, section, title, description, variableName);
    question.setMinValue(minValue);
    question.setMaxValue(maxValue);
    question.setMinValueTitle(new LocalizedString(minValueTitle));
    question.setMaxValueTitle(new LocalizedString(maxValueTitle));
    question.setStepAmount(stepAmount);
    section.addQuestion(question);
    return question;
  }

  private Survey prepareDemoSurvey(final String title, final String description,
      final String note) {
    final Survey survey = new Survey();
    survey.setTitle(new LocalizedString(title));
    survey.setDescription(new LocalizedString(description));
    survey.setNote(new LocalizedString(note));
    return survey;
  }

  private SurveySection prepareDemoSurveySection(final Survey survey, final String title,
      final String description, final String note) {
    final SurveySection surveySection = new SurveySection();
    surveySection.setTitle(new LocalizedString(title));
    surveySection.setDescription(new LocalizedString(description));
    surveySection.setNote(new LocalizedString(note));
    survey.addSection(surveySection);
    return surveySection;
  }

  private QuestionTextArea prepareDemoTextAreaQuestion(final SurveySection section,
      final String title, final String description, final String variableName) {
    final QuestionTextArea question = new QuestionTextArea();
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private QuestionTextFloat prepareDemoTextFloatQuestion(final SurveySection section,
      final String title, final String description, final String variableName) {
    final QuestionTextFloat question = new QuestionTextFloat();
    question.setTextTitle(new LocalizedString("Text field"));
    question.setFloatTitle(new LocalizedString("Float field"));
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private QuestionText prepareDemoTextQuestion(final SurveySection section, final String title,
      final String description, final String variableName) {
    final QuestionText question = new QuestionText();
    fillQuestion(question, section, title, description, variableName);
    section.addQuestion(question);
    return question;
  }

  private SurveyCalculationScale prepareSurveyCalculationScale(final Survey survey,
      final String title, final String template, final String condition,
      final String variableName, final int minValue, final String minValueTitle,
      final int maxValue, final String maxValueTitle) {
    final SurveyCalculationScale surveyCalculation =
        new SurveyCalculationScale(minValue, minValueTitle, maxValue, maxValueTitle);
    fillSurveyCalculation(surveyCalculation, survey, title, template, condition, variableName);
    survey.addCalculation(surveyCalculation);
    return surveyCalculation;
  }

  private SurveyCalculationText prepareSurveyCalculationText(final Survey survey,
      final String title, final String template, final String condition,
      final String variableName) {
    final SurveyCalculationText surveyCalculation = new SurveyCalculationText();
    fillSurveyCalculation(surveyCalculation, survey, title, template, condition, variableName);
    survey.addCalculation(surveyCalculation);
    return surveyCalculation;
  }
}
