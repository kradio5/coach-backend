package com.ydw.survey.initializer;

import com.ydw.survey.dao.QuestionAnswerBooleanDao;
import com.ydw.survey.dao.QuestionAnswerFloatDao;
import com.ydw.survey.dao.QuestionAnswerIntegerDao;
import com.ydw.survey.dao.QuestionAnswerListDao;
import com.ydw.survey.dao.QuestionAnswerScaleDao;
import com.ydw.survey.dao.QuestionAnswerTextAreaDao;
import com.ydw.survey.dao.QuestionAnswerTextDao;
import com.ydw.survey.dao.QuestionAnswerTextFloatDao;
import com.ydw.survey.dao.SurveyResultCalculationDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.dao.SurveyResultSectionDao;
import com.ydw.survey.model.Question;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.QuestionAnswerBoolean;
import com.ydw.survey.model.QuestionAnswerFloat;
import com.ydw.survey.model.QuestionAnswerInteger;
import com.ydw.survey.model.QuestionAnswerList;
import com.ydw.survey.model.QuestionAnswerScale;
import com.ydw.survey.model.QuestionAnswerText;
import com.ydw.survey.model.QuestionAnswerTextArea;
import com.ydw.survey.model.QuestionAnswerTextFloat;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveyResultSection;
import com.ydw.survey.model.SurveySection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class SurveyResultDataPopulator {

  private final SurveyResultDao surveyResultDao;

  private final SurveyResultSectionDao surveyResultSectionDao;

  private final QuestionAnswerListDao questionAnswerListDao;

  private final QuestionAnswerTextDao questionAnswerTextDao;

  private final QuestionAnswerTextAreaDao questionAnswerTextAreaDao;

  private final QuestionAnswerIntegerDao questionAnswerIntegerDao;

  private final QuestionAnswerFloatDao questionAnswerFloatDao;

  private final QuestionAnswerBooleanDao questionAnswerBooleanDao;

  private final QuestionAnswerScaleDao questionAnswerScaleDao;

  private final QuestionAnswerTextFloatDao questionAnswerTextFloatDao;

  /**
   * Constructor.
   */
  @Autowired
  public SurveyResultDataPopulator(final SurveyResultDao surveyResultDao,
      final SurveyResultSectionDao surveyResultSectionDao,
      final QuestionAnswerListDao questionAnswerListDao,
      final QuestionAnswerTextDao questionAnswerTextDao,
      final QuestionAnswerTextAreaDao questionAnswerTextAreaDao,
      final QuestionAnswerIntegerDao questionAnswerIntegerDao,
      final QuestionAnswerFloatDao questionAnswerFloatDao,
      final QuestionAnswerBooleanDao questionAnswerBooleanDao,
      final QuestionAnswerScaleDao questionAnswerScaleDao,
      final QuestionAnswerTextFloatDao questionAnswerTextFloatDao,
      final SurveyResultCalculationDao surveyResultCalculationDao) {
    this.surveyResultDao = surveyResultDao;
    this.surveyResultSectionDao = surveyResultSectionDao;
    this.questionAnswerListDao = questionAnswerListDao;
    this.questionAnswerTextDao = questionAnswerTextDao;
    this.questionAnswerTextAreaDao = questionAnswerTextAreaDao;
    this.questionAnswerIntegerDao = questionAnswerIntegerDao;
    this.questionAnswerFloatDao = questionAnswerFloatDao;
    this.questionAnswerBooleanDao = questionAnswerBooleanDao;
    this.questionAnswerScaleDao = questionAnswerScaleDao;
    this.questionAnswerTextFloatDao = questionAnswerTextFloatDao;
  }

  /**
   * Fills survey result basing on survey.
   */
  @Transactional
  public SurveyResult fillSurveyResult(final SurveyResult surveyResult) {

    for (final SurveySection section : surveyResult.getSurvey().getSections()) {
      // create demo Survey Result sections
      SurveyResultSection surveyResultSection = new SurveyResultSection(surveyResult, section);
      surveyResultSection = surveyResultSectionDao.save(surveyResultSection);
      for (final Question question : section.getQuestions()) {
        if (question.isDeleted()) {
          continue;
        }
        QuestionAnswer questionAnswer = null;
        switch (question.getQuestionType()) {
          case LIST:
            questionAnswer = questionAnswerListDao
                .save(new QuestionAnswerList(question, surveyResultSection));
            break;
          case INTEGER:
            questionAnswer = questionAnswerIntegerDao
                .save(new QuestionAnswerInteger(question, surveyResultSection));
            break;
          case FLOAT:
            questionAnswer = questionAnswerFloatDao
                .save(new QuestionAnswerFloat(question, surveyResultSection));
            break;
          case TEXTAREA:
            questionAnswer = questionAnswerTextAreaDao
                .save(new QuestionAnswerTextArea(question, surveyResultSection));
            break;
          case BOOLEAN:
            questionAnswer = questionAnswerBooleanDao
                .save(new QuestionAnswerBoolean(question, surveyResultSection));
            break;
          case SCALE:
            questionAnswer = questionAnswerScaleDao
                .save(new QuestionAnswerScale(question, surveyResultSection));
            break;
          case TEXT_FLOAT:
            questionAnswer = questionAnswerTextFloatDao
                .save(new QuestionAnswerTextFloat(question, surveyResultSection));
            break;
          default:
            questionAnswer = questionAnswerTextDao
                .save(new QuestionAnswerText(question, surveyResultSection));
        }
        surveyResultSection.addQuestionAnswer(questionAnswer);
      }
      surveyResultSection = surveyResultSectionDao.save(surveyResultSection);

      surveyResult.addSection(surveyResultSection);
    }

    // save survey result with sections and calculations
    return surveyResultDao.save(surveyResult);
  }

}
