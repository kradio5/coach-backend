package com.ydw.survey.initializer;

import com.ydw.common.spring.Profiles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Profile(Profiles.DEMO)
public class DemoDataPopulator {

  private final SurveyDemoDataPopulator surveyDemoDataPopulator;

  /**
   * Initializes object with temporary security role in context.
   */
  @Autowired
  public DemoDataPopulator(final SurveyDemoDataPopulator surveyDemoDataPopulator) {
    final Authentication auth = new UsernamePasswordAuthenticationToken("user",
        "N/A", AuthorityUtils.createAuthorityList("EXPERT"));
    SecurityContextHolder.getContext().setAuthentication(auth);
    this.surveyDemoDataPopulator = surveyDemoDataPopulator;
  }

  /**
   * Invoke populator services for populating surveys.
   */
  @PostConstruct
  public void init() {
    surveyDemoDataPopulator.perform();
  }

}
