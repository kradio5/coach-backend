package com.ydw.survey.initializer;

import com.ydw.common.spring.Profiles;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Profile(Profiles.DEMO)
public class SurveyResultDemoDataPopulator {

  private static final String DUMMY_ACCOUNT_ID = "1";

  private static final Logger LOG =
      LoggerFactory.getLogger(SurveyResultDemoDataPopulator.class);

  private final SurveyDao surveyDao;

  private final SurveyResultDao surveyResultDao;

  private final SurveyResultDataPopulator surveyResultDataPopulator;

  /**
   * Constructor.
   */
  @Autowired
  public SurveyResultDemoDataPopulator(final SurveyDao surveyDao,
      final SurveyResultDao surveyResultDao,
      final SurveyResultDataPopulator surveyResultDataPopulator) {
    this.surveyDao = surveyDao;
    this.surveyResultDao = surveyResultDao;
    this.surveyResultDataPopulator = surveyResultDataPopulator;
  }

  /**
   * Creates some demo surveys objects to test.
   */
  @Transactional
  public void perform() {
    if (surveyResultDao.count() > 0) {
      return;
    }

    final Iterable<Survey> surveys = surveyDao.findAll();
    for (final Survey survey : surveys) {

      // create demo Survey Result
      SurveyResult surveyResult = createDemoSurveyResult(survey);
      surveyResult = surveyResultDataPopulator.fillSurveyResult(surveyResult);
      LOG.info("created survey result: " + surveyResult.getId());
    }

  }

  private SurveyResult createDemoSurveyResult(final Survey survey) {
    SurveyResult surveyResult = new SurveyResult();
    surveyResult.setSurvey(survey);
    surveyResult.setCreatedBy(DUMMY_ACCOUNT_ID);
    surveyResult = surveyResultDao.save(surveyResult);
    return surveyResult;
  }

}
