package com.ydw.survey.services;

import static org.junit.Assert.assertEquals;

import com.ydw.survey.CalculationExceptionMatcher;
import com.ydw.survey.model.TextFloatValue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;

/**
 * Survey calculator tests.
 *
 * @author etroshkin
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class SurveySpelCalculatorTests {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void before() {}

  @Test
  public void test_calculate_correctTemplate() throws IOException, Exception {
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    final String result = calculationService.calculateTemplate("result: {{#test1 + #test2}}",
        "test1", 5, "test2", 10);
    assertEquals("Expected result: 15", result, "result: 15");
  }

  @Test
  public void test_calculate_incorrectTemplate() throws IOException, Exception {
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    final String result = calculationService.calculateTemplate("result: {{#test1 #test2}}",
        "test1", 5, "test2", 10);
    assertEquals("Expected " + SurveySpelCalculator.CALC_RESULT_NOT_APPLICABLE, result,
        SurveySpelCalculator.CALC_RESULT_NOT_APPLICABLE);
  }

  @Test
  public void test_calculate_textFloatValue() throws IOException, Exception {
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    final TextFloatValue value1 = new TextFloatValue(5, "test_5");
    final TextFloatValue value2 = new TextFloatValue(10, "test_10");
    final TextFloatValue[] value3 =
        { new TextFloatValue(15, "test_15"), new TextFloatValue(20, "test_20") };

    // check calculations of object values together with primitive values
    String result =
        calculationService
            .calculateTemplate(
                "average result: {{(#value1 + #value2 + #value3[0] + #value3[1] + 10) / 5}}, "
                    + "used items: {{#value1.text}}, {{#value2.text}}, {{#value3[0].text}}, "
                    + "{{#value3[1].text}}",
                "value1", value1, "value2", value2, "value3", value3);
    final String expectedValue =
        "average result: 12, used items: test_5, test_10, test_15, test_20";
    assertEquals("Expected " + expectedValue, result, expectedValue);

    // // check usage of object values in custom spel functions
    result = calculationService.calculateTemplate(
        "{{AVERAGE(10, #value1, #value2, #value3[0], #value3[1])}}", "value1", value1, "value2",
        value2, "value3", value3);
    assertEquals("Expected 12", result, "12");

    // check usage of object values in custom spel functions
    result = calculationService.calculateTemplate(
        "{{25 + AVERAGE(1, 2, 3) + NUMBER(#value1 > 10 ? #value3[0] : #value3[1])}}", "value1",
        value1, "value2", value2, "value3", value3);

    assertEquals("Expected 47", result, "47");
  }

  @Test
  public void test_missingVariableTemplate() throws IOException, Exception {
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    final String result =
        calculationService.calculateTemplate("result: {{#test1 #test2}}", "test1", 5);
    assertEquals("Expected " + SurveySpelCalculator.CALC_RESULT_NOT_APPLICABLE, result,
        SurveySpelCalculator.CALC_RESULT_NOT_APPLICABLE);
  }

  @Test
  public void test_validate_correctTemplate() throws IOException, Exception {
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    calculationService.validateTemplate("result: {{#test1 + #test2}}", "test1", 5, "test2", 10);
  }

  @Test
  public void test_validate_incorrectTemplate() throws IOException, Exception {
    thrown.expect(
        new CalculationExceptionMatcher(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_EXPRESSION));
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    calculationService.validateTemplate("result: {{#test1 #test2}}", "test1", 5, "test2", 10);
  }

  @Test
  public void test_validate_missingVariableTemplate() throws IOException, Exception {
    thrown.expect(
        new CalculationExceptionMatcher(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE));
    final SurveySpelCalculationService calculationService = new SurveySpelCalculationService();
    calculationService.init();
    calculationService.validateTemplate("result: {{#test1 #test2}}", "test1", 5);
  }
}
