package com.ydw.survey.services;

import static org.junit.Assert.assertEquals;

import com.ydw.survey.CalculationExceptionMatcher;
import com.ydw.survey.model.TextFloatValue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;

/**
 * Survey calculator tests.
 *
 * @author etroshkin
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class SurveyEvalexCalculatorTests {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Before
  public void before() {}

  @Test
  public void test_calculate_correctTemplate() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    final String result = calculationService.calculateTemplate("result: {{#test1 + #test2}}",
        "test1", 5, "test2", 10);
    assertEquals("Expected result: 15", result, "result: 15");
  }

  @Test
  public void test_calculate_incorrectTemplate() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    final String result = calculationService.calculateTemplate("result: {{#test1 #test2}}",
        "test1", 5, "test2", 10);
    assertEquals("Expected result: 5 10", result, "result: 5 10");
  }

  @Test
  public void test_calculate_textFloatValue() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    final TextFloatValue value1 = new TextFloatValue(5, "test_5");
    final TextFloatValue value2 = new TextFloatValue(10, "test_10");

    // check calculations of object values together with primitive values
    final String result = calculationService.calculateTemplate(
        "average result: {{(#value1 + #value2 + 10) / 5}}", "value1", value1, "value2", value2);
    final String expectedValue = "average result: 5";
    assertEquals("Expected " + expectedValue, result, expectedValue);
  }

  @Test
  public void test_calculateWithoutExpressions_correctTemplate() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    final String result = calculationService.calculateTemplateWithoutExpressions(
        "result: {{#test1 + #test2}}", "test1", 5, "test2", 10);
    assertEquals("Expected result: 15", result, "result: {{5 + 10}}");
  }

  @Test
  public void test_missingVariableTemplate() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    final String result =
        calculationService.calculateTemplate("result: {{#test1 #test2}}", "test1", 5);
    assertEquals("Expected " + SurveyEvalexCalculator.CALC_RESULT_NOT_APPLICABLE, result,
        SurveyEvalexCalculator.CALC_RESULT_NOT_APPLICABLE);
  }

  @Test
  public void test_validate_correctTemplate() throws IOException, Exception {
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    calculationService.validateTemplate("result: {{#test1 + #test2}}", "test1", 5, "test2", 10);
  }

  @Test
  public void test_validate_missingVariableTemplate() throws IOException, Exception {
    thrown.expect(
        new CalculationExceptionMatcher(SurveyErrorCodes.SURVEY_CALCULATOR_INVALID_VARIABLE));
    final SurveyEvalexCalculationService calculationService =
        new SurveyEvalexCalculationService();
    calculationService.validateTemplate("result: {{#test1 #test2}}", "test1", 5);
  }
}
