package com.ydw.survey.api;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.common.security.utils.SecurityUtils;
import com.ydw.survey.AbstractApiTests;
import com.ydw.survey.dao.QuestionAnswerDao;
import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultCalculationDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.initializer.SurveyResultDataPopulator;
import com.ydw.survey.model.QuestionAnswer;
import com.ydw.survey.model.QuestionAnswerText;
import com.ydw.survey.model.QuestionBoolean;
import com.ydw.survey.model.QuestionFloat;
import com.ydw.survey.model.QuestionInteger;
import com.ydw.survey.model.QuestionList;
import com.ydw.survey.model.QuestionListValue;
import com.ydw.survey.model.QuestionScale;
import com.ydw.survey.model.QuestionText;
import com.ydw.survey.model.QuestionTextArea;
import com.ydw.survey.model.QuestionTextFloat;
import com.ydw.survey.model.QuestionType;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculationScale;
import com.ydw.survey.model.SurveyCalculationText;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;
import com.ydw.survey.model.SurveySection;
import com.ydw.survey.services.ResourceNotFoundException;
import com.ydw.survey.services.SurveyEvalexCalculationService;
import com.ydw.survey.services.SurveyResultService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.MediaTypes;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Survey demo data population tests.
 *
 * @author etroshkin
 */
@Transactional
public class SurveyResultApiTests extends AbstractApiTests {

  @Autowired
  private WebApplicationContext context;

  @Autowired
  EntityLinks entityLinks;

  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  QuestionAnswerDao questionAnswerDao;

  @Autowired
  SurveyCampaignDao surveyCampaignDao;

  @Autowired
  SurveyDao surveyDao;

  @Autowired
  SurveyEvalexCalculationService surveyEvalexCalculationService;

  @Autowired
  SurveyResultCalculationDao surveyResultCalculationDao;

  @Autowired
  SurveyResultDao surveyResultDao;

  @Autowired
  SurveyResultDataPopulator surveyResultDataPopulator;

  @Autowired
  SurveyResultService surveyResultService;

  /**
   * Clean-up.
   */
  @After
  public void after() {}

  /**
   * Setup.
   */
  @Before
  public void before() {
    mockMvc = MockMvcBuilders.webAppContextSetup(this.context).alwaysDo(print()).build();
  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveyCampaign() throws IOException, Exception {

    // Given
    final Survey survey1 = surveyDao.save(prepareShortSurvey());
    final Survey survey2 = surveyDao.save(prepareShortSurvey());
    final Survey survey3 = surveyDao.save(prepareShortSurvey());

    SurveyCampaign surveyCampaign = new SurveyCampaign();
    surveyCampaign.setAllowAnswerChanges(true);
    surveyCampaign.setTitle(new LocalizedString("test campaign"));
    surveyCampaign.addSurvey(survey1);
    surveyCampaign.addSurvey(survey2);
    surveyCampaign.addSurvey(survey3);
    surveyCampaign = surveyCampaignDao.save(surveyCampaign);

    // pass survey campaign first time
    SurveyResult surveyResult = passSurvey(surveyCampaign, survey1);
    surveyResult = passSurvey(surveyCampaign, survey2);
    surveyResult = passSurvey(surveyCampaign, survey3);
    assertEquals("Expected survey result is last", surveyResult.isLastInCampaign(), true);

    // pass survey campaign second time
    // When
    surveyResult = passSurvey(surveyCampaign, survey1);
    // Then
    assertEquals(
        "Expected survey result campaign answer counter: "
            + surveyResult.getCampaignAnswerCounter(),
        surveyResult.getCampaignAnswerCounter(), 2);

    // When
    surveyResult = passSurvey(surveyCampaign, survey2);
    // Then
    assertEquals(
        "Expected survey result campaign answer counter: "
            + surveyResult.getCampaignAnswerCounter(),
        surveyResult.getCampaignAnswerCounter(), 2);

    // When
    surveyResult = passSurvey(surveyCampaign, survey3);
    // Then
    assertEquals(
        "Expected survey result campaign answer counter: "
            + surveyResult.getCampaignAnswerCounter(),
        surveyResult.getCampaignAnswerCounter(), 2);
    assertEquals("Expected survey result is last", surveyResult.isLastInCampaign(), true);
  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveyResultCalculationSaved_Success() throws IOException, Exception {

    // Given
    Survey survey = prepareShortSurvey();

    // customize survey calculation template
    final SurveyCalculationText surveyCalculation1 = new SurveyCalculationText();
    surveyCalculation1.setCondition("5 > 4");
    surveyCalculation1.setTemplate(new LocalizedString("result: {{#q1}}, {{#q2}}"));
    survey.addCalculation(surveyCalculation1);

    final SurveyCalculationScale surveyCalculation2 =
        new SurveyCalculationScale(1, "bad", 10, "good");
    surveyCalculation2.setTemplate(new LocalizedString("{{5 + 4}}"));
    survey.addCalculation(surveyCalculation2);

    survey = surveyDao.save(survey);

    surveyEvalexCalculationService.validateSurveyCalculation(survey.getCalculations().get(0));
    surveyEvalexCalculationService.validateSurveyCalculation(survey.getCalculations().get(1));

    // create survey result for dummy user
    final SurveyResult surveyResult = prepareSurveyResult(survey);

    final URI surveyResultUri = entityLinks.linkForSingleResource(surveyResult).toUri();

    // save survey result calculation via REST
    final Map<String, Object> surveyResultMap = new HashMap<String, Object>();
    surveyResultMap.put("calculate", true);

    final ResultActions resultActions = mockMvc.perform(patch(surveyResultUri) //
        .contentType(MediaTypes.HAL_JSON) //
        .content(objectMapper.writeValueAsString(surveyResultMap)) //
        .accept(MediaTypes.HAL_JSON));

    // Then
    resultActions.andExpect(status().isOk());
  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveyResultFindBy_Success() throws IOException, Exception {

    // Given
    Survey survey = prepareFullSurvey();
    survey = surveyDao.save(survey);

    final URI surveyUri = entityLinks.linkForSingleResource(survey).toUri();

    final Map<String, Object> surveyResultMap = new HashMap<>();
    surveyResultMap.put("survey", surveyUri.toString());
    final URI surveyResultUri = entityLinks.linkFor(SurveyResult.class).toUri();

    // When
    mockMvc.perform(post(surveyResultUri) //
        .contentType(MediaTypes.HAL_JSON) //
        .content(objectMapper.writeValueAsString(surveyResultMap)) //
        .accept(MediaTypes.HAL_JSON));

    final SurveyResult surveyResult = surveyResultDao.findBySurveyOrderByCreated(survey).get(0);

    final URI surveyResultSectionsUri =
        entityLinks.linkForSingleResource(surveyResult).slash("sections").toUri();

    final ResultActions getSurveyResultSectionsActions =
        mockMvc.perform(get(surveyResultSectionsUri) //
            .contentType(MediaTypes.HAL_JSON) //
            .accept(MediaTypes.HAL_JSON));

    // Then
    getSurveyResultSectionsActions.andExpect(status().isOk()) //
        .andExpect(
            jsonPath(
                "$._embedded.surveyResultSections[0]._embedded."
                    + "questionAnswers[0].question.questionType",
                is(QuestionType.TEXT.toString()))) //
        .andExpect(
            jsonPath(
                "$._embedded.surveyResultSections[0]._embedded."
                    + "questionAnswers[1].question.questionType",
                is(QuestionType.LIST.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[2].question.questionType",
            is(QuestionType.TEXTAREA.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[3].question.questionType",
            is(QuestionType.INTEGER.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[4].question.questionType",
            is(QuestionType.FLOAT.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[5].question.questionType",
            is(QuestionType.BOOLEAN.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[6].question.questionType",
            is(QuestionType.SCALE.toString())))
        .andExpect(jsonPath(
            "$._embedded.surveyResultSections[0]._embedded."
                + "questionAnswers[7].question.questionType",
            is(QuestionType.TEXT_FLOAT.toString())));

  }

  private SurveyResult passSurvey(final SurveyCampaign surveyCampaign, final Survey survey) {
    // When
    SurveyResult surveyResult;
    try {
      surveyResult = surveyResultService
          .findBySurveyCampaignSlugAndCreatedBy(surveyCampaign.getSlug(),
              SecurityUtils.getMyPrincipal());
    } catch (final ResourceNotFoundException rnfexc) {
      // create next survey result in campaign
      surveyResult = new SurveyResult();
      surveyResult.setSurveyCampaign(surveyCampaign);
      surveyResult.setCreatedBy(SecurityUtils.getMyPrincipal());
      surveyResultService.assignSurveyResultByCampaign(surveyResult);
      surveyResult = surveyResultDao.save(surveyResult);
      surveyResult = surveyResultDataPopulator.fillSurveyResult(surveyResult);
    }
    // Then
    assertEquals("Expected survey: " + survey.getSlug(), surveyResult.getSurvey().getSlug(),
        survey.getSlug());
    assertEquals("Expected campaign: " + surveyCampaign.getSlug(),
        surveyResult.getSurveyCampaign().getSlug(), surveyCampaign.getSlug());
    surveyResult.setCompleted(true);
    surveyResult.setCampaignAnswerCounter(surveyResult.getCampaignAnswerCounter() + 1);
    surveyResult = surveyResultDao.save(surveyResult);
    return surveyResult;
  }

  private Survey prepareFullSurvey() {
    final Survey survey = new Survey();
    survey.setTitle(new LocalizedString("test survey"));

    // survey section
    final SurveySection surveySection = new SurveySection();
    surveySection.setTitle(new LocalizedString("test survey section"));
    survey.addSection(surveySection);

    // question text
    final QuestionText questionText = new QuestionText();
    questionText.setTitle(new LocalizedString("test survey question text"));
    surveySection.addQuestion(questionText);

    // question list
    final QuestionList questionList = new QuestionList();
    questionList.setTitle(new LocalizedString("test survey question"));
    questionList.addListValue(new QuestionListValue("list value 1", 0, null));
    questionList.addListValue(new QuestionListValue("list value 2", 1, null));
    surveySection.addQuestion(questionList);

    // question text area
    final QuestionTextArea questionTextArea = new QuestionTextArea();
    questionTextArea.setTitle(new LocalizedString("test survey question text area"));
    surveySection.addQuestion(questionTextArea);

    // question integer
    final QuestionInteger questionInteger = new QuestionInteger();
    questionInteger.setTitle(new LocalizedString("test survey question integer"));
    surveySection.addQuestion(questionInteger);

    // question float
    final QuestionFloat questionFloat = new QuestionFloat();
    questionFloat.setTitle(new LocalizedString("test survey question float"));
    surveySection.addQuestion(questionFloat);

    // question boolean
    final QuestionBoolean questionBoolean = new QuestionBoolean();
    questionBoolean.setTitle(new LocalizedString("test survey question boolean"));
    surveySection.addQuestion(questionBoolean);

    // question scale
    final QuestionScale questionScale = new QuestionScale();
    questionScale.setTitle(new LocalizedString("test survey question scale"));
    surveySection.addQuestion(questionScale);

    // question text float
    final QuestionTextFloat questionTextFloat = new QuestionTextFloat();
    questionTextFloat.setTitle(new LocalizedString("test survey question text float"));
    surveySection.addQuestion(questionTextFloat);

    return survey;
  }

  private Survey prepareShortSurvey() {
    final Survey survey = new Survey();
    survey.setTitle(new LocalizedString("test survey"));

    final SurveySection section = new SurveySection();
    section.setTitle(new LocalizedString("test survey section"));
    survey.addSection(section);

    final QuestionText question1 = new QuestionText();
    question1.setTitle(new LocalizedString("test survey section"));
    question1.setVariableName("q1");
    section.addQuestion(question1);

    final QuestionText question2 = new QuestionText();
    question2.setTitle(new LocalizedString("test survey section"));
    question2.setVariableName("q2");
    section.addQuestion(question2);

    return survey;
  }

  private SurveyResult prepareSurveyResult(final Survey survey)
      throws Exception, JsonProcessingException {
    final URI surveyResultUri = entityLinks.linkFor(SurveyResult.class).toUri();
    final URI surveyUri = entityLinks.linkForSingleResource(survey).toUri();

    final Map<String, Object> surveyResultMap = new HashMap<String, Object>();
    surveyResultMap.put("survey", surveyUri.toString());

    mockMvc.perform(post(surveyResultUri) //
        .contentType(MediaTypes.HAL_JSON) //
        .content(objectMapper.writeValueAsString(surveyResultMap)).accept(MediaTypes.HAL_JSON));

    final SurveyResult surveyResult = surveyResultDao.findBySurveyOrderByCreated(survey).get(0);

    // When
    final List<QuestionAnswer> questionAnswers =
        questionAnswerDao.findBySectionSurveyResultOrderBySortOrder(surveyResult);

    final QuestionAnswerText questionAnswerText1 = (QuestionAnswerText) questionAnswers.get(0);
    final QuestionAnswerText questionAnswerText2 = (QuestionAnswerText) questionAnswers.get(1);

    // save result survey answers
    questionAnswerText1.setAnswerValue("test1");
    questionAnswerText2.setAnswerValue("test1");
    questionAnswerDao.save(questionAnswers);
    return surveyResult;
  }

}
