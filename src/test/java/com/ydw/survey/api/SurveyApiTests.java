package com.ydw.survey.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.survey.AbstractApiTests;
import com.ydw.survey.dao.QuestionTextDao;
import com.ydw.survey.dao.SurveyCalculationScaleDao;
import com.ydw.survey.dao.SurveyCalculationTextDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.model.QuestionText;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCalculationScale;
import com.ydw.survey.model.SurveyCalculationText;
import com.ydw.survey.model.SurveySection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

/**
 * Survey demo data population tests.
 *
 * @author etroshkin
 */
@Transactional
public class SurveyApiTests extends AbstractApiTests {

  @Autowired
  private WebApplicationContext context;

  @Autowired
  EntityLinks entityLinks;

  @Autowired
  SurveyDao surveyDao;

  @Autowired
  SurveyCalculationTextDao surveyCalculationTextDao;

  @Autowired
  SurveyCalculationScaleDao surveyCalculationScaleDao;

  @Autowired
  QuestionTextDao questionTextDao;

  @Autowired
  private ObjectMapper objectMapper;

  private MockMvc mockMvc;

  /**
   * Clean-up.
   */
  @After
  public void after() {}

  /**
   * Setup.
   */
  @Before
  public void before() {
    mockMvc = MockMvcBuilders.webAppContextSetup(this.context).alwaysDo(print()).build();
  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveyCalculationSaved_Success() throws IOException, Exception {

    // Given
    Survey survey = new Survey();
    survey.setTitle(new LocalizedString("test survey"));
    final SurveySection section = new SurveySection();
    section.setTitle(new LocalizedString("test survey section"));
    survey.addSection(section);
    survey = surveyDao.save(survey);

    final QuestionText question1 = new QuestionText();
    question1.setTitle(new LocalizedString("test survey section"));
    question1.setVariableName("q1");
    question1.setSection(section);
    questionTextDao.save(question1);

    final QuestionText question2 = new QuestionText();
    question2.setTitle(new LocalizedString("test survey section"));
    question2.setVariableName("q2");
    question2.setSection(section);
    questionTextDao.save(question2);

    SurveyCalculationText surveyCalculation1 = new SurveyCalculationText();
    surveyCalculation1.setTemplate(new LocalizedString("{{#q1}}, {{#q2}}"));
    surveyCalculation1.setSurvey(survey);
    surveyCalculation1 = surveyCalculationTextDao.save(surveyCalculation1);

    SurveyCalculationScale surveyCalculation2 =
        new SurveyCalculationScale(1, "bad", 10, "good");
    surveyCalculation2.setTemplate(new LocalizedString("{{5 + 4}}"));
    surveyCalculation2.setSurvey(survey);
    surveyCalculation2 = surveyCalculationScaleDao.save(surveyCalculation2);

    survey.addCalculation(surveyCalculation1);
    survey.addCalculation(surveyCalculation2);
    surveyDao.save(survey);

    // When

    // prepare survey calculation with valid template
    final Map<String, Object> surveyMap = new HashMap<>();
    surveyMap.put("validate", true);
    final URI surveyUri = entityLinks.linkForSingleResource(survey).toUri();

    final ResultActions resultActions = mockMvc.perform(patch(surveyUri) //
        .contentType(MediaTypes.HAL_JSON) //
        .content(objectMapper.writeValueAsString(surveyMap)) //
        .accept(MediaTypes.HAL_JSON));

    // Then
    resultActions.andExpect(status().isOk());

  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveySaved_Success() throws IOException, Exception {

    // Given
    final URI surveyUri = entityLinks.linkFor(Survey.class).toUri();
    final Map<String, Object> survey = new HashMap<>();
    survey.put("title", "test survey");

    // When
    final ResultActions resultActions = mockMvc.perform(post(surveyUri) //
        .contentType(MediaTypes.HAL_JSON) //
        .content(objectMapper.writeValueAsString(survey)) //
        .accept(MediaTypes.HAL_JSON));

    // Then
    resultActions.andExpect(status().isCreated()) //
        .andExpect(header().string(HttpHeaders.LOCATION, notNullValue())) //
        .andExpect(jsonPath("title", is("test survey")));
  }

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_checkSurveySectionOrdered_Success() throws IOException, Exception {

    // Given
    Survey survey = new Survey();
    survey.setTitle(new LocalizedString("test survey"));
    for (int index = 0; index < 3; index++) {
      final SurveySection surveySection = new SurveySection();
      surveySection.setTitle(new LocalizedString("test survey section 1." + (index + 1)));
      survey.addSection(surveySection);
    }
    survey = surveyDao.save(survey);

    final SurveySection surveySection11 = survey.getSections().get(0);
    final SurveySection surveySection13 = survey.getSections().get(2);

    final URI surveySection1_1ReorderUri =
        entityLinks.linkForSingleResource(surveySection11).toUri();
    final URI surveySection1_3ReorderUri =
        entityLinks.linkForSingleResource(surveySection13).toUri();

    // When
    final int surveySection11SortOrder = surveySection11.getSortOrder();
    final int surveySection13SortOrder = surveySection13.getSortOrder();
    final Map<String, Object> surveySectionMap = new HashMap<>();
    surveySectionMap.put("sortOrder", surveySection13SortOrder);

    final ResultActions resultActionsReorder11 =
        mockMvc.perform(patch(surveySection1_1ReorderUri) //
            .contentType(MediaTypes.HAL_JSON) //
            .content(objectMapper.writeValueAsString(surveySectionMap)) //
            .accept(MediaTypes.HAL_JSON));

    surveySectionMap.put("sortOrder", surveySection11SortOrder);

    final ResultActions resultActionsReorder13 =
        mockMvc.perform(patch(surveySection1_3ReorderUri) //
            .contentType(MediaTypes.HAL_JSON) //
            .content(objectMapper.writeValueAsString(surveySectionMap)) //
            .accept(MediaTypes.HAL_JSON));

    // Then
    resultActionsReorder11.andExpect(status().isOk()) //
        .andExpect(jsonPath("sortOrder", is(surveySection13SortOrder)));

    resultActionsReorder13.andExpect(status().isOk()) //
        .andExpect(jsonPath("sortOrder", is(surveySection11SortOrder)));
  }

  // @Test
  // public void test_checkSurveysPopulated_Success() throws IOException, Exception {
  //
  // // Given
  // final URI surveysUri = entityLinks.linkFor(Survey.class).toUri();
  //
  // // When
  // final ResultActions resultActions = mockMvc.perform(get(surveysUri));
  //
  // // Then
  // resultActions.andExpect(status().isOk()) //
  // .andExpect(jsonPath("_links.self.href", notNullValue()));
  // }

}
