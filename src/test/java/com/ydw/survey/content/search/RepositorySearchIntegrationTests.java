package com.ydw.survey.content.search;

import com.ydw.survey.AbstractIntegrationTests;
import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.content.model.Category;
import com.ydw.survey.content.services.CategoryService;
import com.ydw.survey.model.SurveyCategory;
import com.ydw.survey.services.SurveyService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;

@WithMockUser(authorities = "ADMIN")
public class RepositorySearchIntegrationTests extends AbstractIntegrationTests {

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private CategorySearchDao categorySearchDao;

  @Autowired
  private CategorySearchService categorySearchService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private SurveyService surveyService;

  @Before
  public void before() {
    surveyService.deleteAll();
    categorySearchDao.deleteAll();
    categoryDao.deleteAll();
  }

  @Test
  public void test_mismatchCategory_notFoundByTitle() {
    // Given
    categoryService.save(new SurveyCategory("Test category title"));

    // When
    final List<Category> results =
        categorySearchService.findByTaxonomyAndTitle(SurveyCategory.TAXONOMY,
            "Test category title another");

    // Then
    Assert.assertEquals("Result size", 0, results.size());
  }

  @Test
  public void test_savedCategory_foundByTitle() {
    // Given
    final SurveyCategory category =
        categoryService.save(new SurveyCategory("Test category title"));

    final List<Category> results =
        categorySearchService.findByTaxonomyAndTitle(SurveyCategory.TAXONOMY,
            category.getTitle().getValue());

    // Then
    Assert.assertEquals("Result size", 1, results.size());
    Assert.assertTrue("Found in results", results.contains(category));
  }

}
