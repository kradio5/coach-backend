package com.ydw.survey;

import com.ydw.common.spring.Profiles;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SurveyServer.class)
@ActiveProfiles(Profiles.TEST)
public abstract class AbstractIntegrationTests {
}
