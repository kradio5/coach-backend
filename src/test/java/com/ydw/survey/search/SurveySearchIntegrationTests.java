package com.ydw.survey.search;

import com.ydw.common.data.model.LocalizedString;
import com.ydw.survey.AbstractIntegrationTests;
import com.ydw.survey.content.dao.CategoryDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCategory;
import com.ydw.survey.model.SurveySection;
import com.ydw.survey.search.model.SurveySearchOptions;
import com.ydw.survey.services.SurveyService;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.test.context.support.WithMockUser;

@WithMockUser(authorities = "ADMIN")
public class SurveySearchIntegrationTests extends AbstractIntegrationTests {

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private SurveySearchService surveySearchService;

  @Autowired
  private SurveyService surveyService;

  /**
   * Prepares a test.
   */
  @Before
  public void before() {
    surveyService.deleteAll();
    categoryDao.deleteAll();
  }

  @Test
  public void test_Search_OptionalCategory_Ignored() {
    // Given
    final SurveyCategory category1 = new SurveyCategory();
    category1.setSlug("category-1");
    categoryDao.save(category1);

    final SurveyCategory category2 = new SurveyCategory();
    category2.setSlug("category-2");
    categoryDao.save(category2);

    final Survey survey1 = new Survey();
    survey1.setTitle(new LocalizedString("Survey 1"));
    survey1.addCategory(category1);
    survey1.addCategory(category2);
    survey1.addSection(new SurveySection());
    surveyService.save(survey1);

    final Survey survey2 = new Survey();
    survey2.setTitle(new LocalizedString("Survey 2"));
    survey2.addCategory(category2);
    survey2.addSection(new SurveySection());
    surveyService.save(survey2);

    // When
    final SurveySearchOptions options = new SurveySearchOptions();
    options.addCategory(category1.getSlug());
    options.addCategory("category-3");
    final Page<Survey> result =
        surveySearchService.findAll(options, new PageRequest(0, 20));

    // Then
    Assert.assertEquals("Results count", 1, result.getContent().size());
    Assert.assertTrue("Entity in result", result.getContent().contains(survey1));
  }

  @Test
  public void test_SurveySearchPublishedAndCategory() {
    // Given
    final SurveyCategory category1 = new SurveyCategory();
    category1.setSlug("category-1");
    categoryDao.save(category1);

    final SurveyCategory category2 = new SurveyCategory();
    category2.setSlug("category-2");
    categoryDao.save(category2);

    final Survey survey1 = new Survey();
    survey1.setTitle(new LocalizedString("Survey 1"));
    survey1.addCategory(category1);
    survey1.addCategory(category2);
    survey1.addSection(new SurveySection());
    // survey1.getAccess().setPublished(ZonedDateTime.now());
    surveyService.save(survey1);

    final Survey survey2 = new Survey();
    survey2.setTitle(new LocalizedString("Survey 2"));
    survey2.addCategory(category1);
    survey2.addSection(new SurveySection());
    surveyService.save(survey2);

    // When
    final SurveySearchOptions options = new SurveySearchOptions();
    options.addCategory(category1.getSlug());
    options.addCategory("optional-category");
    // options.setShowPublishedOnly(true);
    final Page<Survey> result =
        surveySearchService.findAll(options, new PageRequest(0, 20));

    // Then
    Assert.assertEquals("Results count", 2, result.getContent().size());
    Assert.assertTrue("Entity in result", result.getContent().contains(survey1));
  }

  @Test
  public void test_SurveyWithTitlePhrase_FoundByWord() {
    // Given
    Survey entity = new Survey();
    entity.setTitle(new LocalizedString("A Word Match Test"));
    entity.addSection(new SurveySection());
    entity = surveyService.save(entity);

    Survey entityMismatch = new Survey();
    entityMismatch.setTitle(new LocalizedString("Mismatch"));
    entityMismatch.addSection(new SurveySection());
    entityMismatch = surveyService.save(entityMismatch);

    // When
    final SurveySearchOptions options = new SurveySearchOptions();
    options.setText("match");
    final Page<Survey> result =
        surveySearchService.findAll(options, new PageRequest(0, 20));

    // Then
    Assert.assertEquals("Results count", 1, result.getContent().size());
    Assert.assertEquals("Entity in result", entity.getId(), result.getContent().get(0).getId());
  }

}
