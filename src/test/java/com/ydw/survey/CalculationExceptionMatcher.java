package com.ydw.survey;

import com.ydw.survey.services.CalculationException;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;

public class CalculationExceptionMatcher extends BaseMatcher<CalculationException> {

  private final String code;

  public CalculationExceptionMatcher(String code) {
    this.code = code;
  }

  @Override
  public boolean matches(Object item) {
    return item instanceof CalculationException
        && ((CalculationException) item).getCode().equals(code);
  }

  @Override
  public void describeTo(Description description) {}
}
