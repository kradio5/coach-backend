package com.ydw.survey.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

import java.io.IOException;

/**
 * Tests of logic within model.
 *
 * @author et
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class ModelTests {

  @Before
  public void before() {}

  @Test
  public void test_validate_questionScaleAnswerAndStep() throws IOException, Exception {
    final QuestionScale question = new QuestionScale();
    final QuestionAnswerScale questionAnswer = new QuestionAnswerScale();
    questionAnswer.setQuestion(question);
    final int[][] minMaxes =
        { { 0, 100 }, { 5, 13 }, { -10, 1001 }, { 201, 2000 }, { 503, 510 } };
    for (final int[] minMax : minMaxes) {
      // System.out.println("***********" + minMax[0] + "-" + minMax[1] + "***********");
      question.setMinValue(minMax[0]);
      question.setMaxValue(minMax[1]);
      for (int stepAmount = 1; stepAmount <= 30; stepAmount++) {
        question.setStepAmount(stepAmount);
        // System.out.println("***********" + stepAmount + "***********");
        for (int step = 0; step <= stepAmount; step++) {
          questionAnswer.setStep(step);
          final double answerValue = questionAnswer.getAnswerValue();
          questionAnswer.setAnswerValue(answerValue);
          // System.out.println(questionAnswer.getStep() + ": " + answerValue);
          assertEquals("Expected step: " + step, step, questionAnswer.getStep());
        }
      }
    }
  }
}
