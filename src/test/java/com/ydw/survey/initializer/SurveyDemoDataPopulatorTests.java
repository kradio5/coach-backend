package com.ydw.survey.initializer;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ydw.survey.dao.SurveyCampaignDao;
import com.ydw.survey.dao.SurveyDao;
import com.ydw.survey.dao.SurveyResultDao;
import com.ydw.survey.model.Survey;
import com.ydw.survey.model.SurveyCampaign;
import com.ydw.survey.model.SurveyResult;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.test.context.support.WithMockUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Survey demo data population tests.
 *
 * @author etroshkin
 */
@RunWith(MockitoJUnitRunner.class)
public class SurveyDemoDataPopulatorTests {

  private DemoDataPopulator demoDataPopulator;

  @Mock
  private SurveyDao surveyDao;

  @Mock
  private SurveyResultDao surveyResultDao;

  @Mock
  private SurveyCampaignDao surveyCampaignDao;

  @Mock
  private SurveyResultDataPopulator surveyResultDataPopulator;

  @Before
  public void before() {}

  @Test
  @WithMockUser(authorities = "EXPERT")
  public void test_init_SurveyAdded() throws IOException, Exception {
    when(surveyCampaignDao.save(any(SurveyCampaign.class)))
        .then(invocation -> invocation.getArgumentAt(0, SurveyCampaign.class));
    when(surveyDao.save(any(Survey.class)))
        .then(invocation -> invocation.getArgumentAt(0, Survey.class));
    when(surveyResultDataPopulator.fillSurveyResult(any(SurveyResult.class)))
        .then(invocation -> invocation.getArgumentAt(0, SurveyResult.class));

    final List<Survey> allSurveys = new ArrayList<Survey>();
    allSurveys.add(new Survey());
    allSurveys.add(new Survey());
    allSurveys.add(new Survey());
    when(surveyDao.findAll()).thenReturn(allSurveys);
    when(surveyResultDao.save(any(SurveyResult.class)))
        .then(invocation -> invocation.getArgumentAt(0, SurveyResult.class));
    demoDataPopulator =
        new DemoDataPopulator(new SurveyDemoDataPopulator(surveyDao, surveyCampaignDao));
    demoDataPopulator.init();
    verify(surveyDao, atLeast(2)).save(any(Survey.class));
  }
}
